package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.IDPLog;

public interface IDPLogService {

    IDPLog findByLogID(int logID);
    List<IDPLog> getAllLogs();
    void saveLog(IDPLog log);
    void updateLog(IDPLog log);
    void deleteLog(int logID);
}
