package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.IDPLogDao;
import trust.simulator.entity.IDPLog;
import trust.simulator.service.IDPLogService;


@Service("IDPLogService")
@Transactional(readOnly = true)
public class IDPLogServiceImpl implements IDPLogService {

    @Autowired
    private IDPLogDao idplogDao;

    @Override
    public IDPLog findByLogID(int logID) {
        return idplogDao.findById(logID);
    }

	@Override
	public List<IDPLog> getAllLogs() {
		return idplogDao.getAllInstances();
		
	}

	@Override
	public void saveLog(IDPLog log) {
		idplogDao.save(log);
		
	}

	@Override
	public void updateLog(IDPLog log) {
	idplogDao.update(log);
		
	}

	@Override
	public void deleteLog(int logID) {
		IDPLog idpLog = idplogDao.findById(logID);
        if (idpLog != null) {
        	idplogDao.delete(idpLog);
        }
		
	}

}
