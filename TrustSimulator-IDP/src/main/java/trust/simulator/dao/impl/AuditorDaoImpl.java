package trust.simulator.dao.impl;


import java.util.HashSet;
import java.util.Set;

import org.mitre.oauth2.model.ClientDetailsEntity;
import org.mitre.oauth2.repository.OAuth2ClientRepository;
import org.mitre.oauth2.repository.impl.JpaOAuth2ClientRepository;
import org.springframework.stereotype.Repository;

import trust.simulator.dao.AuditorDao;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorLog;
import trust.simulator.oauth2.mapping.Authorities_oAuth2;
import trust.simulator.oauth2.mapping.Userinfo_oAuth2;
import trust.simulator.oauth2.mapping.Users_oAuth2;

@Repository
public class AuditorDaoImpl extends AbstractDaoImpl<Auditor, String> implements AuditorDao {

    protected AuditorDaoImpl() {
        super(Auditor.class);
    }
    
	//Log Events
    @Override
    public Auditor save(Auditor e) {
     
    	AuditorLog auditorLog = new AuditorLog(e, "save");
        getCurrentSession().persist(auditorLog);
        
        Users_oAuth2 oAuth2User = new Users_oAuth2(e.getName(), e.getPassword(), 1);
        getCurrentSession().persist(oAuth2User);
        
        Authorities_oAuth2 Role_User = new Authorities_oAuth2(e.getName(),"ROLE_USER");
        getCurrentSession().persist(Role_User);
        Authorities_oAuth2 Role_Auditor = new Authorities_oAuth2(e.getName(),"ROLE_AUDITOR");
        getCurrentSession().persist(Role_Auditor);
        
        Userinfo_oAuth2 oAuth2Userinfo = new Userinfo_oAuth2(e.getID(), e.getSub(),e.getName(),e.getName(),e.getEmail(),1);
        getCurrentSession().persist(oAuth2Userinfo);
        
        OAuth2ClientRepository clientRepo = new JpaOAuth2ClientRepository(getCurrentSession());
        
        ClientDetailsEntity clientDetails = new ClientDetailsEntity();
        
        Set<String> redirectUris = new HashSet<String>();
        //TODO get the correct path by communicating with the entity api
        redirectUris.add(e.getHost()+"/openid_connect_login");
        clientDetails.setRedirectUris(redirectUris);
        
        Set<String> scopeSet = new HashSet<String>();
        //TODO get the correct scopes by communicating with the entity api
        scopeSet.add("TrustSimulator_Scope");
        scopeSet.add("openid");
        scopeSet.add("offline_access");
        clientDetails.setScope(scopeSet);
        
        Set<String> responseTypes = new HashSet<String>();
        //TODO get the correct response type by communicating with the entity api
        responseTypes.add("code");
        clientDetails.setResponseTypes(responseTypes);
        
        Set<String> grantTypes = new HashSet<String>();
        //TODO get the correct grant type by communicating with the entity api
        grantTypes.add("authorization_code");
        grantTypes.add("refresh_token");
        clientDetails.setGrantTypes(grantTypes);
        
        clientDetails.setReuseRefreshToken(true);
        clientDetails.setDynamicallyRegistered(false);
        clientDetails.setAllowIntrospection(true);
        clientDetails.setIdTokenValiditySeconds(600);
        clientDetails.setClientId(e.getName());
        clientDetails.setClientSecret(e.getPassword());
        clientDetails.setClientName(e.getName());
        clientDetails.setTokenEndpointAuthMethod(ClientDetailsEntity.AuthMethod.SECRET_BASIC);
        clientDetails.setSubjectType(ClientDetailsEntity.SubjectType.PUBLIC);
        clientDetails.setRequireAuthTime(false);
        clientDetails.setCreatedAt(new java.util.Date());
        
        clientRepo.saveClient(clientDetails);
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(Auditor e) {
    	   
        AuditorLog auditorLog = new AuditorLog(e, "update");
        getCurrentSession().persist(auditorLog);
        
        Users_oAuth2 oAuth2User = new Users_oAuth2(e.getName(), e.getPassword(), 1);
        getCurrentSession().merge(oAuth2User);
        
        Userinfo_oAuth2 oAuth2Userinfo = new Userinfo_oAuth2(e.getID(), e.getSub(),e.getName(),e.getName(),e.getEmail(),1);
        getCurrentSession().merge(oAuth2Userinfo);
        
        OAuth2ClientRepository clientRepo = new JpaOAuth2ClientRepository(getCurrentSession());
        
        ClientDetailsEntity clientDetails = clientRepo.getClientByClientId(e.getName());
        
        Set<String> redirectUris = new HashSet<String>();
        //TODO get the correct path by communicating with the entity api
        redirectUris.add(e.getHost()+"/openid_connect_login");
        clientDetails.setRedirectUris(redirectUris);
                
        clientDetails.setClientId(e.getName());
        clientDetails.setClientSecret(e.getPassword());
        clientDetails.setClientName(e.getName());
        
        clientRepo.updateClient(clientDetails.getId(), clientDetails);
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(Auditor e) {
    	
        Users_oAuth2 oAuth2User = new Users_oAuth2(e.getName(), e.getPassword(), 1);
        getCurrentSession().remove(getCurrentSession().merge(oAuth2User));
        
        Authorities_oAuth2 Role_User = new Authorities_oAuth2(e.getName(),"ROLE_USER");
        getCurrentSession().remove(getCurrentSession().merge(Role_User));
        Authorities_oAuth2 Role_Auditor = new Authorities_oAuth2(e.getName(),"ROLE_AUDITOR");
        getCurrentSession().remove(getCurrentSession().merge(Role_Auditor));
        
    	Userinfo_oAuth2 oAuth2Userinfo = new Userinfo_oAuth2(e.getID());
        getCurrentSession().remove(getCurrentSession().merge(oAuth2Userinfo));
        
        OAuth2ClientRepository clientRepo = new JpaOAuth2ClientRepository(getCurrentSession());
        
        ClientDetailsEntity clientDetails = clientRepo.getClientByClientId(e.getName());
        
        clientRepo.deleteClient(clientDetails);
       
        AuditorLog auditorLog = new AuditorLog(e, "remove");
        getCurrentSession().persist(auditorLog);
        
        getCurrentSession().flush();
    }

}
