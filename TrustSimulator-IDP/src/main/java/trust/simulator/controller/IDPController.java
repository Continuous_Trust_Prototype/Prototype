package trust.simulator.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.IDPLog;
import trust.simulator.entity.SP;
import trust.simulator.entity.User;
import trust.simulator.entity.UserLog;
import trust.simulator.service.AbstractLogService;
import trust.simulator.service.AdminService;
import trust.simulator.service.AuditorService;
import trust.simulator.service.SPService;
import trust.simulator.service.UserService;

@Controller
public class IDPController {
	
    @Autowired
    private AdminService adminService;
	
    @Autowired
    private UserService userService;

    @Autowired
    private AuditorService auditorService;
    
    @Autowired
    private SPService spService;
    
    @Autowired
    private AbstractLogService abstractlogService;

 /*   @RequestMapping(value = "/logoutSimulator", method = RequestMethod.GET)
    public void logout(HttpSession session) {
    	System.out.println("IDP logout 1");
    	//session = request.getSession(false);
    	if (session != null) session.invalidate();
    	SecurityContextHolder.clearContext();    
    }    
*/
    
    @RequestMapping(value = "/resetDB", method = RequestMethod.GET)
    public String resetDB() {
    	
    	adminService.resetDB();
    	
    	return "index";
    }
    
    @RequestMapping(value = "/persistEndUser", method = RequestMethod.POST)
    public void persistEndUser(@RequestParam("action_user") String action_user) {
    	
    	System.out.println("persistEndUser - action_user: "+action_user);
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_user);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_user"); 
        
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    	
    	User user = User.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) userService.saveUser(user);
    	else if(action.equals("update")) userService.updateUser(user);
    	else if(action.equals("delete")) userService.deleteUser(user);
    }
    
    @RequestMapping(value = "/persistSP", method = RequestMethod.POST)
    public void persistSP(@RequestParam("action_sp") String action_sp) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_sp);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_sp"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	SP sp = SP.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) spService.saveSp(sp);
    	else if(action.equals("update")) spService.updateSp(sp);
    	else if(action.equals("delete")) spService.deleteSp(sp);
    }
    
    @RequestMapping(value = "/persistAuditor", method = RequestMethod.POST)
    public void persistAuditor(@RequestParam("action_auditor") String action_auditor) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_auditor);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_auditor"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    	
    	Auditor auditor = Auditor.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) auditorService.saveAuditor(auditor);
    	else if(action.equals("update")) auditorService.updateAuditor(auditor);
    	else if(action.equals("delete")) auditorService.deleteAuditor(auditor);
    }
    
    @RequestMapping(value = "/getUserLogs", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getUserLogs(@RequestParam("userID") int userID, @RequestParam(value = "untilTime", required = false) String untilTime) {
     	
    	JsonArray jsonArray = new JsonArray();    	
    	
    	List<AbstractLog> logs;
    	
    	if(untilTime != null && !untilTime.equals(""))
    	{
    		System.out.println("1: untilTime = "+untilTime);
    		
    		logs = userService.getAllUserLogs(userID, untilTime);
    	}
    	else
    	{
    		System.out.println("2: no untilTime!! ");
    		
    		logs = userService.getAllUserLogs(userID);
    	}
    		
    	for(int i=0; i< logs.size(); i++)
    	{
    		if(logs.get(i) instanceof UserLog)
    		{
    			UserLog logObj = (UserLog) logs.get(i);
    			JsonObject log = logObj.toJson();
        		jsonArray.add(log);
    		}
    		else if(logs.get(i) instanceof IDPLog)
    		{
    			IDPLog logObj = (IDPLog) logs.get(i);
    			JsonObject log = logObj.toJson();
        		jsonArray.add(log);
    		}
    		
    	}
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("logs", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
    	
        return new ResponseEntity<String>(jsonArrayString,HttpStatus.OK);
    }
    
}
