package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.MSP;
import trust.simulator.entity.SP;
import trust.simulator.service.MSPService;
import trust.simulator.service.MessageService;
import trust.simulator.service.SPService;
import trust.simulator.util.Operations;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class SPController {

    @Autowired
    private SPService spService;
    @Autowired
    private MSPService mspService;
    @Autowired
    private MessageService messageService;
  
    @RequestMapping(value = "/manageSPs", method = RequestMethod.GET)
    public String showSPPage(Model model) {
        model.addAttribute("sp",new SP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);       
        List<SP> sps = spService.getAllSps();
        model.addAttribute("sps", sps);
        return "manageSPs";
    }

    @RequestMapping(value = "/addSP", method = RequestMethod.POST)
    public String addSP( Model model, SP sp, HttpServletRequest request) {
        SP existing = spService.findByName(sp.getName());
        if (existing != null) {
            model.addAttribute("status", "exist");
            List<SP> sps = spService.getAllSps();
            model.addAttribute("sps", sps);
            return "manageSPs";
        }
        Integer mspID = Integer.parseInt(request.getParameter("mspID"));
        if(mspID != null && mspID != 0)
        {
	        MSP msp = mspService.findByMspID(mspID);
	        sp.setAffiliatedWithMsp(mspID);
	        sp.setAffiliatedWithMspName(msp.getName());
	        sp.setAffiliatedWithMspHost(msp.getHost());
        }
        sp.setCreatedOn(Operations.getCurrentTime());
        sp.setModifiedOn(Operations.getCurrentTime());
        //sp.setSpID(spService.getNewId());
        spService.saveSp(sp);
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);       
        model.addAttribute("saved", "success");
        model.addAttribute("sp",new SP());
        List<SP> sps = spService.getAllSps();
        model.addAttribute("sps", sps);
        return "manageSPs";
    }

    @RequestMapping(value = "/editSP_{spID}", method = RequestMethod.GET)
    public String editSP(Model model, @PathVariable int spID) {
        SP sp = spService.findBySpID(spID);
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);       
        model.addAttribute("sp", sp);
        model.addAttribute("request", "edit");
        return "manageSPs";
    }

    @RequestMapping(value = "/editSP_2", method = RequestMethod.POST)
    public String editSP_2( Model model, SP sp, HttpServletRequest request) {
    	Integer mspID = Integer.parseInt(request.getParameter("mspID"));
       System.out.println("mspID = "+mspID);
    	if(mspID != null && mspID != 0)
        {
    		System.out.println("1");
	        MSP msp = mspService.findByMspID(mspID);
	        sp.setAffiliatedWithMsp(mspID);
	        sp.setAffiliatedWithMspName(msp.getName());
	        sp.setAffiliatedWithMspHost(msp.getHost());
        }
        else
        {
        	System.out.println("2");
	        sp.setAffiliatedWithMsp(0);
	        sp.setAffiliatedWithMspName(null);
	        sp.setAffiliatedWithMspHost(null);
        }
    	sp.setModifiedOn(Operations.getCurrentTime());
        spService.updateSp(sp);
        model.addAttribute("updated", "success");
        model.addAttribute("sp",new SP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);       
        List<SP> sps = spService.getAllSps();
        model.addAttribute("sps", sps);
        return "manageSPs";
    }
    
    @RequestMapping(value = "/deleteSP_{spID}", method = RequestMethod.GET)
    public String deleteSP(Model model, @PathVariable int spID) {
    	SP sp = spService.findBySpID(spID);
    	spService.deleteSp(sp);
        model.addAttribute("deleted", "success");
        model.addAttribute("sp", new SP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);       
        List<SP> sps = spService.getAllSps();
        model.addAttribute("sps", sps);
        return "manageSPs";
    }
    
    @RequestMapping(value = "/getExpandedSP_{spID}", method = RequestMethod.GET)
    public String getExpndedSP(Model model, HttpSession session, @PathVariable int spID) {
    	    	
    	SP sp = spService.findBySpID(spID);
    	model.addAttribute("sp", sp);
    	
        model.addAttribute("request", "showExpandedSP");

        return "manageSPs";
    }
    

}
