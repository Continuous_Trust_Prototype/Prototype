package trust.simulator.controller;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import trust.simulator.api.AuditorApi;
import trust.simulator.api.IDPApi;
import trust.simulator.api.MSPApi;
import trust.simulator.api.SPApi;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.MSP;
import trust.simulator.entity.Message;
import trust.simulator.entity.SP;
import trust.simulator.entity.User;
import trust.simulator.service.AdminService;
import trust.simulator.service.AuditorService;
import trust.simulator.service.MSPService;
import trust.simulator.service.MessageService;
import trust.simulator.service.SPService;
import trust.simulator.service.UserService;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;
    
    @Autowired
    private UserService userService;
	
    @Autowired
    private SPService spService;
    
    @Autowired
    private MSPService mspService;
    
    @Autowired
    private AuditorService auditorService;
    
    @Autowired
    private MessageService messageService;
    
    private IDPApi idpApi = new IDPApi();
    
    private SPApi spApi = new SPApi();
    
    private MSPApi mspApi = new MSPApi();
    
    private AuditorApi auditorApi = new AuditorApi();
    
    @Value("${idpHost}")
    private String idp_host;
    
    @Value("${idpResetDBPath}")
    private String idp_resetDB_path;
    
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showAdminPage() {
        System.out.println("showAdminPage");
    	return "admin";
    }
    
    @RequestMapping(value = "/resetSimulation", method = RequestMethod.GET)
    public String resetSimulation(HttpSession session) throws ClientProtocolException, URISyntaxException, IOException {
    	
    	List<SP> sps = spService.getAllSps();
    	for(int i=0; i< sps.size(); i++)
    	{
    		spApi.resetSimulation(sps.get(i).getHost(), "/resetDB");
    		spService.deleteSp(sps.get(i));
    	}
    	
    	List<MSP> msps = mspService.getAllMsps();
    	for(int i=0; i< msps.size(); i++)
    	{
    		mspApi.resetSimulation(msps.get(i).getHost(), "/resetDB");
    		mspService.deleteMsp(msps.get(i).getID());
    	}
    	
    	List<Auditor> auditors = auditorService.getAllAuditors();
    	for(int i=0; i< auditors.size(); i++)
    	{
    		auditorApi.resetSimulation(auditors.get(i).getHost(), "/resetDB");
    		auditorService.deleteAuditor(auditors.get(i));
    	}
    	
    	idpApi.resetSimulation(idp_host, idp_resetDB_path);
    	
    	adminService.resetDB();
    	
    	@SuppressWarnings("rawtypes")
		Enumeration e = session.getAttributeNames();
    	while (e.hasMoreElements()) 
    	{
	    	String name = (String)e.nextElement();	
	    	session.removeAttribute(name); 
    	}
    	
    	MSP msp1 = new MSP("MSP 1", "1234", "", "Emails", "MSP1@TrustSimulator.com", "http://localhost:8083/TrustSimulator_MSP1");
    	mspService.saveMsp(msp1);
    	int msp1ID = mspService.findByName("MSP 1").getID();
    	
    	SP sp1 = new SP("SP 1", "1234", "", "http://localhost:8084/TrustSimulator_SP1", "MSP 1", msp1ID, "http://localhost:8083/TrustSimulator_MSP1", "Gold", "SP1@TrustSimulator.com" );
    	spService.saveSp(sp1);
    	
    	SP sp2 = new SP("SP 2", "1234", "", "http://localhost:8084/TrustSimulator_SP2", "", 0, "", "Silver", "SP2@TrustSimulator.com" );
    	spService.saveSp(sp2);
    	
    	SP sp3 = new SP("SP 3", "1234", "", "http://localhost:8086/TrustSimulator_SP3", "", 0, "", "Bronze", "SP3@TrustSimulator.com" );
    	spService.saveSp(sp3);
    	
    	SP sp4 = new SP("SP 4", "1234", "", "http://localhost:8086/TrustSimulator_SP4", "MSP 1", msp1ID, "http://localhost:8083/TrustSimulator_MSP1", "Diamond", "SP4@TrustSimulator.com" );
    	spService.saveSp(sp4);
    	
    	Auditor auditor1 = new Auditor("Auditor 1", "1234", "", "Auditor1@trustSimulator.com", "http://localhost:8085/TrustSimulator_Auditor1");
    	auditorService.saveAuditor(auditor1);
    	
    	User user1 = new User("User 1", "1234", "User1@trustSimulator.com", "0111222333");
    	userService.saveUser(user1);
    	
    	User user2 = new User("User 2", "1234", "User2@trustSimulator.com", "0333222111");
    	userService.saveUser(user2);
    	
    	User user3 = new User("User 3", "1234", "User3@trustSimulator.com", "0222333111");
    	userService.saveUser(user3);
    	
    	User user4 = new User("User 4", "1234", "User4@trustSimulator.com", "");
    	userService.saveUser(user4);
    	
    	User user5 = new User("User 5", "1234", "User5@trustSimulator.com", "0999888777");
    	userService.saveUser(user5);
    	
    	User user6 = new User("User 6", "1234", "User6@trustSimulator.com", "0888999777");
    	userService.saveUser(user6);
    	
    	User user7 = new User("User 7", "1234", "User7@trustSimulator.com", "0777999888");
    	userService.saveUser(user7);
    	
    	User user8 = new User("User 8", "1234", "User8@trustSimulator.com", "0999777888");
    	userService.saveUser(user8);
    	
    	User user9 = new User("User 9", "1234", "User9@trustSimulator.com", "0888777999");
    	userService.saveUser(user9);
    	
    	User user10 = new User("User 10", "1234", "User10@trustSimulator.com", "");
    	userService.saveUser(user10);
    	
    	return "index";
    }
    
    @RequestMapping(value = "/manageUsers", method = RequestMethod.GET)
    public String showManageUsersPage(HttpSession session) {
    	session.setAttribute("ses_page", "manageUsers");
    	return "redirect:/user";
    }
    
 /*   @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session, HttpServletRequest request) throws ClientProtocolException, URISyntaxException, IOException {
    	
    	idpApi.logout(Constants.idp_host, Constants.idp_logout_path);
    	
    	//session = request.getSession(false);
    	if (session != null) session.invalidate();
    	SecurityContextHolder.clearContext();   
    	
    	return "index";
    }
    */
    @RequestMapping(value = "/manageAuditors", method = RequestMethod.GET)
    public String showManageAuditorsPage(HttpSession session) {
    	session.setAttribute("ses_page", "manageAuditors");
    	return "redirect:/auditor";
    }
    
    @RequestMapping(value = "/addMessage", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> addMessage(@RequestParam("message") String message) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject messageJson = (JsonObject)parser.parse(message);
    	Message message2 = Message.fromJson(messageJson);
        
    	Message message3 = new Message(message2.getSenderEmail(), message2.getSenderName(), message2.getReceiverEmail(), message2.getReceiverName(), message2.getTitle(), message2.getBody(), message2.getStatus());
    	message3.setCreatedOn(message2.getCreatedOn());
    	
    	messageService.saveMessage(message3);
    	
        return new ResponseEntity<String>(Integer.toString(message3.getMessageID()),HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getSPs", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getSPs() {
     	
    	JsonArray jsonArray = new JsonArray();    	
    	List<SP> sps = spService.getAllSps();
    	for(int i=0; i< sps.size(); i++)
    	{
    		JsonObject sp = sps.get(i).toJson();
    		jsonArray.add(sp);
    	}
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("sps", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
    	
        return new ResponseEntity<String>(jsonArrayString,HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getMSPs", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getMSPs() {
     	
    	JsonArray jsonArray = new JsonArray();    	
    	List<MSP> msps = mspService.getAllMsps();
    	for(int i=0; i< msps.size(); i++)
    	{
    		JsonObject msp = msps.get(i).toJson();
    		jsonArray.add(msp);
    	}
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("msps", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
    	
        return new ResponseEntity<String>(jsonArrayString,HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getMSP", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getMSP(@RequestParam("mspID") String mspID) {
     		
    	int mspIDint = Integer.valueOf(mspID);
    	
    	//System.out.println("maspID: "+mspID);
    	
    	MSP msp = mspService.findByMspID(mspIDint);
    	
    	JsonObject mspJson = msp.toJson();
    		
    	String jsonString = mspJson.toString();
    	
        return new ResponseEntity<String>(jsonString,HttpStatus.OK);
    }
    
    @RequestMapping(value = "/persistSP", method = RequestMethod.POST)
    public void persistSP(@RequestParam("action_sp") String action_sp) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_sp);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_sp"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	SP sp = SP.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) spService.simpleSaveSp_Log(sp);
    	else if(action.equals("update")) spService.simpleUpdateSp_Log(sp);
    	else if(action.equals("delete")) spService.simpleDeleteSp_Log(sp);
    }
    
    @RequestMapping(value = "/persistMSP", method = RequestMethod.POST)
    public void persistMSP(@RequestParam("action_msp") String action_msp) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_msp);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_msp"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	MSP msp = MSP.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) mspService.simpleSaveMsp_Log(msp);
    	else if(action.equals("update")) mspService.simpleUpdateMsp_Log(msp);
    	else if(action.equals("delete")) mspService.simpleDeleteMsp_Log(msp);
    }
    
    @RequestMapping(value = "/persistAuditor", method = RequestMethod.POST)
    public void persistAuditor(@RequestParam("action_auditor") String action_auditor) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_auditor);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_auditor"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	Auditor auditor = Auditor.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) auditorService.simpleSaveAuditor_Log(auditor);
    	else if(action.equals("update")) auditorService.simpleUpdateAuditor_Log(auditor);
    	else if(action.equals("delete")) auditorService.simpleDeleteAuditor_Log(auditor);
    }
    
}
