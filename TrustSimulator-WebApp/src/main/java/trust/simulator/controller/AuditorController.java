package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.Auditor;
import trust.simulator.service.AuditorService;
import trust.simulator.util.Operations;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
public class AuditorController {

    @Autowired
    private AuditorService auditorService;
    
  
    @RequestMapping(value = "/auditor", method = RequestMethod.GET)
    public String showAuditorPage(Model model, HttpSession session) {
        model.addAttribute("auditor",new Auditor());
        
        List<Auditor> auditors = auditorService.getAllAuditors();
        model.addAttribute("auditors", auditors);
        
        return "manageAuditors";
    }

    @RequestMapping(value = "/addAuditor", method = RequestMethod.POST)
    public String addAuditor( Model model, Auditor auditor, HttpSession session) {
        Auditor existing = auditorService.findByName(auditor.getName());
        if (existing != null) {
            model.addAttribute("status", "exist");
            List<Auditor> auditors = auditorService.getAllAuditors();
            model.addAttribute("auditors", auditors);
            return "manageAuditors";
        }
        auditor.setCreatedOn(Operations.getCurrentTime());
        auditor.setModifiedOn(Operations.getCurrentTime());
        //auditor.setAuditorID(auditorService.getNewId());
        auditorService.saveAuditor(auditor);
        model.addAttribute("saved", "success");
        model.addAttribute("auditor",new Auditor());
        List<Auditor> auditors = auditorService.getAllAuditors();
        model.addAttribute("auditors", auditors);
        return "manageAuditors";
    }

    @RequestMapping(value = "/editAuditor_{auditorID}", method = RequestMethod.GET)
    public String editAuditor(Model model, @PathVariable int auditorID, HttpSession session) {
        Auditor auditor = auditorService.findByAuditorID(auditorID);
        model.addAttribute("auditor", auditor);
        model.addAttribute("request", "edit");
        return "manageAuditors";
    }

    @RequestMapping(value = "/editAuditor_2", method = RequestMethod.POST)
    public String editAuditor_2( Model model, Auditor auditor, HttpSession session) {
    	auditor.setModifiedOn(Operations.getCurrentTime());
        auditorService.updateAuditor(auditor);
        model.addAttribute("updated", "success");
        model.addAttribute("auditor",new Auditor());
        List<Auditor> auditors = auditorService.getAllAuditors();
        model.addAttribute("auditors", auditors);
        
        return "manageAuditors";
    }
    
    @RequestMapping(value = "/deleteAuditor_{auditorID}", method = RequestMethod.GET)
    public String deleteAuditor(Model model, @PathVariable int auditorID, HttpSession session) {
    	Auditor auditor = auditorService.findByAuditorID(auditorID);
    	auditorService.deleteAuditor(auditor);
        model.addAttribute("deleted", "success");
        model.addAttribute("auditor", new Auditor());
        List<Auditor> auditors = auditorService.getAllAuditors();
        model.addAttribute("auditors", auditors);
        return "manageAuditors";
    }
    
    @RequestMapping(value = "/getExpandedAuditor_{auditorID}", method = RequestMethod.GET)
    public String getExpndedAuditor(Model model, HttpSession session, @PathVariable int auditorID) {
    	    	
    	Auditor auditor = auditorService.findByAuditorID(auditorID);
    	model.addAttribute("auditor", auditor);
    	
        model.addAttribute("request", "showExpandedAuditor");

        return "manageAuditors";
    }



}
