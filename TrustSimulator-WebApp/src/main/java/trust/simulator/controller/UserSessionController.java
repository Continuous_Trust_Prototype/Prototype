package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.Auditor;
import trust.simulator.entity.Message;
import trust.simulator.entity.SP;
import trust.simulator.entity.User;
import trust.simulator.service.AuditorService;
import trust.simulator.service.MessageService;
import trust.simulator.service.SPService;
import trust.simulator.service.UserService;

import java.util.List;

import javax.servlet.http.HttpSession;


@Controller
public class UserSessionController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private SPService spService;
    
    @Autowired
    private MessageService messageService;
    
    @Autowired
    private AuditorService auditorService;
     
    @RequestMapping(value = "/resumeUserSession_{userID}", method = RequestMethod.GET)
    public String resumeUserSession(Model model, @PathVariable int userID, HttpSession session) {
        User user = userService.findByUserID(userID);
        model.addAttribute("user", user);
        
        List<SP> sps = spService.getAllSps();
        model.addAttribute("sps", sps);
        
        session.setAttribute("logedinUser", "true");
        session.setAttribute("logedinUserID", Integer.toString(user.getID()));
        session.setAttribute("logedinUserName", user.getName());
        
        model.addAttribute("messagesNotification", userService.getMessagesNotification(user));
        
        return "userSession";
    }
    
    /*Not Used -- replaced with j_spring_security_logout
    @RequestMapping(value = "/logoutUserSession_{userID}", method = RequestMethod.GET)
    public String logoutUserSession(Model model, @PathVariable int userID, HttpSession session) {
               
    	Enumeration e = session.getAttributeNames();
    	while (e.hasMoreElements()) 
    	{
	    	String name = (String)e.nextElement();	
	    	session.removeAttribute(name); 
    	}
    	
        return "index";
    }*/
    
    @RequestMapping(value = "/getService_{spID}", method = RequestMethod.GET)
    public String getService(Model model, @PathVariable int spID, HttpSession session) {
    	    	
    	int userID = Integer.parseInt((String) session.getAttribute("logedinUserID"));
    	User user = userService.findByUserID(userID);
        model.addAttribute("user", user);
        SP sp = spService.findBySpID(spID);
        model.addAttribute("sp", sp);
        model.addAttribute("request", "getService");
        model.addAttribute("messagesNotification", userService.getMessagesNotification(user));
        return "userSession";
    }
    
    @RequestMapping(value = "/inbox_{userID}", method = RequestMethod.GET)
    public String showUserInbox(Model model, @PathVariable int userID, HttpSession session) {
        
    	model.addAttribute("request", "getInbox");
    	
    	User user = userService.findByUserID(userID);
        model.addAttribute("user", user);
        
        List<Message> messages = userService.getUserMessages(user);
        model.addAttribute("messages", messages);
        
        session.setAttribute("logedinUser", "true");
        session.setAttribute("logedinUserID", Integer.toString(user.getID()));
        session.setAttribute("logedinUserName", user.getName());
        
        model.addAttribute("messagesNotification", userService.getMessagesNotification(user));
        
        return "userSession";
    }
    
    
    @RequestMapping(value = "/showMessage_{messageID}", method = RequestMethod.GET)
    public String showMessage(Model model, @PathVariable int messageID, HttpSession session) {
        
    	int userID = Integer.parseInt((String) session.getAttribute("logedinUserID"));
    	User user = userService.findByUserID(userID);
    	model.addAttribute("user", user);
    	
    	model.addAttribute("request", "showMessage");
    
    	Message message = messageService.findByMessageID(messageID);
        message.setStatus("read");
        messageService.updateMessage(message);
        model.addAttribute("message", message);
    	
        model.addAttribute("messagesNotification", userService.getMessagesNotification(user));
        
        return "userSession";
    }

    @RequestMapping(value = "/reportToAuditor_{messageID}", method = RequestMethod.GET)
    public String reportToAuditor(Model model, @PathVariable int messageID, HttpSession session) {
    	
    	int userID = Integer.parseInt((String) session.getAttribute("logedinUserID"));
    	User user = userService.findByUserID(userID);
    	model.addAttribute("user", user);
    	
    	model.addAttribute("request", "reportToAuditor");
    	
        Message message = messageService.findByMessageID(messageID);
        model.addAttribute("message", message);
        
        String messageString = message.toJson().toString();
        System.out.println("messageString: "+messageString);
        model.addAttribute("messageString", messageString);
        
        List<Auditor> auditors = auditorService.getAllAuditors();
        model.addAttribute("auditors", auditors);
        
        model.addAttribute("messagesNotification", userService.getMessagesNotification(user));
        
        return "userSession";
    }
          
}
