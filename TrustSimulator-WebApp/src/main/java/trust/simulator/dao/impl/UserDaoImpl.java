package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.persistence.Query;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.IDPApi;
import trust.simulator.dao.UserDao;
import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.User;
import trust.simulator.entity.UserLog;
import trust.simulator.service.UserService;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, String> implements UserDao {

    protected UserDaoImpl() {
        super(User.class);
    }
    
    IDPApi idpApi = new IDPApi();
    
    @Value("${idpHost}")
    private String idp_host;
    
    @Value("${idpPersistEndUserPath}")
    private String idp_persistEndUser_path;
    
    @Autowired
    private UserService userService;
    
	//Log Events
    @Override
    public User save(User e) {
        
    	getCurrentSession().persist(e);
        
    	UserLog userLog = new UserLog(e, "save");
        getCurrentSession().persist(userLog);
        
        try {
        	
			idpApi.persistEndUser(idp_host, idp_persistEndUser_path, e, "save");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    
    }
    
    //Log Events
    @Override
    public void update(User e) {
        getCurrentSession().merge(e);
       
        UserLog userLog = new UserLog(e, "update");
        getCurrentSession().persist(userLog);
        
        try {
        	
			idpApi.persistEndUser(idp_host, idp_persistEndUser_path, e, "update");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(User e) {	
    
    	try {
        	
			idpApi.persistEndUser(idp_host, idp_persistEndUser_path, e, "delete");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    	
    	User user = getCurrentSession().getReference(User.class, e.getID());
    	getCurrentSession().remove(user);

    	UserLog userLog = new UserLog(e, "remove");
        getCurrentSession().persist(userLog);
        
        getCurrentSession().flush();
    	
    }
    
      
    @Override
	public List<AbstractLog> getAllUserLogs(int userID) {
    	
    	User user = userService.findByUserID(userID);
    	String userEmail = user.getEmail();
    	
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractLog where "
    			+"entityID = '"+userID+"' OR  auditorcaseUserID = '"+userID+"' OR mspdataUserID = '"+userID+"' "
    			+"OR spdataUserID = '"+userID+"' OR messageReceiverEmail = '"+userEmail+"' OR "
    			+"messageSenderEmail = '"+userEmail+"' OR IDPUserSub = '"+user.getSub()+"'", AbstractLog.class);
    	
    	@SuppressWarnings("unchecked")
		List<AbstractLog> resultList = query.getResultList();
		return resultList;    	
	}
    
    @Override
	public List<AbstractLog> getAllUserLogs(int userID, String untilTime) {
    	
    	User user = userService.findByUserID(userID);
    	String userEmail = user.getEmail();
    	
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractLog where ("
    			+"entityID = '"+userID+"' OR  auditorcaseUserID = '"+userID+"' OR mspdataUserID = '"+userID+"' "
    			+"OR spdataUserID = '"+userID+"' OR messageReceiverEmail = '"+userEmail+"' OR "
    			+"messageSenderEmail = '"+userEmail+"' OR IDPUserSub = '"+user.getSub()+"') AND createdOn > '"+untilTime+"'", AbstractLog.class);
    	
    	@SuppressWarnings("unchecked")
		List<AbstractLog> resultList = query.getResultList();
		return resultList;    	
	}


}
