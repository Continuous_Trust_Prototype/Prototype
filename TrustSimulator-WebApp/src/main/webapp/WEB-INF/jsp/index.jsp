<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="index" scope="session" />
<c:set var="ses_headerTitle" value="TrustSimulator: Protecting you in World WILD Web!" scope="session" />
<c:set var="ses_pageTitle" value="TrustSimulator: Protecting you in World WILD Web!" scope="session" />
<c:set var="ses_pageDescription" value="TrustSimulator is a simulation enviroment to gauge the effectiveness of certain trust algorithms." scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
<jsp:include page="common/head.jsp"/>
</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<div id="mainA">

<h1>This WebApp represents the main interface for the simulation environment where the simulation admin can do tasks like:</h1>

<dl>

<dt>Add/Edit/Delete simulation entities like Users, SPs, MSPs, or Auditors.</dt>

<dt>View the aggregated logs from all the entities to compare it with the logs and decisions made by the simulated entities.</dt> 

<dt>Start user simulation sessions where a user would interact with the system deployed entities.</dt> 

</dl>

</div>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>
