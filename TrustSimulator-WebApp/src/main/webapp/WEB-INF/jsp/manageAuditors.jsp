<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageAuditors" scope="session" />
<c:set var="ses_headerTitle" value="Manage Auditors..." scope="session" />
<c:set var="ses_pageTitle" value="Manage Auditors..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Add / Edit / Delete Auditors:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'showExpandedAuditor'}">
<div id="mainA">
	
		<table class="recordsTable">
			<tr class='recordsHeader'>
				<td>Auditor ID</td>
				<td>Name</td>
				<td colspan='2'>Email</td>
			</tr>
			<tr>
				<td>${auditor.ID}</td>
				<td>${auditor.name}</td>
				<td colspan='2'>${auditor.email}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='2'>Description</td>
				<td colspan='2'>Host</td>
			</tr>
			<tr>
				<td colspan='2'>${auditor.description}</td>
				<td colspan='2'>${auditor.host}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='2'>Created On</td>
				<td colspan='2'>Last Modified</td>
			</tr>
			<tr>
				<td colspan='2'>${auditor.createdOn}</td>
				<td colspan='2'>${auditor.modifiedOn}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='4'>Actions</td>
			</tr>
			<tr>
				<td colspan='4'><a href="${auditor.host}/resumeAuditorSession_${auditor.ID}">Resume</a> : <a href="/TrustSimulator/editAuditor_${auditor.ID}">Edit</a> : <a href="/TrustSimulator/deleteAuditor_${auditor.ID}">Delete</a></td>
			</tr>
			
		</table>
		
		<h2><a href="/TrustSimulator/manageAuditors">Go back to the Manage Auditors page</a></h2>
		
		
	</div>
</c:when>


<c:when test="${request == 'edit'}">
<div id="mainA">

	<h2>Edit a Current Auditor</h2>
	<form:form modelAttribute="auditor" action="/TrustSimulator/editAuditor_2" method="post">
	<form:input type="hidden" path="ID" value="${auditor.ID}"/>
	<form:input type="hidden" path="name" value="${auditor.name}"/>
	<form:input type="hidden" path="createdOn" value="${auditor.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${auditor.modifiedOn}"/>
	<form:input type="hidden" path="email" value="${auditor.email}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>Auditor ID</td></form:label>
			<td colspan='4' class='cellData'> ${auditor.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${auditor.name} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${auditor.password}"/></td>
		</tr>

		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${auditor.email} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" value="${auditor.host}"/></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" value="${auditor.description}" /></td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update Auditor</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>

<c:otherwise>
<div id="mainA">

	<h2>Add a New Auditor</h2>
		<c:if test="${saved == 'success'}">
			<p class="success">Auditor Created Successfully</p>
		</c:if>
		<c:if test="${updated == 'success'}">
			<p class="success">Auditor Updated Successfully</p>
		</c:if>
		<c:if test="${deleted == 'success'}">
			<p class="success">Auditor Deleted Successfully</p>
		</c:if>
		<c:if test="${status == 'exist'}">
			<p class="error">Auditor Already Exist</p>
		</c:if>
	<form:form modelAttribute="auditor" action="/TrustSimulator/addAuditor" method="post">
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="name" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" /></td>
		</tr>

		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="email" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" /></td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Add Auditor</button></td>
		</tr>
			
	</table>
	</form:form>

	<h2>Existing Auditors</h2>
	
	<table class="recordsTable">	
	
	<tr class='recordsHeader'>
	<th>Name</th>
	<th>Email</th>
	<th>Host</th>
	<th>Description</th>
	<th>Actions</th>
	</tr>
	<c:forEach var="current" items="${auditors}">
		<tr>
		<td>${current.name}</td>
		<td>${current.email}</td>
		<td>${current.host}</td>
		<td>${current.description}</td>
		<td><a href="/TrustSimulator/getExpandedAuditor_${current.ID}">Expand</a> : <a href="${current.host}/resumeAuditorSession_${current.ID}">Resume</a> : <a href="/TrustSimulator/editAuditor_${current.ID}">Edit</a> : <a href="/TrustSimulator/deleteAuditor_${current.ID}">Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</div>
</c:otherwise>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>