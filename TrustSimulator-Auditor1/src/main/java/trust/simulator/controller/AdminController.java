package trust.simulator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.Auditor;
import trust.simulator.service.AdminService;
import trust.simulator.service.AuditorService;

@Controller
public class AdminController {
	
    @Autowired
    private AuditorService auditorService;
    
    @Autowired
    private AdminService adminService;
    
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showAdminPage() {
    	return "admin";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(Model model) {
        model.addAttribute("auditor",new Auditor());
        return "login";
    }
    
    @RequestMapping(value = "/resetDB", method = RequestMethod.GET)
    public String resetDB() {
    	
    	adminService.resetDB();
    	
    	return "admin";
    }
    
    @RequestMapping(value = "/login_2", method = RequestMethod.POST)
    public String confirmLogin(Model model, Auditor auditor, HttpSession session) {
    	Auditor existing = auditorService.findByName(auditor.getName());
    	
    	if (existing != null && existing.getPassword().equals(auditor.getPassword()) ) {   	
    		
             model.addAttribute("status", "exist");
             model.addAttribute("request", "greet");
             session.setAttribute("logedinAuditor", "true");
             session.setAttribute("logedinAuditorID", Integer.toString(existing.getID()));
             session.setAttribute("logedinAuditorName", existing.getName());
             return "login";
        }
    	
        model.addAttribute("auditor",new Auditor());
        model.addAttribute("status", "Does not exist");
        
        return "login";
    }
        
}
