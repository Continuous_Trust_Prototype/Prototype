package trust.simulator.controller;

import org.apache.http.client.ClientProtocolException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.mitre.openid.connect.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.api.IDPApi;
import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorCase;
import trust.simulator.entity.Message;
import trust.simulator.service.AuditorCaseService;
import trust.simulator.service.AuditorService;
import trust.simulator.service.MessageService;
import trust.simulator.util.Operations;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpSession;


@Controller
public class UserToAuditorController {
    
    @Autowired
    private MessageService messageService;
    
    @Autowired
    private AuditorService auditorService;
    
    @Autowired
    private AuditorCaseService auditorcaseService;
    
    private IDPApi idpApi = new IDPApi();
    
    @Value("${idpHost}")
    private String idp_host;
    
    @Value("${idpUserInfoPath}")
    private String idp_userInfo_path;
    
    @Value("${idpUserGetUserLogsPath}")
    private String idp_getUserLogs_path;

    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorUserPath}")
    private String simulator_user_path;
    
    @RequestMapping(value = "/reportSpam_{auditorID}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_END_USER')")
    public String reportSpam(Model model, @PathVariable("auditorID") int auditorID, HttpSession session, @RequestParam String messageString) throws ClientProtocolException, URISyntaxException, IOException {
    	
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	OIDCAuthenticationToken token = (OIDCAuthenticationToken) auth;
    	
    	String access_token = token.getAccessTokenValue();	
    	
    	UserInfo userInfo = idpApi.getUserInfo(idp_host, idp_userInfo_path, access_token);
    	
    	int userID = Operations.getIdFromSub((userInfo.getSub()));
    	
    	//TODO load client's detail from local DB instead of searching 
    	Auditor auditor = auditorService.findByAuditorID(auditorID);
    	model.addAttribute("auditor", auditor);
    	
    	//TODO try to make Message a claim associated with the access_token	
    	JsonParser parser = new JsonParser();
    	JsonObject messageJson = (JsonObject)parser.parse(messageString);
    	Message message = Message.fromJson(messageJson);
        messageService.saveMessage(message);
     
        AuditorCase auditorcase = new AuditorCase(auditor.getID(), "email", userInfo.getEmail(), userID, userInfo.getName(), message.getMessageID(), "Open");
        auditorcaseService.saveAuditorCase(auditorcase);
        
        List<AbstractLog> caseLogs = idpApi.getUserLogs(idp_host, idp_getUserLogs_path, userID, message.getCreatedOn());
        
        for(int i=0; i<caseLogs.size(); i++)
        {
        	System.out.println("Log "+i+" = "+caseLogs.get(i).toString());
        }
        
        //TODO make this URL fetchable from the token instead of hardcoding it
        model.addAttribute("userHome", simulator_host+simulator_user_path);
    	
        model.addAttribute("request", "reportSpam");
    	return "userSession";
    }

}
