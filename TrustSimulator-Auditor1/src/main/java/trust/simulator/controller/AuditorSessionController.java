package trust.simulator.controller;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.api.SimulatorApi;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorCase;
import trust.simulator.entity.Message;
import trust.simulator.entity.SP;
import trust.simulator.service.AuditorCaseService;
import trust.simulator.service.AuditorService;
import trust.simulator.service.MessageService;
import trust.simulator.service.SPService;
import trust.simulator.util.Operations;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
public class AuditorSessionController {
    
    @Autowired
    private SPService spService;
    
    @Autowired
    private MessageService messageService;
    
    @Autowired
    private AuditorService auditorService;
    
    @Autowired
    private AuditorCaseService auditorcaseService;
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorGetSPsPath}")
    private String simulator_getSPs_path;
     
    @RequestMapping(value = "/resumeAuditorSession_{auditorID}", method = RequestMethod.GET)
    public String resumeUserSession(Model model, @PathVariable int auditorID, HttpSession session) {

    	Auditor auditor = auditorService.findByAuditorID(auditorID);
        model.addAttribute("auditor", auditor);
      
        List<AuditorCase> auditorcases = auditorcaseService.getAllAuditorCases(auditorID);
        model.addAttribute("auditorcases", auditorcases);
        
        //session.setAttribute("logedinAuditor", "true");
        //session.setAttribute("logedinAuditorID", Integer.toString(auditor.getID()));
        //session.setAttribute("logedinAuditorName", auditor.getName());
        
        return "auditorSession";
    }
    
    @RequestMapping(value = "/getAuditorCase_{auditorcaseID}", method = RequestMethod.GET)
    public String getAuditorCase(Model model, @PathVariable int auditorcaseID, HttpSession session) {
    	
    	AuditorCase auditorcase = auditorcaseService.findByAuditorCaseID(auditorcaseID);
    	model.addAttribute("auditorcase", auditorcase);
    	
    	Message message = messageService.findByMessageID(auditorcase.getAuditorcaseMessageID());
    	model.addAttribute("message", message);
    	
    	//Auditor auditor = auditorcase.getAuditorcaseAuditorID();
    	Auditor auditor = auditorService.findByAuditorID(auditorcase.getAuditorcaseAuditorID());
        model.addAttribute("auditor", auditor);
    	
    	model.addAttribute("request", "getAuditorCase");
    
        return "auditorSession";
    }
    
    @RequestMapping(value = "/updateAuditorCase_{auditorcaseID}", method = RequestMethod.GET)
    public String updateAuditorCase(Model model, @PathVariable int auditorcaseID, HttpSession session) throws ClientProtocolException, URISyntaxException, IOException {
    	
    	AuditorCase auditorcase = auditorcaseService.findByAuditorCaseID(auditorcaseID);
    	model.addAttribute("auditorcase", auditorcase);
    	
    	Message message = messageService.findByMessageID(auditorcase.getAuditorcaseMessageID());
    	model.addAttribute("message", message);
    	      
    	List<SP> sps = simulatorApi.getSPs(simulator_host, simulator_getSPs_path);
        model.addAttribute("sps", sps);

        Auditor auditor = auditorService.findByAuditorID(auditorcase.getAuditorcaseAuditorID());
        model.addAttribute("auditor", auditor);
        
        session.setAttribute("form_message",auditorcase.getAuditorcaseMessageID());
        session.setAttribute("form_user", auditorcase.getAuditorcaseUserID());
        
        model.addAttribute("request", "updateAuditorCase");
        return "auditorSession";
    }
    
    @RequestMapping(value = "/updateAuditorCase_2", method = RequestMethod.POST)
    public String updateAuditorCase2(Model model, AuditorCase auditorcase, HttpSession session, HttpServletRequest request) {
    	
    	if(request.getParameter("convictedSP") != null)
    	{
	    	int convictedSP = Integer.parseInt(request.getParameter("convictedSP"));
	        auditorcase.setConvictedSPID(convictedSP);
    	}
    	
    	int auditorID = Integer.parseInt((String) request.getParameter("auditorID"));
    	Auditor auditor = auditorService.findByAuditorID(auditorID);
    	
    	auditorcase.setAuditorcaseAuditorID(auditor.getID());
    	
    	int messageID = Integer.parseInt(request.getParameter("messageID"));
    	Message message = messageService.findByMessageID(messageID);
    	auditorcase.setAuditorcaseMessageID(message.getMessageID());
    	
        auditorcase.setModifiedOn(Operations.getCurrentTime());
        auditorcaseService.updateAuditorCase(auditorcase);
        model.addAttribute("updated", "success");
        
        model.addAttribute("auditorcase", auditorcase);
        model.addAttribute("message", message);
        
        model.addAttribute("request", "getAuditorCase");
        return "auditorSession";
    }
    
    @RequestMapping(value = "/updateAuditorCaseUser_{auditorcaseID}", method = RequestMethod.GET)
    public String updateAuditorCaseUser(Model model, @PathVariable int auditorcaseID, HttpSession session) {
    	    	
    	AuditorCase auditorcase = auditorcaseService.findByAuditorCaseID(auditorcaseID);
    	model.addAttribute("auditorcase", auditorcase);

    	Auditor auditor = auditorService.findByAuditorID(auditorcase.getAuditorcaseAuditorID());

    	//Message caseMessage = auditorcase.getAuditorcaseMessageID();
    	Message caseMessage = messageService.findByMessageID(auditorcase.getAuditorcaseMessageID());
    	String userName = auditorcase.getAuditorcaseUserName();
    	model.addAttribute("message", caseMessage);
    	
        Message message = new Message(auditor.getEmail(), auditor.getName(),caseMessage.getReceiverEmail(), userName, Operations.setMessageAuditorUpdateTitle(auditor.getName()), Operations.setMessageAuditorUpdateBody(auditorcase, auditor, caseMessage), "unread");
        messageService.sendMessage(message);
        
        model.addAttribute("userUpdated", "success");
        
        model.addAttribute("request", "getAuditorCase");
        
        model.addAttribute("auditor", auditor);
        
        return "auditorSession";
    }   

}
