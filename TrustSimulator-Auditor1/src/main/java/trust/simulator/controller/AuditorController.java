package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.entity.Auditor;
import trust.simulator.service.AuditorService;
import trust.simulator.util.Operations;

import javax.servlet.http.HttpSession;

@Controller
public class AuditorController {

    @Autowired
    private AuditorService auditorService;
    
    @RequestMapping(value = "/editAuditor_{auditorID}", method = RequestMethod.GET)
    public String editAuditor(Model model, @PathVariable int auditorID, HttpSession session) {
        Auditor auditor = auditorService.findByAuditorID(auditorID);
        model.addAttribute("auditor", auditor);
        model.addAttribute("request", "edit");
        return Operations.routeView(session, "manageAuditors", "auditor");
    }

    @RequestMapping(value = "/editAuditor_2", method = RequestMethod.POST)
    public String editAuditor_2( Model model, Auditor auditor, HttpSession session) {
    	
    	auditor.setModifiedOn(Operations.getCurrentTime());
        
    	auditorService.updateAuditor(auditor);
        
    	model.addAttribute("updated", "success");
    	model.addAttribute("request", "edit");
        model.addAttribute("auditor", auditor);
        
        return "manageAuditors";
    }
    
    @RequestMapping(value = "/persistAuditor", method = RequestMethod.POST)
    public void persistAuditor(@RequestParam("action_auditor") String action_auditor) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_auditor);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_auditor"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	Auditor auditor = Auditor.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) auditorService.simpleSaveAuditor_Log(auditor);
    	else if(action.equals("update")) auditorService.simpleUpdateAuditor_Log(auditor);
    	else if(action.equals("delete")) auditorService.simpleDeleteAuditor_Log(auditor);
    }


}
