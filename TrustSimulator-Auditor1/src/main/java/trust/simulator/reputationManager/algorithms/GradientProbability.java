package trust.simulator.reputationManager.algorithms;

import java.util.ArrayList;
import java.util.List;

public class GradientProbability {

	class RankItem{
		
		private String item;
		private float rank;
		
		public String getItem() {
			return item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public float getRank() {
			return rank;
		}
		public void setRank(float rank) {
			this.rank = rank;
		}
	}
	
/*	class CaseList{
		
		private String item;
		private int order;
		
		public String getItem() {
			return item;
		}
		public void setItem(String item) {
			this.item = item;
		}
		public int getOrder() {
			return order;
		}
		public void setOrder(int order) {
			this.order = order;
		}
	}
	*/
	
	public List<RankItem> generateRanks(List<String> caseList)
	{
		List<RankItem> rankingList = new ArrayList<RankItem>();
		List<RankItem> finalRankingList = new ArrayList<RankItem>();
		
		float listSize = caseList.size();
		float evenDistr = 1/listSize;
		
		//System.out.println("listSize: "+ listSize);
		//System.out.println("evenDistr: "+ evenDistr);
		
		float iniRank = 0;
		float iniRankSum = 0;
		
		for(int i=0; i<listSize;i++)
		{
			
			iniRank = evenDistr * (listSize-i);
			iniRankSum = iniRankSum + iniRank;
			
			//System.out.println("iniRank: "+ iniRank);
			//System.out.println("iniRankSum: "+ iniRankSum);
			
			RankItem rankItem = new RankItem();
			rankItem.setItem(caseList.get((int) (listSize-i-1)));
			rankItem.setRank(iniRank);
			rankingList.add(rankItem);
			
		}
		
		for(int i=0; i<listSize;i++)
		{
			RankItem rankItem = rankingList.get(i);
			
			rankItem.setRank(rankItem.getRank()/iniRankSum);
			
			rankingList.set(i, rankItem);
			
			boolean itemExist = false;
			for(int j = 0; j < finalRankingList.size(); j++)
			{
				if(rankItem.getItem().equals(finalRankingList.get(j).getItem()))
				{
					float currentRank = finalRankingList.get(j).getRank();
					finalRankingList.get(j).setRank(currentRank+rankItem.getRank());
					itemExist = true;
					break;
				}
			}
			if(!itemExist)
				finalRankingList.add(rankItem);
			

		}
		
		return finalRankingList;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		List<String> caseList = new ArrayList<String>();
		caseList.add("SP 1");
		caseList.add("SP 3");
		caseList.add("SP 4");
		caseList.add("SP 1");
		//caseList.add("SP 1");
		//caseList.add("SP 1");
		//caseList.add("SP 1");
		
		GradientProbability ranker = new GradientProbability();
		
		List<RankItem> rankedList = ranker.generateRanks(caseList);
		
		for(int i = 0; i < rankedList.size(); i++)
		{
			System.out.println(rankedList.get(i).getItem()+" Rank: "+ rankedList.get(i).getRank() );
		}


	}

}
