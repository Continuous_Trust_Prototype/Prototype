package trust.simulator.dao;

import java.util.List;

import trust.simulator.entity.Message;
import trust.simulator.service.MessagesNotification;

public interface MessageDao extends AbstractDao<Message, String> {

	void send(Message e);
	MessagesNotification getMessagesNotification(String receiverName);
	List<Message> getUserMessages(String receiverName);

}
