package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.IDPApi;
import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.AuditorDao;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorLog;

@Repository
public class AuditorDaoImpl extends AbstractDaoImpl<Auditor, String> implements AuditorDao {

    protected AuditorDaoImpl() {
        super(Auditor.class);
    }
    
    private IDPApi idpApi = new IDPApi();
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${idpHost}")
    private String idp_host;
    
    @Value("${idpPersistAuditorPath}")
    private String idp_persistAuditor_path;
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorPersistAuditorPath}")
    private String simulator_persistAuditor_path;
    
	//Log Events
    @Override
    public Auditor save(Auditor e) {
        
    	getCurrentSession().persist(e);
        
    	AuditorLog auditorLog = new AuditorLog(e, "save");
        getCurrentSession().persist(auditorLog);
        
        try {
        	
			idpApi.persistAuditor(idp_host, idp_persistAuditor_path, e, "save");
			simulatorApi.persistAuditor(simulator_host, simulator_persistAuditor_path, e, "save");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(Auditor e) {
    	
        getCurrentSession().merge(e);
        
        AuditorLog auditorLog = new AuditorLog(e, "update");
        getCurrentSession().persist(auditorLog);
        
        try {
        	
			idpApi.persistAuditor(idp_host, idp_persistAuditor_path, e, "update");
			simulatorApi.persistAuditor(simulator_host, simulator_persistAuditor_path, e, "update");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(Auditor e) {
    	
    	try {
        	
			idpApi.persistAuditor(idp_host, idp_persistAuditor_path, e, "delete");
			simulatorApi.persistAuditor(simulator_host, simulator_persistAuditor_path, e, "delete");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().remove(e);
       
        AuditorLog auditorLog = new AuditorLog(e, "remove");
        getCurrentSession().persist(auditorLog);
    }
    
	//Log Events
    @Override
    public void simpleSave_Log(Auditor e) {
        
    	getCurrentSession().persist(e);
        
    	AuditorLog auditorLog = new AuditorLog(e, "save");
        getCurrentSession().persist(auditorLog);
        
        getCurrentSession().flush();
      
     }
    
  //Log Events
    @Override
    public void simpleUpdate_Log(Auditor e) {
    	
    	getCurrentSession().merge(e);
        
    	AuditorLog auditorLog = new AuditorLog(e, "update");
        getCurrentSession().persist(auditorLog);
        
        getCurrentSession().flush();
        
    }

    //Log Events
    @Override
    public void simpleDelete_Log(Auditor e) {
                
    	Auditor auditor = getCurrentSession().getReference(Auditor.class, e.getID());
        getCurrentSession().remove(auditor);
        
        AuditorLog auditorLog = new AuditorLog(e, "remove");
        getCurrentSession().persist(auditorLog);
        
        getCurrentSession().flush();
    }


}
