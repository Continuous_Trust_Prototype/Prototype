package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.AuditorCaseDao;
import trust.simulator.entity.AuditorCase;
import trust.simulator.entity.AuditorCaseLog;

@Repository
public class AuditorCaseDaoImpl extends AbstractDaoImpl<AuditorCase, String> implements AuditorCaseDao {

    protected AuditorCaseDaoImpl() {
        super(AuditorCase.class);
    }
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorAddAuditorCaseLogPath}")
    private String simulator_aadAuditorCaseLog_path;

	//Log Events
    @Override
    public AuditorCase save(AuditorCase e) {
        getCurrentSession().persist(e);
        AuditorCaseLog auditorCaseLog = new AuditorCaseLog(e, "save");
        getCurrentSession().persist(auditorCaseLog);
        
        try {
        	simulatorApi.addAuditorCaseLog(auditorCaseLog, simulator_host, simulator_aadAuditorCaseLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(AuditorCase e) {
        getCurrentSession().merge(e);
        AuditorCaseLog auditorCaseLog = new AuditorCaseLog(e, "update");
        getCurrentSession().persist(auditorCaseLog);
        try {
        	simulatorApi.addAuditorCaseLog(auditorCaseLog, simulator_host, simulator_aadAuditorCaseLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(AuditorCase e) {
        getCurrentSession().remove(e);
        AuditorCaseLog auditorCaseLog = new AuditorCaseLog(e, "remove");
        getCurrentSession().persist(auditorCaseLog);
        try {
        	simulatorApi.addAuditorCaseLog(auditorCaseLog, simulator_host, simulator_aadAuditorCaseLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }
    
    
	@Override
	public List<AuditorCase> getAllCases(int auditorID) {
		TypedQuery<AuditorCase> query = getCurrentSession().createQuery("SELECT g FROM AuditorCase g WHERE g.auditorcaseAuditorID = '"+auditorID+"' ORDER BY g.createdOn DESC", AuditorCase.class);
		//TypedQuery<AuditorCase> query = getCurrentSession().createQuery("SELECT g FROM AuditorCase g ORDER BY g.createdOn DESC", AuditorCase.class);
		return query.getResultList();
	}

}
