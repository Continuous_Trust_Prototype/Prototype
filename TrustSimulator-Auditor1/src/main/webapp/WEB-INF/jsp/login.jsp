<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="login" scope="session" />
<c:set var="ses_headerTitle" value="${app_name}: Login to access Admin Menu" scope="session" />
<c:set var="ses_pageTitle" value="${app_name}: Login Now!" scope="session" />
<c:set var="ses_pageDescription" value="Fill in the required form below to login ..." scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
<jsp:include page="common/head.jsp"/>
</head>

<body>


<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'greet'}">
<div id="mainA">

	<script>goAdmin();</script>
	<h2>Thanks for logging in to manage</h2>
	<h1>${sessionScope.logedinAuditorName}</h1>
	<h2>You will be redirected to the Admin page in few seconds</h2>
	<h3><a href='${sessionScope.app_url}/admin'>or click here to go there directly</a></h3>
	
</div>
</c:when>

<c:otherwise>
		
<c:if test="${not empty sessionScope.logedinAuditor && sessionScope.logedinAuditor =='true'}">
	<script>goMainDirectly();</script>	
</c:if>


<div id="mainA">

<c:if test="${status == 'Does not exist'}">
	<p class="error">Admin name/password combination cannot be found in DB</p>
</c:if>

<h2>Login with your Admin credentials:</h2>
	<form:form modelAttribute="auditor" action="${sessionScope.app_url}/login_2" method="post">

	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="Name"><td colspan='2' class='cellTitle'>Auditor Name</td></form:label>
			<td colspan='4' class='cellData'> <form:input path="name" value="${auditor.name}"/> </td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${auditor.password}"/></td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Login</button></td>
		</tr>
			
	</table>
	
	</form:form>
	
</div>

</c:otherwise>

</c:choose>


<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>
