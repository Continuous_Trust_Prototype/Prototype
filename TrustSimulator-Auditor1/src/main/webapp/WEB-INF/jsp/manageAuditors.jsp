<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageAuditors" scope="session" />
<c:set var="ses_headerTitle" value="Manage Auditors..." scope="session" />
<c:set var="ses_pageTitle" value="Manage Auditors..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Edit this Auditor:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<%@ include file="auditorPageBody.jsp" %>