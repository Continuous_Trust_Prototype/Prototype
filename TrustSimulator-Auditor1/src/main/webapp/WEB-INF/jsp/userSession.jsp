<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="userSession" scope="session" />
<c:set var="ses_headerTitle" value="User Simulation Session..." scope="session" />
<c:set var="ses_pageTitle" value="User Simulation Session..." scope="session" />
<c:set var="ses_pageDescription" value="In this simulation page, you can choose to edit your personal details or check your inbox using the menu tabs. Plus, you can start interacting with one of the service providers listed below:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>

<c:choose>

<c:when test="${request == 'reportSpam'}">
<div id="mainA">

	<script>goUser();</script>
	<h2>Your SPAM have been successfully reported to: </h2>
	<h1>${auditor.name}</h1>
	<h2>You might be contacted by this auditor in case further information is needed or if a certain SP is found convicted in this SPAM: </h2>
	<h2>You will be redirected now to the Main page in few seconds</h2>
	<h3><a href='${simulator_url}'>or click here to go there directly</a></h3>
	
</div>
</c:when>

<c:otherwise>
<div id="mainA">

<h2>:)</h2>
	
</div>
</c:otherwise>

</c:choose>


<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>