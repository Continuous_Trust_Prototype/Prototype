<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="auditorSession" scope="session" />
<c:set var="ses_headerTitle" value="Auditor Simulation Session..." scope="session" />
<c:set var="ses_pageTitle" value="Auditor Simulation Session..." scope="session" />
<c:set var="ses_pageDescription" value="In this simulation page, you can choose to edit your personal details or check the current reported cases to you." scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<c:if test="${empty sessionScope.logedinAuditor || sessionScope.logedinAuditor =='false'}">
		<script>goLoginDirectly();</script>	
</c:if>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>

<c:choose>

<c:when test="${request == 'getAuditorCase'}">

<div id="mainA">

		<c:if test="${updated == 'success'}">
			<p class="success">This case have been updated successfully</p>
		</c:if>
		<c:if test="${userUpdated == 'success'}">
			<p class="success">The user of this case have been updated successfully</p>
		</c:if>

<a href="${sessionScope.app_url}/resumeAuditorSession_${auditorcase.auditorcaseAuditorID}"> Back to your Cases List </a>
	
	<table class='recordsTable'>
		<tr class='recordsHeader'>
			<td colspan='4'>  </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Reporting Date</td> 
			<td colspan='2' class='cellData'> ${auditorcase.createdOn}</td>
		</tr>	

		<tr>
			<td class='cellTitle'>Reporting User Name</td> 
			<td class='cellData'> ${auditorcase.auditorcaseUserName}</td>
			<td class='cellTitle'>Reporting User Email</td> 
			<td class='cellData'> ${message.receiverEmail}</td>
		</tr>	
		
		<tr>
			<td class='cellTitle'>Investigated Data Type</td> 
			<td class='cellData'> ${auditorcase.investigatedDataType}</td>
			<td class='cellTitle'>Investigated Data Value</td> 
			<td class='cellData'> ${auditorcase.investigatedDataValue}</td>
		</tr>	
		
		<tr>
			<td class='cellTitle'>Status</td> 
			<td class='cellData'> ${auditorcase.status}</td>
			<td class='cellTitle'>Convicted SP ID</td> 
			<td class='cellData'> ${auditorcase.convictedSPID}</td>
		</tr>	
		
		<tr>
			<td class='cellTitle'>Auditor Comment</td> 
			<td class='cellData'> ${auditorcase.comment}</td>
			<td class='cellTitle'>Last Modified</td> 
			<td class='cellData'> ${auditorcase.modifiedOn}</td>
		</tr>	
		
		<tr class='formHeader'>
			<td colspan='2'> <a href="${sessionScope.app_url}/updateAuditorCase_${auditorcase.auditorcaseID}"> Update Case </a> </td>
			<td colspan='2'> <a href="${sessionScope.app_url}/updateAuditorCaseUser_${auditorcase.auditorcaseID}"> Update User </a> </td>
		</tr>
		
		<tr>
			<td class='cellTitle'>Sender Name</td>
			<td class='cellData'> ${message.senderName} </td>
			<td class='cellTitle'>Sender Email</td>
			<td class='cellData'> ${message.senderEmail} </td>
		</tr>
		
		<tr>
			<td class='cellTitle'>Title</td> 
			<td class='cellData'> ${message.title}</td>
			<td class='cellTitle'>Date</td> 
			<td class='cellData'> ${message.createdOn}</td>
		</tr>			
		
		<tr class='recordsHeader'>
			<td colspan='4'>  </td>
		</tr>
			
		<tr>
			<td class='cellTitle'>Body</td>
			<td colspan='3' rowspan='5' class="cellData">${message.body}</td>
		</tr>

	</table>
		
</div>

</c:when>

<c:when test="${request == 'updateAuditorCase'}">

<div id="mainA">

<a href="${sessionScope.app_url}/resumeAuditorSession_${auditorcase.auditorcaseAuditorID}"> Back to your Cases List </a>
	
	<form:form modelAttribute="auditorcase" action="${sessionScope.app_url}/updateAuditorCase_2" method="post">
	<form:input type="hidden" path="auditorcaseID" value="${auditorcase.auditorcaseID}"/>
	<input type="hidden" name="auditorID" value="${auditorcase.auditorcaseAuditorID}"/>
	<input type="hidden" name="messageID" value="${auditorcase.auditorcaseMessageID}"/>
	<form:input type="hidden" path="auditorcaseUserID" value="${auditorcase.auditorcaseUserID}"/>
	<form:input type="hidden" path="auditorcaseUserName" value="${auditorcase.auditorcaseUserName}"/>
	<form:input type="hidden" path="investigatedDataType" value="${auditorcase.investigatedDataType}"/>
	<form:input type="hidden" path="investigatedDataValue" value="${auditorcase.investigatedDataValue}"/>
	<form:input type="hidden" path="createdOn" value="${auditorcase.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${auditorcase.modifiedOn}"/>
	<table class='recordsTable'>
		<tr class='recordsHeader'>
			<td colspan='4'>  </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Reporting Date</td> 
			<td colspan='2' class='cellData'> ${auditorcase.createdOn}</td>
		</tr>	

		<tr>
			<td class='cellTitle'>Reporting User Name</td> 
			<td class='cellData'> ${message.receiverName}</td>
			<td class='cellTitle'>Reporting User Email</td> 
			<td class='cellData'> ${message.receiverEmail}</td>
		</tr>	
		
		<tr>
			<td class='cellTitle'>Investigated Data Type</td> 
			<td class='cellData'> ${auditorcase.investigatedDataType}</td>
			<td class='cellTitle'>Investigated Data Value</td> 
			<td class='cellData'> ${auditorcase.investigatedDataValue}</td>
		</tr>	
		
		<tr>
			<td class='cellTitle'>Status</td> 
			<td class='cellData'>
			<select name='status'>
		
			<c:choose>
			<c:when test="${auditorcase.status == 'Open'}"><option value='Open' selected='selected'>Open</option></c:when>
			<c:otherwise> <option value='Open'>Open</option> </c:otherwise>
			</c:choose>
			
			<c:choose>
			<c:when test="${auditorcase.status == 'Closed'}"><option value='Closed' selected='selected'>Closed</option></c:when>
			<c:otherwise> <option value='Closed'>Closed</option> </c:otherwise>
			</c:choose>
			
			</select>
			</td>
			<td class='cellTitle'>Convicted SP ID</td> 
			<td class='cellData'> 
			<select name='convictedSP'>
			<option value='0'></option>
			<c:forEach var="currentSP" items="${sps}">
			<c:choose>
			<c:when test="${currentSP.ID == auditorcase.convictedSPID}"><option value='${currentSP.ID}' selected='selected'>${currentSP.ID}</option></c:when>
			<c:otherwise> <option value='${currentSP.ID}'>${currentSP.ID}</option></c:otherwise>
			</c:choose>
			</c:forEach>
			</select>
			</td>
		</tr>	
		
		<tr>
			<td class='cellTitle'>Auditor Comment</td> 
			<td class='cellData'> <form:textarea rows="3" path="comment" value="${auditorcase.comment}"/></td>
			<td class='cellTitle'>Last Modified</td> 
			<td class='cellData'> ${auditorcase.modifiedOn}</td>
		</tr>	
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit'>Confirm Updates</button></td>
		</tr>
		
		<tr>
			<td class='cellTitle'>Sender Name</td>
			<td class='cellData'> ${message.senderName} </td>
			<td class='cellTitle'>Sender Email</td>
			<td class='cellData'> ${message.senderEmail} </td>
		</tr>
		
		<tr>
			<td class='cellTitle'>Title</td> 
			<td class='cellData'> ${message.title}</td>
			<td class='cellTitle'>Date</td> 
			<td class='cellData'> ${message.createdOn}</td>
		</tr>			
		
		<tr class='recordsHeader'>
			<td colspan='4'>  </td>
		</tr>
			
		<tr>
			<td class='cellTitle'>Body</td>
			<td colspan='3' rowspan='5' class="cellData">${message.body}</td>
		</tr>

	</table>
	</form:form>
		
</div>

</c:when>


<c:otherwise>
<div id="mainA">

<h2>Currently Reported Cases</h2>
	
	<table class="recordsTable">	
	<tr class='recordsHeader'>
	<th>Case ID</th>
	<th>User ID</th>
	<th>Convicted SP ID</th>
	<th>Date</th>
	<th>Status</th>
	<th>Actions</th>
	</tr>
	<c:forEach var="current" items="${auditorcases}">
		<tr>
		<td>${current.auditorcaseID}</td>
		<td>${current.auditorcaseUserID}</td>
		<td>${current.convictedSPID}</td>
		<td>${current.createdOn}</td>
		<td>${current.status}</td>
		<td><a href="${sessionScope.app_url}/getAuditorCase_${current.auditorcaseID}">View</a></td>
		</tr>
	</c:forEach>
	</table>
	
</div>
</c:otherwise>

</c:choose>


<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>