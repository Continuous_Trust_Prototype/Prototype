<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>

<fmt:bundle basename="trust.simulator.config.application">

<fmt:message key='appUrl' var="appUrl" />
<c:set var="app_url" value="${appUrl}" scope="session" />  

<fmt:message key='simulatorUrl' var="simulatorUrl" />
<c:set var="simulator_url" value="${simulatorUrl}" scope="session" />  

<fmt:message key='appName' var="appName" />
<c:set var="app_name" value="${appName}" scope="session" /> 

</fmt:bundle>
