<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<style type='text/css'> @import url(site.css); </style>

<script type="text/javascript">

function goMain ( )
{
  setTimeout ( "window.location = '${sessionScope.app_url}/index'", 4000 );
}

function goAdmin ( )
{
  setTimeout ( "window.location = '${sessionScope.app_url}/admin'", 4000 );
}

function goMainDirectly ( )
{
  window.location = "${sessionScope.app_url}/index";
}

function goLoginDirectly ( )
{
  window.location = "${sessionScope.app_url}/login";
}

function goUser ( )
{
  setTimeout ( "window.location = '${sessionScope.simulator_url}/user'", 4000 );
}

function goAuditor ( )
{
  setTimeout ( "window.location = '${sessionScope.app_url}/auditor'", 4000 );
}


</script>

<title> ${sessionScope.ses_headerTitle}</title>
