<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="auditor" scope="session" />
<c:set var="ses_headerTitle" value="Auditor View..." scope="session" />
<c:set var="ses_pageTitle" value="Auditor View..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can resume an auditor session or create a new one" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />


<%@ include file="auditorPageBody.jsp" %>