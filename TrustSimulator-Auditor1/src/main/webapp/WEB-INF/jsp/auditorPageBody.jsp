<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'edit'}">
<div id="mainA">

	<h2>Edit Auditor Details</h2>

	<c:if test="${updated == 'success'}">
		<p class="success">Auditor Updated Successfully</p>
	</c:if>

	<form:form modelAttribute="auditor" action="${sessionScope.app_url}/editAuditor_2" method="post">
	<form:input type="hidden" path="ID" value="${auditor.ID}"/>
	<form:input type="hidden" path="name" value="${auditor.name}"/>
	<form:input type="hidden" path="createdOn" value="${auditor.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${auditor.modifiedOn}"/>
	<form:input type="hidden" path="email" value="${auditor.email}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>Auditor ID</td></form:label>
			<td colspan='4' class='cellData'> ${auditor.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${auditor.name} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${auditor.password}"/></td>
		</tr>

		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${auditor.email} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" value="${auditor.host}"/></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" value="${auditor.description}" /></td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update Auditor</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>
</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>