package trust.simulator.oauth2.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_info")
public class Userinfo_oAuth2 {

	@Id
	@GeneratedValue
    private int id;
	
	@Column (name = "sub", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String sub;

	@Column (name = "preferred_username", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String preferred_username;
	
	@Column (name = "name", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String name;
	
	@Column (name = "given_name", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String given_name;
	
	@Column (name = "family_name", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String family_name;
	
	@Column (name = "middle_name", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String middle_name;
	
	@Column (name = "nickname", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String nickname;

	@Column (name = "profile", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String profile;
	
	@Column (name = "picture", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String picture;
	
	@Column (name = "website", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String website;
	
	@Column (name = "email", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String email;
	
	@Column (name = "email_verified", unique = false, nullable = true, columnDefinition="tinyint(1)")
    private int email_verified;

	@Column (name = "gender", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String gender;
	
	@Column (name = "zone_info", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String zone_info;
	
	@Column (name = "locale", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String locale;
	
	@Column (name = "phone_number", unique = false, nullable = true, columnDefinition="varchar(256)")
    private int phone_number;
	
	@Column (name = "phone_number_verified", unique = false, nullable = true, columnDefinition="tinyint(1)")
    private int phone_number_verified;
	
	@Column (name = "address_id", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String address_id;
	
	@Column (name = "updated_time", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String updated_time;
	
	@Column (name = "birthdate", unique = false, nullable = true, columnDefinition="varchar(256)")
    private String birthdate;
	
	
    public Userinfo_oAuth2() {
    }

    public Userinfo_oAuth2(int id, String sub, String preferred_username, String name, String email, int email_verified, int phone, int phone_verified) {
        this.setId(id);
    	this.setSub(sub);
        this.setPreferred_username(preferred_username);
        this.setName(name);
        this.setEmail(email);
        this.setEmail_verified(email_verified);
        this.setPhone_number(phone);
        this.setPhone_number_verified(phone_verified);
    }
    
    public Userinfo_oAuth2(int id, String sub, String preferred_username, String name, String email, int email_verified) {
        this.setId(id);
    	this.setSub(sub);
        this.setPreferred_username(preferred_username);
        this.setName(name);
        this.setEmail(email);
        this.setEmail_verified(email_verified);
    }
    
    public Userinfo_oAuth2(int id)
    {
    	this.setId(id);
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getPreferred_username() {
		return preferred_username;
	}

	public void setPreferred_username(String preferred_username) {
		this.preferred_username = preferred_username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getFamily_name() {
		return family_name;
	}

	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEmail_verified() {
		return email_verified;
	}

	public void setEmail_verified(int email_verified) {
		this.email_verified = email_verified;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getZone_info() {
		return zone_info;
	}

	public void setZone_info(String zone_info) {
		this.zone_info = zone_info;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public int getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(int phone) {
		this.phone_number = phone;
	}

	public int getPhone_number_verified() {
		return phone_number_verified;
	}

	public void setPhone_number_verified(int phone_number_verified) {
		this.phone_number_verified = phone_number_verified;
	}

	public String getAddress_id() {
		return address_id;
	}

	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	
}
