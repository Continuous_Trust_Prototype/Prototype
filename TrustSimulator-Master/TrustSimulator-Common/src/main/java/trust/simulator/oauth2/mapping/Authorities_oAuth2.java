package trust.simulator.oauth2.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(AuthoritiesPK_oAuth2.class)
@Table(name = "authorities")
public class Authorities_oAuth2 {

	@Id
	@Column (name = "username", unique = false, nullable = false, columnDefinition="varchar(50)")
    private String username;
	
	@Id
	@Column (name = "authority", unique = false, nullable = false, columnDefinition="varchar(50)")
    private String authority;
	
    public Authorities_oAuth2() {
    }

    public Authorities_oAuth2(String username, String authority) {
        this.setUsername(username);
        this.setAuthority(authority);
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
}
