package trust.simulator.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import org.mitre.openid.connect.model.DefaultUserInfo;
import org.mitre.openid.connect.model.UserInfo;

import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.IDPLog;
import trust.simulator.entity.SP;
import trust.simulator.entity.User;
import trust.simulator.entity.UserLog;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class IDPApi {
	
	public IDPApi()
	{
		
	}

/*	public void logout(String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		System.out.println("logout uri: "+uri);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(uri);
		httpclient.execute(httpGet);	

	}
*/
	public UserInfo getUserInfo(String host, String path, String access_token) throws URISyntaxException, ClientProtocolException, IOException
	{
		UserInfo userInfo = new DefaultUserInfo();
		
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath("/"+path)
        .setParameter("access_token", access_token)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(uri);
		System.out.println("URI: "+httpGet.getURI().toString());
		httpGet.addHeader("Accept","application/json");
		HttpResponse response1 = httpclient.execute(httpGet);	
        HttpEntity entity1 = response1.getEntity();    
        
        String json = EntityUtils.toString(entity1);
     
        JsonParser parser = new JsonParser();
        JsonObject jsonObj = (JsonObject)parser.parse(json);
        
        userInfo = DefaultUserInfo.fromJson(jsonObj);
        
        EntityUtils.consume(entity1);

		return userInfo;
	}
	
	public List<AbstractLog> getUserLogs(String host, String path, int userID, String untilTime) throws URISyntaxException, ClientProtocolException, IOException
	{
		List<AbstractLog> logs = new ArrayList<AbstractLog>(); 
		
		URI uri;
		
		if(untilTime != null && !untilTime.equals(""))
		{
			System.out.println("1: untilTime = "+untilTime);
			
			 uri = new URIBuilder()
	        .setScheme("http")
	        .setHost(host)
	        .setPath(path)
	        .setParameter("userID", Integer.toString(userID))
	        .setParameter("untilTime", untilTime)
	        .build();
		}
		else
		{
			System.out.println("2: no untilTime!! ");
			
			 uri = new URIBuilder()
	        .setScheme("http")
	        .setHost(host)
	        .setPath(path)
	        .setParameter("userID", Integer.toString(userID))
	        .build();
		}
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		
		HttpGet httpGet = new HttpGet(uri);
		httpGet.addHeader("Accept","application/json");
		
		HttpResponse response1 = httpclient.execute(httpGet);	
		
        HttpEntity entity1 = response1.getEntity();    
        
        String jsonString = EntityUtils.toString(entity1);
        
        JsonParser parser = new JsonParser();
        System.out.println("jsonString: "+jsonString);
        JsonObject jsonObj = (JsonObject)parser.parse(jsonString);
        
        JsonArray jsonMainArr = jsonObj.getAsJsonArray("logs"); 
        
        for(int i=0; i<jsonMainArr.size(); i++)
        {
        	JsonObject obj = (JsonObject) jsonMainArr.get(i);
        	String logClass = (obj.has("logClass") ? obj.get("logClass").getAsString() : null);
        	
        	System.out.println("logClass: "+logClass);
        	
        	if(logClass != null && logClass.equals("UserLog"))
        	{
        		UserLog log = UserLog.fromJson(obj);
        		logs.add(i, log);
        	}
        	else if(logClass != null && logClass.equals("IDPLog"))
        	{
        		IDPLog log = IDPLog.fromJson(obj);
        		logs.add(i, log);
        	}
        	
        }
        
        EntityUtils.consume(entity1);

        return logs;
		
	}
	
	public void persistEndUser(String host, String path, User user, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(user.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_user", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_user", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}
	
	public void persistSP(String host, String path, SP sp, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(sp.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_sp", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_sp", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}
	
	public void persistAuditor(String host, String path, Auditor auditor, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(auditor.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_auditor", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_auditor", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}
	
	public void resetSimulation(String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://"+host+path);
		
		System.out.println("IDP URI: "+httpGet.getURI().toString());
	
		httpclient.execute(httpGet);
	}
	
	public static void main(String args[]) throws Exception {
		
		String access_token = "eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImp0aSI6IjQ5ZWQ0Yjc1LTJmMzMtNDIyMC1iOTE5LTYwZjNlMzIwZTY4YiIsImlhdCI6MTQwODAzMzY1Mn0.bHIWHhmNjtPDMcz8l2JItpK6s-IUq2pRtNelXqoq6wGoz5c16RWC5rY4bWQ43t852IH5Rp-TE_9fRf3ARcWGxtPyFUL8gWcNmOK4Fse4dwdRM1oDd-RpGWwo947c1XRjxpAcVDTfcQlKTbsruGo8_TjvUTjv15c-8f_MfHqqphI";
		
		String host = "localhost:8070";
		String path = "/TrustSimulator_IDP/userinfo";
		
		IDPApi idpApi = new IDPApi();
		
		UserInfo userInfo = idpApi.getUserInfo(host, path, access_token);
		
		System.out.println("userInfo.getName): "+userInfo.getName());
        System.out.println("userInfo.getEmail(): "+userInfo.getEmail());
        System.out.println("userInfo.getEmailVerified(): "+userInfo.getEmailVerified());
        System.out.println("userInfo.getPhone_number(): "+userInfo.getPhoneNumber());
        System.out.println("userInfo.getPhone_number_Verified(): "+userInfo.getPhoneNumberVerified());
	}
}
