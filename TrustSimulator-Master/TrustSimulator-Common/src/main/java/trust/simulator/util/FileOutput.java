package trust.simulator.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileOutput {
	
	public FileOutput(String content)
	{
		try {
					
					File file = new File("/Users/onaizi01/Documents/workspace/TrustSimulator-Master/output.txt");
					 
					// if file doesnt exists, then create it
					if (!file.exists()) {
						
							file.createNewFile();
						} 
		 
					FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(content+"\n");
					bw.close();
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}

}
