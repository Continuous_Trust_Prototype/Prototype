package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import trust.simulator.util.Operations;

@Entity(name = "AuditorCase")
public class AuditorCase {

	@Id
	@GeneratedValue
    private int auditorcaseID;
	
    @Column (name = "AuditorID", unique = false, nullable = false, columnDefinition="varchar(32)")
    private int auditorcaseAuditorID;

	@Column (name = "InvestigatedDataType", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String investigatedDataType;

	@Column (name = "InvestigatedDataValue", unique = false, nullable = false, columnDefinition="varchar(255)")
    private String investigatedDataValue;
	
    @Column (name = "UserID", unique = false, nullable = false)
    private int auditorcaseUserID;
    
	@Column (name = "UserName", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String auditorcaseUserName;

    @Column (name = "MessageID", unique = false, nullable = false, columnDefinition="varchar(32)")
    private int auditorcaseMessageID;

	@Column (name = "Status", unique = false, nullable = false)
    private String status;
	
    @Column (name = "ConvictedSPID", unique = false, nullable = true)
    private int convictedSPID;
	
	@Column (name = "Comment", unique = false, nullable = true, columnDefinition="mediumtext")
    private String comment;

	@Column (name = "CreatedOn", unique = false, nullable = false, columnDefinition="datetime")
    private String createdOn;
	
	@Column (name = "ModifiedOn", unique = false, nullable = false, columnDefinition="datetime")
    private String modifiedOn;

    public AuditorCase() {
    }

    public AuditorCase(int auditorcaseAuditorID, String investigatedDataType, String investigatedDataValue, int auditorcaseUserID, String auditorcaseUserName, int auditorcaseMessageID, String status) {
        //this.setAuditorcaseID(auditorcaseID);
    	this.setAuditorcaseAuditorID(auditorcaseAuditorID);
        this.setInvestigatedDataType(investigatedDataType);
        this.setInvestigatedDataValue(investigatedDataValue);
        this.setAuditorcaseUserID(auditorcaseUserID);
        this.setAuditorcaseUserName(auditorcaseUserName);
        this.setAuditorcaseMessageID(auditorcaseMessageID);
        this.setStatus(status);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setModifiedOn(Operations.getCurrentTime());
    }

	public int getAuditorcaseID() {
		return auditorcaseID;
	}

	public void setAuditorcaseID(int auditorcaseID) {
		this.auditorcaseID = auditorcaseID;
	}
    
	public int getAuditorcaseAuditorID() {
		return auditorcaseAuditorID;
	}

	public void setAuditorcaseAuditorID(int auditorcaseAuditorID) {
		this.auditorcaseAuditorID = auditorcaseAuditorID;
	}
    
	public String getInvestigatedDataType() {
		return investigatedDataType;
	}

	public void setInvestigatedDataType(String investigatedDataType) {
		this.investigatedDataType = investigatedDataType;
	}

	public String getInvestigatedDataValue() {
		return investigatedDataValue;
	}

	public void setInvestigatedDataValue(String investigatedDataValue) {
		this.investigatedDataValue = investigatedDataValue;
	}

	public int getAuditorcaseUserID() {
		return auditorcaseUserID;
	}

	public void setAuditorcaseUserID(int auditorcaseUserID) {
		this.auditorcaseUserID = auditorcaseUserID;
	}
	
	public String getAuditorcaseUserName() {
		return auditorcaseUserName;
	}

	public void setAuditorcaseUserName(String auditorcaseUserName) {
		this.auditorcaseUserName = auditorcaseUserName;
	}
	
	public int getAuditorcaseMessageID() {
		return auditorcaseMessageID;
	}

	public void setAuditorcaseMessageID(int auditorcaseMessageID) {
		this.auditorcaseMessageID = auditorcaseMessageID;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getConvictedSPID() {
		return convictedSPID;
	}

	public void setConvictedSPID(int convictedSPID) {
		this.convictedSPID = convictedSPID;
	}
	
    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}


}
