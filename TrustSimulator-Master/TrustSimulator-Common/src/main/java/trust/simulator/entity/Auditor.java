package trust.simulator.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="Auditor")
@DiscriminatorValue("Auditor")
public class Auditor extends AbstractEntity{
	
    public Auditor() {
    }
    
	public Auditor(String name, String password, String description, String email, String host) {
        //this.setAuditorID(auditorID);
        this.setName(name);
        this.setPassword(password);
        this.setDescription(description);
        this.setEmail(email);
        this.setHost(host);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setModifiedOn(Operations.getCurrentTime());
    }

	//TODO ** restrict access to IDP entity only
	//     **encrypt password OR Encrypt whole JSON
	public JsonObject toJsonFull() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("ID", this.getID());
		obj.addProperty("name", this.getName());
		obj.addProperty("password", this.getPassword());
		if(this.getDescription() != null) obj.addProperty("description", this.getDescription());
		obj.addProperty("email", this.getEmail());
		obj.addProperty("host", this.getHost());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("modifiedOn", this.getModifiedOn());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static Auditor fromJson(JsonObject obj) {
		
		Auditor ui = new Auditor();
		
		ui.setID(obj.has("ID") ? obj.get("ID").getAsInt() : null);
		ui.setName(obj.has("name") ? obj.get("name").getAsString() : null);
		ui.setPassword(obj.has("password") ? obj.get("password").getAsString() : null);
		ui.setDescription(obj.has("description") ? obj.get("description").getAsString() : null);
		ui.setEmail(obj.has("email") ? obj.get("email").getAsString() : null);
		ui.setHost(obj.has("host") ? obj.get("host").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setModifiedOn(obj.has("modifiedOn") ? obj.get("modifiedOn").getAsString() : null);
		
		return ui;

	}
	
}
