package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="User")
@DiscriminatorValue("User")
public class User extends AbstractEntity{
	
	@Column (name = "phone", unique = false, nullable = true, columnDefinition="varchar(32)")
	private String phone;

    public User() {
    }

    public User(String name, String password, String email, String phone) {
        this.setName(name);
        this.setPassword(password);
        this.setEmail(email);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setModifiedOn(Operations.getCurrentTime());
        this.setPhone(phone);
    }

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	//TODO ** Restrict access to IDP entity only
	//     ** Encrypt password OR Encrypt whole JSON
	public JsonObject toJsonFull() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("ID", this.getID());
		obj.addProperty("name", this.getName());
		obj.addProperty("password", this.getPassword());
		obj.addProperty("email", this.getEmail());
		if(this.getPhone() != null) obj.addProperty("phone", this.getPhone());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("modifiedOn", this.getModifiedOn());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static User fromJson(JsonObject obj) {
		
		User ui = new User();
		
		ui.setID(obj.has("ID") ? obj.get("ID").getAsInt() : null);
		ui.setName(obj.has("name") ? obj.get("name").getAsString() : null);
		ui.setPassword(obj.has("password") ? obj.get("password").getAsString() : null);
		ui.setEmail(obj.has("email") ? obj.get("email").getAsString() : null);
		ui.setPhone(obj.has("phone") ? obj.get("phone").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setModifiedOn(obj.has("modifiedOn") ? obj.get("modifiedOn").getAsString() : null);
		
		return ui;

	}
	
}
