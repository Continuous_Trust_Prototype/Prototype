package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import trust.simulator.util.Operations;

@Entity
@Table(name="MSPLog")
@DiscriminatorValue("MSPLog")
public class MSPLog extends AbstractLog{
	
	@Column (name = "MSPinterestedIn", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String interestedIn;

    public MSPLog() {
    }

    public MSPLog(MSP msp, String action) {
        this.setEntityID(msp.getID());
        this.setName(msp.getName());
        this.setDescription(msp.getDescription());
        this.setHost(msp.getHost());
        this.setInterestedIn(msp.getInterestedIn());
        this.setEmail(msp.getEmail());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setAction(action);
    }

	public String getInterestedIn() {
		return interestedIn;
	}

	public void setInterestedIn(String interestedIn) {
		this.interestedIn = interestedIn;
	}
	
}
