package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="SPDataLog")
@DiscriminatorValue("SPDataLog")
public class SPDataLog extends AbstractLog{
	
	@Column (name = "spdataID", unique = false, nullable = true, columnDefinition="int(5)")
    private int spdataID;
		
	@Column (name = "SPDataSPID", unique = false, nullable = true, columnDefinition="int(5)")
    private int spdataSPID;

	@Column (name = "SPDataObtainedDataType", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String obtainedDataType;

	@Column (name = "SPDatOobtainedDataValue", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String obtainedDataValue;
	
	@Column (name = "SPDataUserID", unique = false, nullable = true, columnDefinition="int(5)")
    private int spdataUserID;

    public SPDataLog() {
    }

    public SPDataLog(SPData spdata, String action) {
        this.setSpdataID(spdata.getSpdataID());
    	this.setSpdataSPID(spdata.getSpdataSPID());
        this.setObtainedDataType(spdata.getObtainedDataType());
        this.setObtainedDataValue(spdata.getObtainedDataValue());
        this.setSpdataUserID(spdata.getSpdataUserID());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setAction(action);
    }

	public int getSpdataID() {
		return spdataID;
	}

	public void setSpdataID(int spdataID) {
		this.spdataID = spdataID;
	}
    
	public int getSpdataSPID() {
		return spdataSPID;
	}

	public void setSpdataSPID(int spdataSPID) {
		this.spdataSPID = spdataSPID;
	}
    
	public String getObtainedDataType() {
		return obtainedDataType;
	}

	public void setObtainedDataType(String obtainedDataType) {
		this.obtainedDataType = obtainedDataType;
	}

	public String getObtainedDataValue() {
		return obtainedDataValue;
	}

	public void setObtainedDataValue(String obtainedDataValue) {
		this.obtainedDataValue = obtainedDataValue;
	}

	public int getSpdataUserID() {
		return spdataUserID;
	}

	public void setSpdataUserID(int spdataUserID) {
		this.spdataUserID = spdataUserID;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("action", this.getAction());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("spdataID", this.getSpdataID());
		obj.addProperty("spdataSPID", this.getSpdataSPID());
		obj.addProperty("obtainedDataType", this.getObtainedDataType());
		obj.addProperty("obtainedDataValue", this.getObtainedDataValue());
		obj.addProperty("spdataUserID", this.getSpdataUserID());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static SPDataLog fromJson(JsonObject obj) {
		
		SPDataLog ui = new SPDataLog();

		ui.setAction(obj.has("action") ? obj.get("action").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setSpdataID(obj.has("spdataID") ? obj.get("spdataID").getAsInt() : null);
		ui.setSpdataSPID(obj.has("spdataSPID") ? obj.get("spdataSPID").getAsInt() : null);
		ui.setObtainedDataType(obj.has("obtainedDataType") ? obj.get("obtainedDataType").getAsString() : null);
		ui.setObtainedDataValue(obj.has("obtainedDataValue") ? obj.get("obtainedDataValue").getAsString() : null);
		ui.setSpdataUserID(obj.has("spdataUserID") ? obj.get("spdataUserID").getAsInt() : null);
		
		return ui;

	}
	
}
