package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="IDPLog")
@DiscriminatorValue("IDPLog")
public class IDPLog extends AbstractLog{

	@Column (name = "IDPScope", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String IDPScope;
	
	@Column (name = "IDPTokenValue", unique = false, nullable = true, columnDefinition="varchar(4096)")
    private String IDPTokenValue;
	
	@Column (name = "IDPHost", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String IDPHost;
	
	@Column (name = "IDPClientHost", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String IDPClientHost;

	@Column (name = "IDPUserSub", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String IDPUserSub;
	
	public IDPLog() {
    }

    public IDPLog(String IDPScope, String IDPTokenValue, String IDPHost, String IDPClientHost, String IDPUserSub, String action) {
        this.setAction(action);
        this.setIDPScope(IDPScope);
        this.setIDPTokenValue(IDPTokenValue);
        this.setIDPHost(IDPHost);
        this.setIDPClientHost(IDPClientHost);
        this.setIDPUserSub(IDPUserSub);
        this.setCreatedOn(Operations.getCurrentTime());
    }
    
    public String getIDPScope() {
		return IDPScope;
	}

	public void setIDPScope(String IDPScope) {
		this.IDPScope = IDPScope;
	}

	public String getIDPTokenValue() {
		return IDPTokenValue;
	}

	public void setIDPTokenValue(String IDPTokenValue) {
		this.IDPTokenValue = IDPTokenValue;
	}

	public String getIDPHost() {
		return IDPHost;
	}

	public void setIDPHost(String IDPHost) {
		this.IDPHost = IDPHost;
	}

	public String getIDPClientHost() {
		return IDPClientHost;
	}

	public void setIDPClientHost(String IDPClientHost) {
		this.IDPClientHost = IDPClientHost;
	}

	public String getIDPUserSub() {
		return IDPUserSub;
	}

	public void setIDPUserSub(String IDPUserSub) {
		this.IDPUserSub = IDPUserSub;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("logClass", this.getClass().getSimpleName());
		obj.addProperty("action", this.getAction());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("IDPScope", this.getIDPScope());
		obj.addProperty("IDPTokenValue", this.getIDPTokenValue());
		obj.addProperty("IDPHost", this.getIDPHost());
		obj.addProperty("IDPClientHost", this.getIDPClientHost());
		obj.addProperty("IDPUserSub", this.getIDPUserSub());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static IDPLog fromJson(JsonObject obj) {
		
		IDPLog ui = new IDPLog();

		ui.setAction(obj.has("action") ? obj.get("action").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setIDPScope(obj.has("IDPScope") ? obj.get("IDPScope").getAsString() : null);
		ui.setIDPTokenValue(obj.has("IDPTokenValue") ? obj.get("IDPTokenValue").getAsString() : null);
		ui.setIDPHost(obj.has("IDPHost") ? obj.get("IDPHost").getAsString() : null);
		ui.setIDPClientHost(obj.has("IDPClientHost") ? obj.get("IDPClientHost").getAsString() : null);
		ui.setIDPUserSub(obj.has("IDPUserSub") ? obj.get("IDPUserSub").getAsString() : null);
		
		return ui;

	}

}
