package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import trust.simulator.util.Operations;

@Entity
@Table(name="SPLog")
@DiscriminatorValue("SPLog")
public class SPLog extends AbstractLog{
	
	@Column (name = "SPOffer", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String offer;
		
	@Column (name = "SPAffiliatedWithMsp", unique = false, nullable = true, columnDefinition="int(5)")
    private int affiliatedWithMsp;
	
	@Column (name = "SPAffiliatedWithMspName", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String affiliatedWithMspName;
	
	@Column (name = "SPAffiliatedWithMspHost", unique = false, nullable = true, columnDefinition="VARCHAR(255)")
    private String affiliatedWithMspHost;

    public SPLog() {
    }

    public SPLog(SP sp, String action) {
        this.setEntityID(sp.getID());
        this.setName(sp.getName());
        this.setDescription(sp.getDescription());
        this.setOffer(sp.getOffer());
        if(sp.getAffiliatedWithMsp() != 0)
        	this.setAffiliatedWithMsp(sp.getAffiliatedWithMsp());
        this.setHost(sp.getHost());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setAction(action);
        this.setEmail(sp.getEmail());
        this.setAffiliatedWithMspName(sp.getAffiliatedWithMspName());
        this.setAffiliatedWithMspHost(sp.getAffiliatedWithMspHost());
    }
	
	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public int getAffiliatedWithMsp() {
		return affiliatedWithMsp;
	}

	public void setAffiliatedWithMsp(int affiliatedWithMsp) {
		this.affiliatedWithMsp = affiliatedWithMsp;
	}

	public String getAffiliatedWithMspName() {
		return affiliatedWithMspName;
	}

	public void setAffiliatedWithMspName(String affiliatedWithMspName) {
		this.affiliatedWithMspName = affiliatedWithMspName;
	}

	public String getAffiliatedWithMspHost() {
		return affiliatedWithMspHost;
	}

	public void setAffiliatedWithMspHost(String affiliatedWithMspHost) {
		this.affiliatedWithMspHost = affiliatedWithMspHost;
	}

}
