package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import trust.simulator.util.Operations;

@Entity
@Table(name="MessageLog")
@DiscriminatorValue("MessageLog")
public class MessageLog extends AbstractLog{
	
	@Column (name = "messageID", unique = false, nullable = true, columnDefinition="int(5)")
    private int messageID;

	@Column (name = "messageSenderEmail", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String senderEmail;
	
	@Column (name = "messageSenderName", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String senderName;

	@Column (name = "messageReceiverEmail", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String receiverEmail;
	
	@Column (name = "messageReceiverName", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String receiverName;

	@Column (name = "messageTitle", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String title;
	
	@Column (name = "messageBody", unique = false, nullable = true, columnDefinition="mediumtext")
    private String body;
	
	@Column (name = "messageStatus", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String status;

    public MessageLog() {
    }

    public MessageLog(Message message, String action) {
        this.setMessageID(message.getMessageID());
        this.setSenderEmail(message.getSenderEmail());
        this.setSenderName(message.getSenderName());
        this.setReceiverEmail(message.getReceiverEmail());
        this.setReceiverName(message.getReceiverName());
        this.setTitle(message.getTitle());
        this.setBody(message.getBody());
        this.setStatus(message.getStatus());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setAction(action);
    }

	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
    
	public String getSenderName() {
		return senderName;
	}
	
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status= status;
	}
	

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}


}
