package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity(name = "SPData")
public class SPData {

	@Id
	@GeneratedValue
    private int spdataID;
	
    @Column (name = "spID", unique = false, nullable = false)
    private int spdataSPID;

	@Column (name = "obtainedDataType", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String obtainedDataType;

	@Column (name = "obtainedDataValue", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String obtainedDataValue;
	
    @Column (name = "userID", unique = false, nullable = false)
    private int spdataUserID;

	@Column (name = "createdOn", unique = false, nullable = false, columnDefinition="datetime")
    private String createdOn;

    public SPData() {
    }

    public SPData(int spdataSPID, String obtainedDataType, String obtainedDataValue, int spdataUserID) {
        //this.setSpdataID(spdataID);
    	this.setSpdataSPID(spdataSPID);
        this.setObtainedDataType(obtainedDataType);
        this.setObtainedDataValue(obtainedDataValue);
        this.setSpdataUserID(spdataUserID);
        this.setCreatedOn(Operations.getCurrentTime());
    }

	public int getSpdataID() {
		return spdataID;
	}

	public void setSpdataID(int spdataID) {
		this.spdataID = spdataID;
	}
    
	public int getSpdataSPID() {
		return spdataSPID;
	}

	public void setSpdataSPID(int spdataSPID) {
		this.spdataSPID = spdataSPID;
	}
    
	public String getObtainedDataType() {
		return obtainedDataType;
	}

	public void setObtainedDataType(String obtainedDataType) {
		this.obtainedDataType = obtainedDataType;
	}

	public String getObtainedDataValue() {
		return obtainedDataValue;
	}

	public void setObtainedDataValue(String obtainedDataValue) {
		this.obtainedDataValue = obtainedDataValue;
	}

	public int getSpdataUserID() {
		return spdataUserID;
	}

	public void setSpdataUserID(int spdataUserID) {
		this.spdataUserID = spdataUserID;
	}
	
    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("spdataID", this.getSpdataID());
		obj.addProperty("spdataSPID", this.getSpdataSPID());
		obj.addProperty("obtainedDataType", this.getObtainedDataType());
		obj.addProperty("obtainedDataValue", this.getObtainedDataValue());
		obj.addProperty("spdataUserID", this.getSpdataUserID());
		obj.addProperty("createdOn", this.getCreatedOn());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static SPData fromJson(JsonObject obj) {
		
		SPData ui = new SPData();
		
		ui.setSpdataID(obj.has("spdataID") ? obj.get("spdataID").getAsInt() : null);
		ui.setSpdataSPID(obj.has("spdataSPID") ? obj.get("spdataSPID").getAsInt() : null);
		ui.setObtainedDataType(obj.has("obtainedDataType") ? obj.get("obtainedDataType").getAsString() : null);
		ui.setObtainedDataValue(obj.has("obtainedDataValue") ? obj.get("obtainedDataValue").getAsString() : null);
		ui.setSpdataUserID(obj.has("spdataUserID") ? obj.get("spdataUserID").getAsInt() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		
		return ui;

	}

}
