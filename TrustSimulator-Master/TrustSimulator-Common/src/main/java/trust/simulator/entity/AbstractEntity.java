package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import trust.simulator.util.Operations;

@Entity
@Table(name = "AbstractEntity")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name="discriminator",
    discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="AE")
public class AbstractEntity {

	@Id
	@GeneratedValue
    private int ID;
	
	@Column (name = "name", unique = true, nullable = false, columnDefinition="varchar(255)")
    private String name;
	
	@Column (name = "password", unique = false, nullable = false, columnDefinition="varchar(50)")
    private String password;

	@Column (name = "email", unique = true, nullable = false, columnDefinition="varchar(128)")
    private String email;
	
	@Column (name = "host", unique = true, nullable = true, columnDefinition="varchar(255)")
    private String host;

	@Column (name = "description", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String description;
	
	@Column (name = "createdOn", unique = false, nullable = false, columnDefinition="datetime")
    private String createdOn;

	@Column (name = "modifiedOn", unique = false, nullable = false, columnDefinition="datetime")
    private String modifiedOn;
	
    public AbstractEntity() {
    }

    public AbstractEntity(String name, String password, String email, String description) {
        this.setName(name);
        this.setPassword(password);
        this.setEmail(email);
        this.setDescription(description);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setModifiedOn(Operations.getCurrentTime());
    }

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
    public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getSub() {
		
		System.out.println("this.getClass().getSimpleName(): "+this.getClass().getSimpleName());
		
		System.out.println("getID(): "+getID());
		
		System.out.println("getName().replaceAll(): "+getName().replaceAll("\\s+",""));
		
		return this.getClass().getSimpleName()+"."+getID()+"."+getName().replaceAll("\\s+","");
		//return Integer.toString(getID());
	}

}
