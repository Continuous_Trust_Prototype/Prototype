package trust.simulator.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import trust.simulator.util.Operations;


@Entity
@Table(name="AuditorLog")
@DiscriminatorValue("AuditorLog")
public class AuditorLog extends AbstractLog{
		
    public AuditorLog() {
    }

    public AuditorLog(Auditor auditor, String action) {
        this.setEntityID(auditor.getID());
        this.setName(auditor.getName());
        this.setEmail(auditor.getEmail());
        this.setDescription(auditor.getDescription());
        this.setHost(auditor.getHost());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setAction(action);
    }

}
