package trust.simulator.dao;

import trust.simulator.entity.MSP;

public interface MSPDao extends AbstractDao<MSP, String> {

}
