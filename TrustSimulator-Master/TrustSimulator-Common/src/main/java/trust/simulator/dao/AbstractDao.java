package trust.simulator.dao;

import java.io.Serializable;
import java.util.List;

public interface AbstractDao<E, I extends Serializable> {

    E findById(int userID);
    E getFirstEntity();
    //int getNewId();
    E save(E e);
    void update(E e);
    void delete(E e);
    void simpleSave_Log(E e);
    void simpleUpdate_Log(E e);
    void simpleDelete_Log(E e);
    List<E> getAllInstances();
	E findByName(String name);
}
