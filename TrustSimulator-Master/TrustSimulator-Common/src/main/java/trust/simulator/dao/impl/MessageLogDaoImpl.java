package trust.simulator.dao.impl;


import org.springframework.stereotype.Repository;

import trust.simulator.dao.MessageLogDao;
import trust.simulator.entity.MessageLog;

@Repository
public class MessageLogDaoImpl extends AbstractDaoImpl<MessageLog, String> implements MessageLogDao {

    protected MessageLogDaoImpl() {
        super(MessageLog.class);
    }

}
