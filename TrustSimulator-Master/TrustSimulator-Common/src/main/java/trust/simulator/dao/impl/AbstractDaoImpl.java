package trust.simulator.dao.impl;

import trust.simulator.dao.AbstractDao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

@Repository
public abstract class AbstractDaoImpl<E, I extends Serializable> implements AbstractDao<E,I> {

    // Injected database connection:
    @PersistenceContext
    private EntityManager em;
	
    private Class<E> entityClass;

    protected AbstractDaoImpl(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    protected EntityManager getCurrentSession() {
        return em;
    }

	@Override
    public E findById(int id) {
    	return getCurrentSession().find(entityClass, id);
    }
	
	@Override
    public E getFirstEntity() {
    	TypedQuery<E> query = getCurrentSession().createQuery("SELECT g FROM "+entityClass.getName()+" g", entityClass);
    	List<E> results = query.getResultList();
    	return results.get(0);    
    }
	
	@Override
    public E findByName(String name) {
		TypedQuery<E> query = getCurrentSession().createQuery("SELECT g FROM "+entityClass.getName()+" g WHERE g.name ='"+name+"'", entityClass);
    	
		if (query.getResultList().isEmpty()) return null;
    	
		else return query.getResultList().get(0); 
    }
    
	/*@Override
    public int getNewId() {
		String tableName = Operations.getTableName(entityClass.getName()); //entityClass.getName().substring(23);
		String fieldName = tableName.toLowerCase()+"ID";
		System.out.println("tableName == "+tableName);
		System.out.println("fieldName == "+fieldName);
		TypedQuery<Integer> query = getCurrentSession().createQuery("SELECT g."+fieldName+" FROM "+tableName+" g ORDER BY g."+fieldName+" DESC", Integer.class);
		if (!query.getResultList().isEmpty())
		return query.getResultList().get(0)+1;
		else return 1;
    }
*/

    @Override
    public E save(E e) {
        getCurrentSession().persist(e);
        getCurrentSession().flush();
        return e;
    }
    
    @Override
    public void update(E e) {
        getCurrentSession().merge(e);
        getCurrentSession().flush();
    }
    
    @Override
    public void delete(E e) {
        getCurrentSession().remove(e);
        getCurrentSession().flush();
    }
    
	//Logging should be implemented by sub classes
    @Override
    public void simpleSave_Log(E e) {
        getCurrentSession().persist(e);
        getCurrentSession().flush();
    }
    
	//Logging should be implemented by sub classes
    @Override
    public void simpleUpdate_Log(E e) {
        getCurrentSession().merge(e);
        getCurrentSession().flush();
    }

	//Logging should be implemented by sub classes
    @Override
    public void simpleDelete_Log(E e) {
        getCurrentSession().remove(e);
        getCurrentSession().flush();
    }
    
    @Override
	public List<E> getAllInstances() {
    	TypedQuery<E> query = getCurrentSession().createQuery("SELECT g FROM "+entityClass.getName()+" g", entityClass);
    	return query.getResultList();    	
	}
    
}
