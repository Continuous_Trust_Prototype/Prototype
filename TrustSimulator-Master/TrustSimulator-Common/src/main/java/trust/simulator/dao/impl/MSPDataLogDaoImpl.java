package trust.simulator.dao.impl;

import org.springframework.stereotype.Repository;

import trust.simulator.dao.MSPDataLogDao;
import trust.simulator.entity.MSPDataLog;

@Repository
public class MSPDataLogDaoImpl extends AbstractDaoImpl<MSPDataLog, String> implements MSPDataLogDao {

    protected MSPDataLogDaoImpl() {
        super(MSPDataLog.class);
    }


}
