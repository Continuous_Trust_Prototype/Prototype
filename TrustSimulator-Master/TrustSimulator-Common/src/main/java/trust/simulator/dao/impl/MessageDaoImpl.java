package trust.simulator.dao.impl;


import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import trust.simulator.dao.MessageDao;
import trust.simulator.entity.Message;
import trust.simulator.entity.MessageLog;
import trust.simulator.service.MessagesNotification;

@Repository
public class MessageDaoImpl extends AbstractDaoImpl<Message, String> implements MessageDao {

    protected MessageDaoImpl() {
        super(Message.class);
    }
    
	//Log Events
    @Override
    public Message save(Message e) {
        getCurrentSession().persist(e);
        MessageLog messageLog = new MessageLog(e, "save");
        getCurrentSession().persist(messageLog);
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(Message e) {
        getCurrentSession().merge(e);
        MessageLog messageLog = new MessageLog(e, "update");
        getCurrentSession().persist(messageLog);
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(Message e) {
        getCurrentSession().remove(e);
        MessageLog messageLog = new MessageLog(e, "remove");
        getCurrentSession().persist(messageLog);
        getCurrentSession().flush();
    }
    
    @Override
    public MessagesNotification getMessagesNotification(String receiverEmail) {
        MessagesNotification messagesNotification = new MessagesNotification();
        TypedQuery<String> query = getCurrentSession().createQuery("SELECT g.receiverEmail FROM Message g WHERE g.receiverEmail = '"+receiverEmail+"' AND g.status = 'unread' ORDER BY g.createdOn DESC", String.class);
		if (query.getResultList().isEmpty())
		{
			messagesNotification.setEmpty(true);
			messagesNotification.setUnreadMessages(0);
		}
		else
		{
			messagesNotification.setEmpty(false);
			messagesNotification.setUnreadMessages(query.getResultList().size());
		}
		return messagesNotification;
    }
    
    @Override
    public List<Message> getUserMessages(String receiverEmail) {

        TypedQuery<Message> query = getCurrentSession().createQuery("SELECT g FROM Message g WHERE g.receiverEmail = '"+receiverEmail+"' ORDER BY g.createdOn DESC", Message.class);
        return query.getResultList();
    }


}
