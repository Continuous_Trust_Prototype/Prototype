package trust.simulator.dao;

import trust.simulator.entity.SP;

public interface SPDao extends AbstractDao<SP, String> {

}
