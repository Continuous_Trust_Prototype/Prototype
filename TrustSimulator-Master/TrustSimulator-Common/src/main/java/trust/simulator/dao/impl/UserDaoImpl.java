package trust.simulator.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import trust.simulator.dao.UserDao;
import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.User;
import trust.simulator.service.UserService;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, String> implements UserDao {

    protected UserDaoImpl() {
        super(User.class);
    }
    
    @Autowired
    private UserService userService;
    
    @Override
	public List<AbstractLog> getAllUserLogs(int userID) {
    	
    	User user = userService.findByUserID(userID);
    	String userEmail = user.getEmail();
    	
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractLog where "
    			+"entityID = '"+userID+"' OR  auditorcaseUserID = '"+userID+"' OR mspdataUserID = '"+userID+"' "
    			+"OR spdataUserID = '"+userID+"' OR messageReceiverEmail = '"+userEmail+"' OR "
    			+"messageSenderEmail = '"+userEmail+"' OR IDPUserSub = '"+user.getSub()+"'", AbstractLog.class);
    	
    	@SuppressWarnings("unchecked")
		List<AbstractLog> resultList = query.getResultList();
    	for(int i=0; i<resultList.size(); i++)
    		System.out.println(resultList.get(i).toString());
		return resultList;    	
	}
    
    @Override
	public List<AbstractLog> getAllUserLogs(int userID, String untilTime) {
    	
    	User user = userService.findByUserID(userID);
    	String userEmail = user.getEmail();
    	
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractLog where ("
    			+"entityID = '"+userID+"' OR  auditorcaseUserID = '"+userID+"' OR mspdataUserID = '"+userID+"' "
    			+"OR spdataUserID = '"+userID+"' OR messageReceiverEmail = '"+userEmail+"' OR "
    			+"messageSenderEmail = '"+userEmail+"' OR IDPUserSub = '"+user.getSub()+"') AND createdOn > '"+untilTime+"'", AbstractLog.class);
    	
    	@SuppressWarnings("unchecked")
		List<AbstractLog> resultList = query.getResultList();
		return resultList;    	
	}


}
