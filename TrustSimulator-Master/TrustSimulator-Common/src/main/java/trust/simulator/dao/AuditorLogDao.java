package trust.simulator.dao;

import trust.simulator.entity.AuditorLog;

public interface AuditorLogDao extends AbstractDao<AuditorLog, String> {

}
