package trust.simulator.dao.impl;


import org.springframework.stereotype.Repository;

import trust.simulator.dao.MSPLogDao;
import trust.simulator.entity.MSPLog;

@Repository
public class MSPLogDaoImpl extends AbstractDaoImpl<MSPLog, String> implements MSPLogDao {

    protected MSPLogDaoImpl() {
        super(MSPLog.class);
    }

}
