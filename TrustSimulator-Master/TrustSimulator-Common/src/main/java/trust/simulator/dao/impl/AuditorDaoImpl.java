package trust.simulator.dao.impl;

import org.springframework.stereotype.Repository;

import trust.simulator.dao.AuditorDao;
import trust.simulator.entity.Auditor;

@Repository
public class AuditorDaoImpl extends AbstractDaoImpl<Auditor, String> implements AuditorDao {

    protected AuditorDaoImpl() {
        super(Auditor.class);
    }


}
