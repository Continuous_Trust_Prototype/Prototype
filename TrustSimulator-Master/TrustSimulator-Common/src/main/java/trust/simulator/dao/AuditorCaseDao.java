package trust.simulator.dao;

import java.util.List;

import trust.simulator.entity.AuditorCase;

public interface AuditorCaseDao extends AbstractDao<AuditorCase, String> {
	
	List<AuditorCase> getAllCases(int auditorID);

}
