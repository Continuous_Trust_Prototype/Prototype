package trust.simulator.dao;

import trust.simulator.entity.IDPLog;

public interface IDPLogDao extends AbstractDao<IDPLog, String> {

}
