package trust.simulator.dao.impl;

import org.springframework.stereotype.Repository;

import trust.simulator.dao.AbstractLogDao;
import trust.simulator.entity.AbstractLog;


@Repository
public class AbstractLogDaoImpl extends AbstractDaoImpl<AbstractLog, String> implements AbstractLogDao {

    protected AbstractLogDaoImpl() {
        super(AbstractLog.class);
    }

}
