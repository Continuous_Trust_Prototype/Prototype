package trust.simulator.dao.impl;


import org.springframework.stereotype.Repository;

import trust.simulator.dao.AuditorLogDao;
import trust.simulator.entity.AuditorLog;

@Repository
public class AuditorLogDaoImpl extends AbstractDaoImpl<AuditorLog, String> implements AuditorLogDao {

    protected AuditorLogDaoImpl() {
        super(AuditorLog.class);
    }

}
