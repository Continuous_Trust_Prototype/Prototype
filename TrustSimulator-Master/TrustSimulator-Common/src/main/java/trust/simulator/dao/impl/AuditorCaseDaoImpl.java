package trust.simulator.dao.impl;


import java.util.List;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import trust.simulator.dao.AuditorCaseDao;
import trust.simulator.entity.AuditorCase;
import trust.simulator.entity.AuditorCaseLog;

@Repository
public class AuditorCaseDaoImpl extends AbstractDaoImpl<AuditorCase, String> implements AuditorCaseDao {

    protected AuditorCaseDaoImpl() {
        super(AuditorCase.class);
    }

	//Log Events
    @Override
    public AuditorCase save(AuditorCase e) {
        getCurrentSession().persist(e);
        AuditorCaseLog auditorCaseLog = new AuditorCaseLog(e, "save");
        getCurrentSession().persist(auditorCaseLog);
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(AuditorCase e) {
        getCurrentSession().merge(e);
        AuditorCaseLog auditorCaseLog = new AuditorCaseLog(e, "update");
        getCurrentSession().persist(auditorCaseLog);
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(AuditorCase e) {
        getCurrentSession().remove(e);
        AuditorCaseLog auditorCaseLog = new AuditorCaseLog(e, "remove");
        getCurrentSession().persist(auditorCaseLog);
        getCurrentSession().flush();
    }
    
    
	@Override
	public List<AuditorCase> getAllCases(int auditorID) {
		TypedQuery<AuditorCase> query = getCurrentSession().createQuery("SELECT g FROM AuditorCase g WHERE g.auditorcaseAuditorID = '"+auditorID+"' ORDER BY g.createdOn DESC", AuditorCase.class);
		//TypedQuery<AuditorCase> query = getCurrentSession().createQuery("SELECT g FROM AuditorCase g ORDER BY g.createdOn DESC", AuditorCase.class);
		return query.getResultList();
	}

}
