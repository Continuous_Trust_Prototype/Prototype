package trust.simulator.dao.impl;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.dao.AdminDao;


@Repository
public class AdminDaoImpl implements AdminDao {
    
    @Value("${db.name}")
    private String db_name;
    
    // Injected database connection:
    @PersistenceContext
    private EntityManager em;
	
    public EntityManager getCurrentSession() {
        return em;
    }
    
    
    public void resetDB()
    {
    	
    	Query query = getCurrentSession().createNativeQuery("SHOW TABLES FROM `"+db_name+"`");
   	
    	@SuppressWarnings("unchecked")
		List<String> tableNames = query.getResultList();
        Iterator<String> iterator = tableNames.iterator();
        while (iterator.hasNext()) {
        	String next = iterator.next();
        	Query query2 = getCurrentSession().createNativeQuery("TRUNCATE TABLE  `"+next+"`");
        	query2.executeUpdate();
        	getCurrentSession().flush();
        }	
        
        Query resetSequence = getCurrentSession().createNativeQuery("INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 0);");
        resetSequence.executeUpdate();
        
        getCurrentSession().flush();
    }


}
