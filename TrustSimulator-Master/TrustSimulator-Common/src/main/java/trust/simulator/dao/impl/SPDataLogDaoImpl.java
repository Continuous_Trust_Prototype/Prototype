package trust.simulator.dao.impl;


import org.springframework.stereotype.Repository;

import trust.simulator.dao.SPDataLogDao;
import trust.simulator.entity.SPDataLog;

@Repository
public class SPDataLogDaoImpl extends AbstractDaoImpl<SPDataLog, String> implements SPDataLogDao {

    protected SPDataLogDaoImpl() {
        super(SPDataLog.class);
    }

}
