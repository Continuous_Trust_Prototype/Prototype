package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.AbstractLogDao;
import trust.simulator.dao.SPDao;
import trust.simulator.entity.SP;
import trust.simulator.service.SPService;


@Service("SPService")
@Transactional(readOnly = true)
public class SPServiceImpl implements SPService {

    @Autowired
    private SPDao spDao;
    
    @Autowired
    private AbstractLogDao spdataLogDao;

    @Override
    public SP findBySpID(int spID) {
        return spDao.findById(spID);
    }
    
    @Override
    public SP findByName(String name) {
        return spDao.findByName(name);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveSp(SP sp) {
        spDao.save(sp);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateSp(SP sp) {
        spDao.update(sp);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteSp(SP sp) {
    	spDao.delete(sp);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void simpleSaveSp_Log(SP sp) {
        spDao.simpleSave_Log(sp);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void simpleUpdateSp_Log(SP sp) {
        spDao.simpleUpdate_Log(sp);
    }

    @Override
    @Transactional(readOnly = false)
    public void simpleDeleteSp_Log(SP sp) {
    	spDao.simpleDelete_Log(sp);
    }

	@Override
	public List<SP> getAllSps() {
		return spDao.getAllInstances();
		
	}

/*	@Override
	public int getNewId() {
		return spDao.getNewId();
	}
*/
}
