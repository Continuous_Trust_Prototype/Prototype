package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.MSP;

public interface MSPService {

    MSP findByMspID(int mspID);
    MSP getThisMsp();
    void saveMsp(MSP msp);
    void updateMsp(MSP msp);
    void deleteMsp(int mspID);
    List<MSP> getAllMsps();
   // int getNewId();
	MSP findByName(String name);
	void simpleSaveMsp_Log(MSP msp);
	void simpleUpdateMsp_Log(MSP msp);
	void simpleDeleteMsp_Log(MSP msp);
}
