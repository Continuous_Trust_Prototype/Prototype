package trust.simulator.service;

public class MessagesNotification {

    private boolean isEmpty;
	
    private int unreadMessages;

    public MessagesNotification() {
    }

    public MessagesNotification(boolean empty, int unreadMessages) {
        this.setEmpty(empty);
        this.setUnreadMessages(unreadMessages);
    }

	public boolean isEmpty() {
		return isEmpty;
	}

	public void setEmpty(boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

	public int getUnreadMessages() {
		return unreadMessages;
	}

	public void setUnreadMessages(int unreadMessages) {
		this.unreadMessages = unreadMessages;
	}

}
