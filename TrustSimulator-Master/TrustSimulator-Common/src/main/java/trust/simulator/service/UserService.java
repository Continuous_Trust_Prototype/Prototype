package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.Message;
import trust.simulator.entity.User;

public interface UserService {

    User findByUserID(int userID);
    void saveUser(User user);
    void updateUser(User user);
    void deleteUser(User user);
    List<User> getAllUsers();
    User findByName(String name);
    //int getNewId();
    MessagesNotification getMessagesNotification(User user);
    List<Message> getUserMessages(User user);
    List<AbstractLog> getAllUserLogs(int userID);
    List<AbstractLog> getAllUserLogs(int userID, String untilTime);
}
