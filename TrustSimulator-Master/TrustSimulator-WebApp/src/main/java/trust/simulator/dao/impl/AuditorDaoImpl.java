package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.AuditorApi;
import trust.simulator.api.IDPApi;
import trust.simulator.dao.AuditorDao;
import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorLog;


@Repository
public class AuditorDaoImpl extends AbstractDaoImpl<Auditor, String> implements AuditorDao {

    protected AuditorDaoImpl() {
        super(Auditor.class);
    }
    
    IDPApi idpApi = new IDPApi();
    private AuditorApi auditorApi = new AuditorApi();
    
    @Value("${idpHost}")
    private String idp_host;
    
    @Value("${idpPersistAuditorPath}")
    private String idp_persistAuditor_path;
    
	//Log Events
    @Override
    public Auditor save(Auditor e) {
        
    	getCurrentSession().persist(e);
        
    	AuditorLog auditorLog = new AuditorLog(e, "save");
        getCurrentSession().persist(auditorLog);
        
        try {
        	
			idpApi.persistAuditor(idp_host, idp_persistAuditor_path, e, "save");
			auditorApi.persistAuditor(e.getHost(), "/persistAuditor", e, "save");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(Auditor e) {
    	
        getCurrentSession().merge(e);
        
        AuditorLog auditorLog = new AuditorLog(e, "update");
        getCurrentSession().persist(auditorLog);
        
        try {
        	
			idpApi.persistAuditor(idp_host, idp_persistAuditor_path, e, "update");
			auditorApi.persistAuditor(e.getHost(), "/persistAuditor", e, "update");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(Auditor e) {
    	
    	try {
        	
			idpApi.persistAuditor(idp_host, idp_persistAuditor_path, e, "delete");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
    	Auditor auditor = getCurrentSession().getReference(Auditor.class, e.getID());
        getCurrentSession().remove(auditor);
       
        AuditorLog auditorLog = new AuditorLog(e, "remove");
        getCurrentSession().persist(auditorLog);
        
        getCurrentSession().flush();
    }

}
