package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Repository;

import trust.simulator.api.MSPApi;
import trust.simulator.dao.MSPDao;
import trust.simulator.entity.AbstractEntity;
import trust.simulator.entity.MSP;
import trust.simulator.entity.MSPLog;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPLog;

@Repository
public class MSPDaoImpl extends AbstractDaoImpl<MSP, String> implements MSPDao {

    protected MSPDaoImpl() {
        super(MSP.class);
    }
    
    private MSPApi mspApi = new MSPApi();
    
	//Log Events
    @Override
    public MSP save(MSP e) {
        getCurrentSession().persist(e);
       
        MSPLog mspLog = new MSPLog(e, "save");
        getCurrentSession().persist(mspLog);
        
        try {
        	
			mspApi.persistMSP(e.getHost(), "/persistMSP", e, "save");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
        
    }
    
    //Log Events
    @Override
    public void update(MSP e) {
       
    	getCurrentSession().merge(e);
        
    	MSPLog mspLog = new MSPLog(e, "update");
        getCurrentSession().persist(mspLog);
        
        try {
        	
			mspApi.persistMSP(e.getHost(), "/persistMSP", e, "update");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
	public void delete(MSP e) {
           	
    	MSP msp = getCurrentSession().getReference(MSP.class, e.getID());
    	getCurrentSession().remove(msp);
        
    	MSPLog mspLog = new MSPLog(e, "remove");
        getCurrentSession().persist(mspLog);
        
        //TODO instead of updating all SPs, let each SP check if the MSP it is 
        //Affiliated with still existing before communicating with it or not
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractEntity where "
    			+"affiliatedWithMsp = '"+e.getID()+"'", AbstractEntity.class);
        
        @SuppressWarnings("unchecked")
		List<SP> sps = query.getResultList();
        Iterator<SP> iterator = sps.iterator();
        while (iterator.hasNext()) {
            SP sp = iterator.next();
            sp.setAffiliatedWithMsp(0);
            SPLog spLog = new SPLog(sp, "update");
            getCurrentSession().persist(spLog);
        }
        
        getCurrentSession().flush();
    }
    
	//Log Events
    @Override
    public void simpleSave_Log(MSP e) {
        
    	getCurrentSession().persist(e);
        
    	MSPLog mspLog = new MSPLog(e, "save");
        getCurrentSession().persist(mspLog);
        
        getCurrentSession().flush();
      
     }
    
  //Log Events
    @Override
    public void simpleUpdate_Log(MSP e) {
    	
    	getCurrentSession().merge(e);
        
    	MSPLog mspLog = new MSPLog(e, "update");
        getCurrentSession().persist(mspLog);
        
        getCurrentSession().flush();
        
    }

    //Log Events
    @Override
    public void simpleDelete_Log(MSP e) {
                
    	MSP msp = getCurrentSession().getReference(MSP.class, e.getID());
        getCurrentSession().remove(msp);
        
        MSPLog mspLog = new MSPLog(e, "remove");
        getCurrentSession().persist(mspLog);
        
        getCurrentSession().flush();
    }


}
