package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.AuditorCaseLog;
import trust.simulator.entity.IDPLog;
import trust.simulator.entity.MSPDataLog;
import trust.simulator.entity.SPDataLog;
import trust.simulator.entity.User;
import trust.simulator.service.AbstractLogService;
import trust.simulator.service.UserService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LogsController {

    @Autowired
    private UserService userService;

    @Autowired
    private AbstractLogService abstractLogService;
    
    
    
    @RequestMapping(value = "/manageLogs", method = RequestMethod.GET)
    public String showLogsPage(Model model) {
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);       
        return "manageLogs";
    }

    @RequestMapping(value = "/getUserLog", method = RequestMethod.POST)
    public String getUserlog( Model model, HttpServletRequest request) {

    	Integer userID = Integer.parseInt(request.getParameter("userID"));
    	
    	User user = userService.findByUserID(userID);
    	model.addAttribute("user", user);
    	
        List<AbstractLog> abstractLogs = userService.getAllUserLogs(userID);
        model.addAttribute("abstractLogs", abstractLogs);   
        
        model.addAttribute("request", "showUserLog");

        return "manageLogs";
    }
    
    @RequestMapping(value = "/getUserLog", method = RequestMethod.GET)
    public String getUserlog2( Model model, HttpSession session) {

    	Integer userID = (Integer) session.getAttribute("ses_userLog");
    	//int userID = Integer.parseInt(userLog);
    	
    	User user = userService.findByUserID(userID);
    	model.addAttribute("user", user);
    	
        List<AbstractLog> abstractLogs = userService.getAllUserLogs(userID);
        model.addAttribute("abstractLogs", abstractLogs);   
        
        model.addAttribute("request", "showUserLog");

        return "manageLogs";
    }
    
    @RequestMapping(value = "/getExpandedLog_{ID}", method = RequestMethod.GET)
    public String getExpndedLog( Model model, HttpSession session, @PathVariable int ID) {
    	    	
    	AbstractLog log = abstractLogService.findByLogID(ID);
    	model.addAttribute("log", log);
    	
        model.addAttribute("request", "showExpandedLog");

        return "manageLogs";
    }
    
    @RequestMapping(value = "/addSPDataLog", method = RequestMethod.POST)
    public void addSPDataLog(@RequestParam("spdataLog") String spdataLog) {
    	JsonParser parser = new JsonParser();
    	JsonObject spdataLogJson = (JsonObject)parser.parse(spdataLog);
    	SPDataLog spdataLog2 = SPDataLog.fromJson(spdataLogJson);
    	abstractLogService.saveLog(spdataLog2);
    }
    
    @RequestMapping(value = "/addMSPDataLog", method = RequestMethod.POST)
    public void addMSPDataLog(@RequestParam("mspdataLog") String mspdataLog) {
    	JsonParser parser = new JsonParser();
    	JsonObject mspdataLogJson = (JsonObject)parser.parse(mspdataLog);
    	MSPDataLog mspdataLog2 = MSPDataLog.fromJson(mspdataLogJson);
    	abstractLogService.saveLog(mspdataLog2);
    }
    
    @RequestMapping(value = "/addAuditorCaseLog", method = RequestMethod.POST)
    public void addAuditorCaseLog(@RequestParam("auditorcaseLog") String auditorcaseLog) {
    	JsonParser parser = new JsonParser();
    	JsonObject auditorcaseLogJson = (JsonObject)parser.parse(auditorcaseLog);
    	AuditorCaseLog auditorcaseLog2 = AuditorCaseLog.fromJson(auditorcaseLogJson);
    	abstractLogService.saveLog(auditorcaseLog2);
    }
    
    @RequestMapping(value = "/addIDPLog", method = RequestMethod.POST)
    public void addIDPLog(@RequestParam("idpLog") String idpLog) {
    	JsonParser parser = new JsonParser();
    	JsonObject idpLogJson = (JsonObject)parser.parse(idpLog);
    	IDPLog idpLog2 = IDPLog.fromJson(idpLogJson);
        abstractLogService.saveLog(idpLog2);
    }

}
