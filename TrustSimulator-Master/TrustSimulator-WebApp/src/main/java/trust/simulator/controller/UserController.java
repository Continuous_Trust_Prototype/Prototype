package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.User;
import trust.simulator.service.UserService;
import trust.simulator.util.Operations;

import java.util.List;

import javax.servlet.http.HttpSession;


@Controller
public class UserController {

    @Autowired
    private UserService userService;
  
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String showUserForm(Model model, HttpSession session) {
    	
    	String logedinUser = (String) session.getAttribute("logedinUser");
    	String userID = (String) session.getAttribute("logedinUserID");
    	if(logedinUser != null && logedinUser.equals("true")){
    		model.addAttribute("messagesNotification", userService.getMessagesNotification(userService.findByUserID(Integer.parseInt(userID))));
    		return "redirect:/resumeUserSession_"+userID;
    	}
    	
    	model.addAttribute("user",new User());
        
    	List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        
        return Operations.routeView(session, "manageUsers", "user");
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String saveUser( Model model, User user, HttpSession session) {
        User existing = userService.findByName(user.getName());
        if (existing != null) {
            model.addAttribute("status", "exist");
            List<User> users = userService.getAllUsers();
            model.addAttribute("users", users);
        
            return Operations.routeView(session, "manageUsers", "user");
        }
        
        user.setCreatedOn(Operations.getCurrentTime());
        user.setModifiedOn(Operations.getCurrentTime());
        //user.setUserID(userService.getNewId());
        userService.saveUser(user);
        model.addAttribute("saved", "success");
        model.addAttribute("user",new User());
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
       
        return Operations.routeView(session, "manageUsers", "user");
    }

    @RequestMapping(value = "/editUser_{userID}", method = RequestMethod.GET)
    public String editUser(Model model, @PathVariable int userID, HttpSession session) {
        User user = userService.findByUserID(userID);
        model.addAttribute("user", user);
        model.addAttribute("request", "edit");

        return Operations.routeView(session, "manageUsers", "user");
    }

    @RequestMapping(value = "/editUser_2", method = RequestMethod.POST)
    public String updateUser( Model model, User user, HttpSession session) {
    	user.setModifiedOn(Operations.getCurrentTime());
        userService.updateUser(user);
        model.addAttribute("updated", "success");
        model.addAttribute("user",new User());
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        
    	String logedinUser = (String) session.getAttribute("logedinUser");
    	String userID = (String) session.getAttribute("logedinUserID");
    	if(logedinUser != null && logedinUser.equals("true")) return "redirect:/resumeUserSession_"+userID;
    	else return Operations.routeView(session, "manageUsers", "user");
    }
    @RequestMapping(value = "/deleteUser_{userID}", method = RequestMethod.GET)
    public String deleteUser(Model model, @PathVariable int userID, HttpSession session) {
    	User user = userService.findByUserID(userID);
    	userService.deleteUser(user);
        model.addAttribute("deleted", "success");
        model.addAttribute("user", new User());
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);

        return Operations.routeView(session, "manageUsers", "user");
    }

}
