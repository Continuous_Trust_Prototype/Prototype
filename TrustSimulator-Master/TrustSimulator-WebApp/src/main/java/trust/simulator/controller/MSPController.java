package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.MSP;
import trust.simulator.service.MSPService;
import trust.simulator.util.Operations;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
public class MSPController {

    @Autowired
    private MSPService mspService;
  
    @RequestMapping(value = "/manageMSPs", method = RequestMethod.GET)
    public String showMSPPage(Model model) {
        model.addAttribute("msp",new MSP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);
        return "manageMSPs";
    }

    @RequestMapping(value = "/addMSP", method = RequestMethod.POST)
    public String addMSP( Model model, MSP msp) {
        MSP existing = mspService.findByName(msp.getName());
        if (existing != null) {
            model.addAttribute("status", "exist");
            List<MSP> msps = mspService.getAllMsps();
            model.addAttribute("msps", msps);
            return "manageMSPs";
        }
        msp.setCreatedOn(Operations.getCurrentTime());
        msp.setModifiedOn(Operations.getCurrentTime());
        //msp.setMspID(mspService.getNewId());
        mspService.saveMsp(msp);
        model.addAttribute("saved", "success");
        model.addAttribute("msp",new MSP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);
        return "manageMSPs";
    }

    @RequestMapping(value = "/editMSP_{mspID}", method = RequestMethod.GET)
    public String editMSP(Model model, @PathVariable int mspID) {
        MSP msp = mspService.findByMspID(mspID);
        model.addAttribute("msp", msp);
        model.addAttribute("request", "edit");
        return "manageMSPs";
    }

    @RequestMapping(value = "/editMSP_2", method = RequestMethod.POST)
    public String editMSP_2( Model model, MSP msp) {
    	msp.setModifiedOn(Operations.getCurrentTime());
        mspService.updateMsp(msp);
        model.addAttribute("updated", "success");
        model.addAttribute("msp",new MSP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);
        return "manageMSPs";
    }
    
    @RequestMapping(value = "/deleteMSP_{MSPID}", method = RequestMethod.GET)
    public String deleteMSP(Model model, @PathVariable int MSPID) {
        mspService.deleteMsp(MSPID);
        model.addAttribute("deleted", "success");
        model.addAttribute("msp", new MSP());
        List<MSP> msps = mspService.getAllMsps();
        model.addAttribute("msps", msps);
        return "manageMSPs";
    }
    
    @RequestMapping(value = "/getExpandedMSP_{mspID}", method = RequestMethod.GET)
    public String getExpndedSP(Model model, HttpSession session, @PathVariable int mspID) {
    	    	
    	MSP msp = mspService.findByMspID(mspID);
    	model.addAttribute("msp", msp);
    	
        model.addAttribute("request", "showExpandedMSP");

        return "manageMSPs";
    }

}
