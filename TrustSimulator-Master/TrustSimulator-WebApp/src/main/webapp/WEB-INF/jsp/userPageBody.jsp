<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>

<c:choose>

<c:when test="${request == 'edit'}">
<div id="mainA">

	<h2>Edit a Current User</h2>
	<form:form modelAttribute="user" action="/TrustSimulator/editUser_2" method="post">
	<form:input type="hidden" path="ID" value="${user.ID}"/>
	<form:input type="hidden" path="name" value="${user.name}"/>
	<form:input type="hidden" path="createdOn" value="${user.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${user.modifiedOn}"/>
	<form:input type="hidden" path="email" value="${user.email}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>User ID</td></form:label>
			<td colspan='4' class='cellData'> ${user.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${user.name}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${user.password}"/></td>
		</tr>
				
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${user.email}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="phone"><td colspan='2' class='cellTitle'>Phone</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="phone" value="${user.phone}"/></td>
		</tr>

		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update User</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>

<c:otherwise>
<div id="mainA">

	<h2>Add a New User</h2>
		<c:if test="${saved == 'success'}">
			<p class="success">User Created Successfully</p>
		</c:if>
		<c:if test="${updated == 'success'}">
			<p class="success">User Updated Successfully</p>
		</c:if>
		<c:if test="${deleted == 'success'}">
			<p class="success">User Deleted Successfully</p>
		</c:if>
		<c:if test="${status == 'exist'}">
			<p class="error">User Already Exist</p>
		</c:if>
	
	<form:form modelAttribute="user" action="/TrustSimulator/addUser" method="post">
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="name" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" /></td>
		</tr>
				
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="email" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="phone"><td colspan='2' class='cellTitle'>Phone</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="phone" /></td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Add User</button></td>
		</tr>
			
	</table>
	</form:form>

	<h2>Existing Users</h2>
	
	<table class="recordsTable">	
	
	<tr class='recordsHeader'>
	<th>User ID</th>
	<th>Name</th>
	<th>Email</th>
	<th>Phone</th>
	<th>Created On</th>
	<th>Last Modified</th>
	<th>Actions</th>
	</tr>
	
	<c:forEach var="current" items="${users}">
		<tr>
		<td>${current.ID}</td>
		<td>${current.name}</td>
		<td>${current.email}</td>
		<td>${current.phone}</td>
		<td>${current.createdOn}</td>
		<td>${current.modifiedOn}</td>
		<td><a href="/TrustSimulator/resumeUserSession_${current.ID}">Resume</a> : <a href="/TrustSimulator/editUser_${current.ID}">Edit</a> : <a href="/TrustSimulator/deleteUser_${current.ID}">Delete</a></td>
		</tr>
	</c:forEach>

	</table>
</div>
</c:otherwise>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>



