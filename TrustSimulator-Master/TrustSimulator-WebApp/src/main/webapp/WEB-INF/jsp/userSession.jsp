<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="userSession" scope="session" />
<c:set var="ses_headerTitle" value="User Simulation Session..." scope="session" />
<c:set var="ses_pageTitle" value="User Simulation Session..." scope="session" />
<c:set var="ses_pageDescription" value="In this simulation page, you can choose to edit your personal details or check your inbox using the menu tabs. Plus, you can start interacting with one of the service providers listed below:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>

<c:choose>

<c:when test="${request == 'getService2'}">
<div id="mainA">

	<script>goUser();</script>
	<h2>Thanks for dealing with: </h2>
	<h1>${sp.name}</h1>
	<h2> You will be contacted shortly to get your requested: </h2>
	<h1>${sp.offer}</h1>
	<h2>You will be redirected to the Main page in few seconds</h2>
	<h3><a href='/TrustSimulator/user'>or click here to go there directly</a></h3>
	
</div>
</c:when>

<c:when test="${request == 'getService'}">

<div id="mainA">

	<h2>You are about to send your following information:</h2>
	
	<table class='recordsTable'>
		<tr class='recordsHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Name</td>
			<td colspan='4' class='cellData'> ${user.name} </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Email</td> 
			<td colspan='4' class='cellData'> ${user.email}</td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Phone</td> 
			<td colspan='4' class='cellData'> ${user.phone}</td>
		</tr>
				
	</table>
	
	<h2>To the following Service Provider:</h2>
	
	<table class='recordsTable'>
		<tr class='recordsHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Name</td>
			<td colspan='4' class='cellData'> ${sp.name} </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Description</td> 
			<td colspan='4' class='cellData'> ${sp.description}</td>
		</tr>		
	</table>
	
	<h2>In return for getting: </h2> 
	<h1>${sp.offer}</h1> 
	
	<table class='recordsTable'>	
		<tr class='formHeader'>
			<td colspan='3'><a href="/TrustSimulator/user">Cancel</a></td>
			<td colspan='3'><a href="${sp.host}/getService_2_${sp.ID}">Submit</a></td>
		</tr>
	</table>
	
	</div>
</c:when>

<c:when test="${request == 'getInbox'}">

<div id="mainA">

<h2>Welcome to your Inbox </h2>
	
	<table class="recordsTable">	
	<tr class='recordsHeader'>
	<th>Sender Name</th>
	<th>Title</th>
	<th>Date</th>
	<th>Status</th>
	</tr>
	<c:forEach var="current" items="${messages}">
		
		<c:choose>
			<c:when test="${current.status == 'unread'}">
				<tr class="unreadMessage">
			</c:when>
			<c:otherwise>
				<tr class="readMessage">
			</c:otherwise>	
		</c:choose>
		
		<td>${current.senderName}</td>
		<td><a href="/TrustSimulator/showMessage_${current.messageID}">${current.title}</a></td>
		<td>${current.createdOn}</td>
		<td>${current.status}</td>
		</tr>
	</c:forEach>
	</table>
	
</div>

</c:when>

<c:when test="${request == 'showMessage'}">

<div id="mainA">

<a href="/TrustSimulator/inbox_${user.ID}"> Back to your Inbox </a>
	
	<table class='recordsTable'>
		<tr class='recordsHeader'>
			<td colspan='4'>  </td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Date</td> 
			<td colspan='2' class='cellData'> ${message.createdOn}</td>
		</tr>

		<tr>
			<td class='cellTitle'>Sender Name</td>
			<td class='cellData'> ${message.senderName} </td>
			<td class='cellTitle'>Sender Email</td>
			<td class='cellData'> ${message.senderEmail} </td>
		</tr>
		
		<tr>
			<td class='cellTitle'>Title</td> 
			<td class='cellData'> ${message.title}</td>
			<td class='cellTitle'>Spam?</td> 
			<td class='cellData'>	<a href="/TrustSimulator/reportToAuditor_${message.messageID}"> Report to an Auditor </a> </td>
		</tr>		
		
		<tr class='recordsHeader'>
			<td colspan='4'>  </td>
		</tr>
			
		<tr>
			<td class='cellTitle'>Body</td>
			<td colspan='3' rowspan='5' class="cellData">${message.body}</td>
		</tr>

	</table>
		
</div>

</c:when>

<c:when test="${request == 'reportToAuditor'}">
<div id="mainA">

<c:set var="message" scope="session" value="${message}"/>

<h2>Choose an Auditor</h2>
	
	<table class="recordsTable">	
	<tr class='recordsHeader'>
	<th>Auditor</th>
	<th>Description</th>
	<th>Actions</th>
	</tr>
	<c:forEach var="current" items="${auditors}">
		<tr>
		<td>${current.name}</td>
		<td>${current.description}</td>
		<td>
			<a href="<c:url value='${current.host}/reportSpam_${current.ID}'>
			<c:param name='messageString' value='${messageString}' /> </c:url>">
			Report Spam </a>
		</td>
		</tr>
	</c:forEach>
	</table>
	
</div>
</c:when>

<c:when test="${request == 'reportToAuditor2'}">
<div id="mainA">

	<script>goUser();</script>
	<h2>Your SPAM have been successfully reported to: </h2>
	<h1>${auditor.name}</h1>
	<h2> You might be contacted by this auditor in case further information is needed or if a certain SP is found convicted in this SPAM: </h2>
	<h2>You will be redirected now to the Main page in few seconds</h2>
	<h3><a href='/TrustSimulator/user'>or click here to go there directly</a></h3>
	
</div>
</c:when>

<c:otherwise>
<div id="mainA">

<h2>Available Service Providers</h2>
	
	<table class="recordsTable">	
	<tr class='recordsHeader'>
	<th>Service Provider</th>
	<th>Description</th>
	<th>Offered Service</th>
	<th>Actions</th>
	</tr>
	<c:forEach var="current" items="${sps}">
		<tr>
		<td>${current.name}</td>
		<td>${current.description}</td>
		<td>${current.offer}</td>
		<td><a href="/TrustSimulator/getService_${current.ID}">Get Service</a></td>
		</tr>
	</c:forEach>
	</table>
	
</div>
</c:otherwise>

</c:choose>


<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>