<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageSPs" scope="session" />
<c:set var="ses_headerTitle" value="Manage SPs..." scope="session" />
<c:set var="ses_pageTitle" value="Manage SPs..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Add / Edit / Delete Service Providers - SPs:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'showExpandedSP'}">
<div id="mainA">
	
		<table class="recordsTable">
			<tr class='recordsHeader'>
				<td>SP ID</td>
				<td>Name</td>
				<td colspan='2'>Email</td>
			</tr>
			<tr>
				<td>${sp.ID}</td>
				<td>${sp.name}</td>
				<td colspan='2'>${sp.email}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='3'>Description</td>
				<td>Offer</td>
			</tr>
			<tr>
				<td colspan='3'>${sp.description}</td>
				<td>${sp.offer}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='2'>Host</td>
				<td>Created On</td>
				<td>Last Modified</td>
			</tr>
			<tr>
				<td colspan='2'>${sp.host}</td>
				<td>${sp.createdOn}</td>
				<td>${sp.modifiedOn}</td>
			</tr>
			<tr class='recordsHeader'>
				<td>Affiliated With Msp</td>
				<td colspan='2'>Msp Host</td>
				<td>Actions</td>
			</tr>
			<tr>
				<td>${sp.affiliatedWithMspName}</td>
				<td colspan='2'>${sp.affiliatedWithMspHost}</td>
				<td><a href="/TrustSimulator/editSP_${sp.ID}">Edit</a> : <a href="/TrustSimulator/deleteSP_${sp.ID}">Delete</a></td>
			</tr>
			
		</table>
		
		<h2><a href="/TrustSimulator/manageSPs">Go back to the Manage SPs page</a></h2>
		
		
	</div>
</c:when>


<c:when test="${request == 'edit'}">
<div id="mainA">

	<h2>Edit a Current SP</h2>
	<form:form modelAttribute="sp" action="/TrustSimulator/editSP_2" method="post">
	<form:input type="hidden" path="ID" value="${sp.ID}"/>
	<form:input type="hidden" path="name" value="${sp.name}"/>
	<form:input type="hidden" path="createdOn" value="${sp.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${sp.modifiedOn}"/>
	<form:input type="hidden" path="email" value="${sp.email}"/>
	<form:input type="hidden" path="affiliatedWithMspName" value="${sp.affiliatedWithMspName}"/>
	<form:input type="hidden" path="affiliatedWithMspHost" value="${sp.affiliatedWithMspHost}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>SP ID</td></form:label>
			<td colspan='4' class='cellData'> ${sp.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${sp.name}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${sp.password}"/></td>
		</tr>
		
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${sp.email}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" value="${sp.host}" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" value="${sp.description}" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="offer"><td colspan='2' class='cellTitle'>Offer</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="offer" value="${sp.offer}" /></td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Affiliated With MSP</td>
			<td colspan='4' class='cellData'>  
			<select name='mspID'>
			<option value='0'></option>
			<c:forEach var="currentMSP" items="${msps}">
			<c:choose>
			<c:when test="${currentMSP.ID == sp.affiliatedWithMsp}"><option value='${currentMSP.ID}' selected='selected'>${currentMSP.name}</option></c:when>
			<c:otherwise> <option value='${currentMSP.ID}'>${currentMSP.name}</option></c:otherwise>
			</c:choose>
			</c:forEach>
			</select>
			</td>
		</tr>

		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update SP</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>

<c:otherwise>
<div id="mainA">

	<h2>Add a New SP</h2>
		<c:if test="${saved == 'success'}">
			<p class="success">SP Created Successfully</p>
		</c:if>
		<c:if test="${updated == 'success'}">
			<p class="success">SP Updated Successfully</p>
		</c:if>
		<c:if test="${deleted == 'success'}">
			<p class="success">SP Deleted Successfully</p>
		</c:if>
		<c:if test="${status == 'exist'}">
			<p class="error">SP Already Exist</p>
		</c:if>
	<form:form modelAttribute="sp" action="/TrustSimulator/addSP" method="post">
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="name" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="email" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" /></td>
		</tr>
		
		<tr>
			<form:label path="offer"><td colspan='2' class='cellTitle'>Offer</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="offer" /></td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Affiliated With MSP</td>
			<td colspan='4' class='cellData'>  
			<select name='mspID'>
			<option value='0'></option>
			<c:forEach var="currentMSP" items="${msps}">
			<option value='${currentMSP.ID}'>${currentMSP.name}</option>
			</c:forEach>
			</select>
			</td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Add SP</button></td>
		</tr>
			
	</table>
	</form:form>

	<h2>Existing SPs</h2>
	
	<table class="recordsTable">
		<tr class='recordsHeader'>
			<th>Name</th>
			<th>Email</th>
			<th>Host</th>
			<th>Affiliated With Msp</th>
			<th>Actions</th>
		</tr>
		<c:forEach var="current" items="${sps}">
			<tr>
				<td>${current.name}</td>
				<td>${current.email}</td>
				<td>${current.host}</td>
				<td>${current.affiliatedWithMspName}</td>
				<td><a href="/TrustSimulator/getExpandedSP_${current.ID}">Expand</a> : <a href="/TrustSimulator/editSP_${current.ID}">Edit</a> : <a href="/TrustSimulator/deleteSP_${current.ID}">Delete</a></td>
			</tr>	
		</c:forEach>
	</table>
	
</div>
</c:otherwise>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>



