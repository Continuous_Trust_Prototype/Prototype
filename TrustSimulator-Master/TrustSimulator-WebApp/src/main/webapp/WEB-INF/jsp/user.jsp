<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="user" scope="session" />
<c:set var="ses_headerTitle" value="User View..." scope="session" />
<c:set var="ses_pageTitle" value="User View..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can resume a user session or create a new one" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<%@ include file="userPageBody.jsp" %>