<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="firstHead">

	<dl class="miniDl">

		<c:choose>
		
		<c:when test="${not empty sessionScope.logedinUser && sessionScope.logedinUser =='true'}">
		<dt id="main" class="Links">
			<a href="/TrustSimulator/user">Main</a>
		</dt>
	
		<dt id="logout" class="Links">
			<a href="j_spring_security_logout" onclick="logoutIDP()"> Logout User </a>
		</dt>
		</c:when>
		
		<c:when test="${not empty sessionScope.logedinAuditor && sessionScope.logedinAuditor =='true'}">
		<dt id="main" class="Links">
			<a href="/TrustSimulator/auditor">Main</a>
		</dt>
	
		<dt id="logout" class="Links">
			<a href="/TrustSimulator/j_spring_security_logout" > Logout Auditor </a>
		</dt>
		</c:when>
			
		<c:otherwise>
		<dt id="main" class="Links">
			<a href="/TrustSimulator">Main</a>
		</dt>
		</c:otherwise>

		</c:choose>

	</dl>
	
<c:choose>	

	<c:when test="${not empty sessionScope.logedinUser && sessionScope.logedinUser == 'true'}">
		<dl>
	
			<dt class="mainLinks">
				<a href="/TrustSimulator/editUser_${user.ID}">Edit Profile</a>
			</dt>
			
			<dt class="mainLinks">
			<c:choose>
			<c:when test="${not messagesNotification.isEmpty() }">
				<a href="/TrustSimulator/inbox_${user.ID}">Inbox (${messagesNotification.unreadMessages}) </a>
			</c:when>
			<c:otherwise>
				<a href="/TrustSimulator/inbox_${user.ID}">Inbox</a>
			</c:otherwise>
			</c:choose>
			</dt>
		</dl>	
	</c:when>
	
	<c:when test="${not empty sessionScope.logedinAuditor && sessionScope.logedinAuditor == 'true'}">
		<dl>
			<dt class="mainLinks">
				<a href="/TrustSimulator/editAuditor_${auditor.ID}">Edit Profile</a>
			</dt>
		</dl>	
	</c:when>
	
	<c:when test="${sessionScope.ses_page == 'admin' || sessionScope.ses_page == 'manageUsers' || sessionScope.ses_page == 'manageMSPs' || sessionScope.ses_page == 'manageSPs' || sessionScope.ses_page == 'manageAuditors' || sessionScope.ses_page == 'manageLogs'}">
	<dl>
		<dt class="mainLinks">
			<a href="/TrustSimulator/manageUsers">Manage Users</a>
		</dt>

		<dt class="mainLinks">
			<a href="/TrustSimulator/manageSPs">Manage Service Providers</a>
		</dt>
  
		<dt class="mainLinks">
			<a href="/TrustSimulator/manageMSPs">Manage Malicious Third Parties</a>
		</dt>
	</dl>
		
	<dl>
		<dt class="mainLinks">
			<a href="/TrustSimulator/manageAuditors">Manage Auditors</a>
		</dt>
		
		<dt  class="mainLinks">
			<a href="/TrustSimulator/manageLogs">Manage Logs</a>
		</dt>
	</dl>
</c:when>

<c:when test="${sessionScope.ses_page == 'index'}">
	<dl>
		<dt class="mainLinks">
			<a href="/TrustSimulator/admin">Admin View</a>
		</dt>
  
		<dt class="mainLinks">
			<a href="/TrustSimulator/user">User View</a>
		</dt>
		
	</dl>
</c:when>

</c:choose>

</div>
