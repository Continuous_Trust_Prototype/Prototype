<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

<style type='text/css'> @import url(site.css); </style>

<script type="text/javascript">

function logoutIDP ( )
{
	
	window.open("http://localhost:8070/TrustSimulator_IDP/logout");
	
	/*var xmlhttp;
	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
	{
	  	xmlhttp=new XMLHttpRequest();
	}
	else // code for IE6, IE5
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.open("GET","http://127.0.0.1:8070/TrustSimulator_IDP/logout",true);
	xmlhttp.send();
	xmlhttp.onreadystatechange=function()
	{
		alert('readyState: ' + xmlhttp.readyState);
		alert('status: ' + xmlhttp.status);
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
	  	{
	   		alert(xmlhttp.responseText);
	  	}
		alert('2');
		alert(xmlhttp.responseText);
	};
	*/
}

function goMain ( )
{
  	setTimeout ( "window.location = 'index.jsp'", 4000 );
}

function goMainDirectly ( )
{
 	window.location = "/TrustSimulator/index.jsp";
}

function goUser ( )
{
 	setTimeout ( "window.location = 'user'", 4000 );
}

function goAuditor ( )
{
	setTimeout ( "window.location = 'auditor'", 4000 );
}

$(function() {
	$("#cancel").click(function(event) {
    	$("#user").attr("action", "/TrustSimulator/user");
    	$("#user").attr("method", "get");
		$("#user").submit();
	});
	
});

</script>

<title> ${sessionScope.ses_headerTitle}</title>
