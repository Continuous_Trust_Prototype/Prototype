<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageLogs" scope="session" />
<c:set var="ses_headerTitle" value="Manage Logs..." scope="session" />
<c:set var="ses_pageTitle" value="Manage Logs..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can View the Logs stored in the simulation system" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'showUserLog'}">
<div id="mainA">

	<a href="/TrustSimulator/manageLogs">Go back to show another Log </a>

	<c:set var="ses_userLog" value="${user.ID}" scope="session" />
	
	<h2>This is the Log for User: ${user.ID} whose Name is ${user.name} </h2>
	
	<table class='recordsTable'>
		<tr class='recordsHeader'>
			<td colspan='2'>Time</td>
			<td colspan='3'> Action </td>
			<td> More Details </td>
		</tr>
		<c:forEach var="current" items="${abstractLogs}">
		<tr>
			<td colspan='2'>${current.createdOn}</td>
		
			<td colspan='3'>
		
			<c:choose>
			
				<c:when test="${current.getClass().simpleName eq 'UserLog'}">
					<c:choose>
						<c:when test="${current.action eq 'save'}">
							User Created
						</c:when>
						<c:when test="${current.action eq 'update'}">
							User Updated
						</c:when>
						<c:when test="${current.action eq 'delete'}">
							User Deleted
						</c:when>
					</c:choose>
				</c:when>
				
				<c:when test="${current.getClass().simpleName eq 'IDPLog'}">
					IDP ${current.IDPHost} released userInfo to Client ${current.IDPClientHost}
				</c:when>
				
				<c:when test="${current.getClass().simpleName eq 'SPDataLog'}">
					SP ${current.spdataSPID} got user credentials
				</c:when>
				
				<c:when test="${current.getClass().simpleName eq 'MSPDataLog'}">
					<c:choose>
						<c:when test="${current.action eq 'save'}">
							MSP ${current.mspdataMSPID} got user credentials from SP ${current.mspdataSPID}
						</c:when>
						<c:when test="${current.action eq 'update'}">
							MSP ${current.mspdataMSPID} used user credentials
						</c:when>
					</c:choose>
				</c:when>
				
				<c:when test="${current.getClass().simpleName eq 'MessageLog'}">
					<c:choose>
						<c:when test="${current.action eq 'save'}">
							User received Message ${current.messageID} from ${current.senderEmail}
						</c:when>
						<c:when test="${current.action eq 'update'}">
							User read Message ${current.messageID}
						</c:when>
					</c:choose>
				</c:when>
				
				<c:when test="${current.getClass().simpleName eq 'AuditorCaseLog'}">
					<c:choose>
						<c:when test="${current.action eq 'save'}">
							AuditorCase ${current.auditorcaseID} is created by Auditor ${current.auditorcaseAuditorID} for a reported SPAM by User
						</c:when>
						<c:when test="${current.action eq 'update'}">
							Auditor ${current.auditorcaseAuditorID} updated AuditorCase ${current.auditorcaseID}
						</c:when>
					</c:choose>
				</c:when>
				
				<c:otherwise>
				Log: ${current.getClass().simpleName}
				</c:otherwise>
			
			</c:choose>
			
			</td>
			
			<td><a href="/TrustSimulator/getExpandedLog_${current.ID}">Expand</a></td>
		</tr>
		</c:forEach>
					
	</table>
	
	</div>
</c:when>

<c:when test="${request == 'showExpandedLog'}">
<div id="mainA">

	<a href="/TrustSimulator/getUserLog">Go back to the User Log</a>
	
	<c:choose>
			
		<c:when test="${log.getClass().simpleName eq 'UserLog'}">
			<table class='recordsTable'>
				<tr class='recordsHeader'>
					<td>Time</td>
					<td>UserID</td>
					<td>User Name</td>
					<td>User Email</td>
					<td>User Phone</td>
					<td>Committed Action</td>
				</tr>
				<tr>
					<td>${log.createdOn}</td>
					<td>${log.entityID}</td>
					<td>${log.name}</td>
					<td>${log.email}</td>
					<td>${log.userPhone}</td>
					<td>${log.action}</td>
				</tr>				
			</table>
		</c:when>
		
		<c:when test="${log.getClass().simpleName eq 'IDPLog'}">
			<table class='recordsTable'>
				<tr class='recordsHeader'>
					<td>Time</td>
					<td>Scope of released Data</td>
					<td>IDP Host</td>
				</tr>
				<tr>
					<td>${log.createdOn}</td>
					<td>${log.IDPScope}</td>
					<td>${log.IDPHost}</td>
				</tr>
				<tr class='recordsHeader'>
					<td>Client Host</td>
					<td>User Sub</td>
					<td>Committed Action</td>
				</tr>
				<tr>
					<td>${log.IDPClientHost}</td>
					<td>${log.IDPUserSub}</td>
					<td>${log.action}</td>
				</tr>				
			</table>			
		</c:when>		

		<c:when test="${log.getClass().simpleName eq 'SPDataLog'}">
			<table class='recordsTable'>
				<tr class='recordsHeader'>
					<td>Time</td>
					<td>SP Record ID</td>
					<td>SP ID</td>
					<td>Data Obtained From User ID</td>
					<td>Obtained Data Type</td>
					<td>Obtained Data Value</td>
					<td>Committed Action</td>
				</tr>
				<tr>
					<td>${log.createdOn}</td>
					<td>${log.spdataID}</td>
					<td>${log.spdataSPID}</td>
					<td>${log.spdataUserID}</td>
					<td>${log.obtainedDataType}</td>
					<td>${log.obtainedDataValue}</td>
					<td>${log.action}</td>
				</tr>				
			</table>			
		</c:when>
		
		<c:when test="${log.getClass().simpleName eq 'MSPDataLog'}">
			<table class='recordsTable'>
				<tr class='recordsHeader'>
					<td>Time</td>
					<td>MSP Record ID</td>
					<td>MSP ID</td>
					<td>Leaked From SP ID</td>
					<td>Data Obtained From User ID</td>
					<td>Obtained Data Type</td>
					<td>Obtained Data Value</td>
					<td>Record Usage Count</td>
					<td>Committed Action</td>
				</tr>
				<tr>
					<td>${log.createdOn}</td>
					<td>${log.mspdataID}</td>
					<td>${log.mspdataMSPID}</td>
					<td>${log.mspdataSPID}</td>
					<td>${log.mspdataUserID}</td>
					<td>${log.obtainedDataType}</td>
					<td>${log.obtainedDataValue}</td>
					<td>${log.usedCount}</td>
					<td>${log.action}</td>
				</tr>				
			</table>			
		</c:when>
		
		<c:when test="${log.getClass().simpleName eq 'MessageLog'}">
			<table class='recordsTable'>
				<tr class='recordsHeader'>
					<td>Time</td>
					<td>Message ID</td>
					<td>Sender Name</td>
					<td>Sender Email</td>
					<td>Receiver Name</td>
					<td>receiver Email</td>
					<td>Title</td>
					<td>Body</td>
					<td>Status</td>
					<td>Committed Action</td>
				</tr>
				<tr>
					<td>${log.createdOn}</td>
					<td>${log.messageID}</td>
					<td>${log.senderName}</td>
					<td>${log.senderEmail}</td>
					<td>${log.receiverName}</td>
					<td>${log.receiverEmail}</td>
					<td>${log.title}</td>
					<td><script type="text/plain">${log.body}</script></td>
					<td>${log.status}</td>
					<td>${log.action}</td>
				</tr>				
			</table>
		</c:when>
		
		<c:when test="${log.getClass().simpleName eq 'AuditorCaseLog'}">
			<table class='recordsTable'>
				<tr class='recordsHeader'>
					<td>Time</td>
					<td>AuditorCase ID</td>
					<td>Auditor ID</td>
					<td>Reporting User ID</td>
					<td>Reported Message ID</td>
					<td>Investigated Data Type</td>
					<td>Investigated Data Value</td>
					<td>Status</td>
					<td>Convicted SPID</td>
					<td>Committed Action</td>
				</tr>
				<tr>
					<td>${log.createdOn}</td>
					<td>${log.auditorcaseID}</td>
					<td>${log.auditorcaseAuditorID}</td>
					<td>${log.auditorcaseUserID}</td>
					<td>${log.auditorcaseMessageID}</td>
					<td>${log.investigatedDataType}</td>
					<td>${log.investigatedDataValue}</td>
					<td>${log.status}</td>
					<td>${log.convictedSPID}</td>
					<td>${log.action}</td>
				</tr>				
			</table>
		</c:when>
			
	</c:choose>
	
	</div>
</c:when>

<c:otherwise>
<div id="mainA">

	<h2>Select a user log</h2>
	<form:form action="/TrustSimulator/getUserLog" method="post">
	<table class='formTable'>
		<tr class='formHeader'>
			<td>  </td>
		</tr>
		<tr>
			<td class='cellData'>  
			<select name='userID'>
			<option value='0'></option>
			<c:forEach var="currentUser" items="${users}">
			<option value='${currentUser.ID}'>${currentUser.ID}_${currentUser.name}</option>
			</c:forEach>
			</select>
			</td>
		</tr>
		
		<tr class='formHeader'>
			<td><button type="submit" class='buttonSubmit'>Check</button></td>
		</tr>
			
	</table>
	</form:form>
	
	</div>
</c:otherwise>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>



