<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageMSPs" scope="session" />
<c:set var="ses_headerTitle" value="Manage MSPs..." scope="session" />
<c:set var="ses_pageTitle" value="Manage MSPs..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Add / Edit / Delete Malicious Third Parties - MSPs:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'showExpandedMSP'}">
<div id="mainA">
	
		<table class="recordsTable">
			<tr class='recordsHeader'>
				<td>MSP ID</td>
				<td>Name</td>
				<td colspan='2'>Email</td>
			</tr>
			<tr>
				<td>${msp.ID}</td>
				<td>${msp.name}</td>
				<td colspan='2'>${msp.email}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='3'>Description</td>
				<td>Interested In</td>
			</tr>
			<tr>
				<td colspan='3'>${msp.description}</td>
				<td>${msp.interestedIn}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='2'>Host</td>
				<td>Created On</td>
				<td>Last Modified</td>
			</tr>
			<tr>
				<td colspan='2'>${msp.host}</td>
				<td>${msp.createdOn}</td>
				<td>${msp.modifiedOn}</td>
			</tr>
			<tr class='recordsHeader'>
				<td colspan='4'>Actions</td>
			</tr>
			<tr>
				<td><a href="/TrustSimulator/editMSP_${msp.ID}">Edit</a> : <a href="/TrustSimulator/deleteMSP_${msp.ID}">Delete</a></td>
			</tr>
			
		</table>
		
		<h2><a href="/TrustSimulator/manageMSPs">Go back to the Manage MSPs page</a></h2>
		
		
	</div>
</c:when>

<c:when test="${request == 'edit'}">
<div id="mainA">

	<h2>Edit a Current MSP</h2>
	<form:form modelAttribute="msp" action="/TrustSimulator/editMSP_2" method="post">
	<form:input type="hidden" path="ID" value="${msp.ID}"/>
	<form:input type="hidden" path="name" value="${msp.name}"/>
	<form:input type="hidden" path="createdOn" value="${msp.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${msp.modifiedOn}"/>
	<form:input type="hidden" path="interestedIn" value="${msp.interestedIn}"/>
	<form:input type="hidden" path="email" value="${msp.email}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>MSP ID</td></form:label>
			<td colspan='4' class='cellData'> ${msp.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${msp.name} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${msp.password}"/></td>
		</tr>
		
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${msp.email}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" value="${msp.host}" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" value="${msp.description}" /></td>
		</tr>

		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update MSP</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>

<c:otherwise>
<div id="mainA">

	<h2>Add a New MSP</h2>
		<c:if test="${saved == 'success'}">
			<p class="success">MSP Created Successfully</p>
		</c:if>
		<c:if test="${updated == 'success'}">
			<p class="success">MSP Updated Successfully</p>
		</c:if>
		<c:if test="${deleted == 'success'}">
			<p class="success">MSP Deleted Successfully</p>
		</c:if>
		<c:if test="${status == 'exist'}">
			<p class="error">MSP Already Exist</p>
		</c:if>
	<form:form modelAttribute="msp" action="/TrustSimulator/addMSP" method="post">
	<form:input type="hidden" path="interestedIn" value="email"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="name" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="email" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" /></td>
		</tr>
		
		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Add MSP</button></td>
		</tr>
			
	</table>
	</form:form>

	<h2>Existing MSPs</h2>
	
	<table class="recordsTable">	
	
	<tr class='recordsHeader'>
	<th>Name</th>
	<th>Email</th>
	<th>Host</th>
	<th>Interested In</th>
	<th>Actions</th>
	</tr>
	<c:forEach var="current" items="${msps}">
		<tr>
		<td>${current.name}</td>
		<td>${current.email}</td>
		<td>${current.host}</td>
		<td>${current.interestedIn}</td>
		<td><a href="/TrustSimulator/getExpandedMSP_${current.ID}">Expand</a> : <a href="/TrustSimulator/editMSP_${current.ID}">Edit</a> : <a href="/TrustSimulator/deleteMSP_${current.ID}">Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</div>
</c:otherwise>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>



