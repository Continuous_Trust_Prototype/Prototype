<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageUsers" scope="session" />
<c:set var="ses_headerTitle" value="Manage Users..." scope="session" />
<c:set var="ses_pageTitle" value="Manage Users..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Add / Edit / Delete Users:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<%@ include file="userPageBody.jsp" %>

