/*******************************************************************************
 * Copyright 2014 The MITRE Corporation
 *   and the MIT Kerberos and Internet Trust Consortium
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package org.mitre.openid.connect.web;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.mitre.oauth2.model.ClientDetailsEntity;
import org.mitre.oauth2.service.ClientDetailsEntityService;
import org.mitre.openid.connect.model.UserInfo;
import org.mitre.openid.connect.service.UserInfoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.common.base.Strings;


//import trust.simulator.common.FileOutput;
import trust.simulator.entity.IDPLog;

import trust.simulator.service.IDPLogService;

/**
 * OpenID Connect UserInfo endpoint, as specified in Standard sec 5 and Messages sec 2.4.
 * 
 * @author AANGANES
 *
 */
@Controller
public class UserInfoEndpoint {

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private ClientDetailsEntityService clientService;
	
	@Autowired
	private IDPLogService idplogService;

	private static Logger logger = LoggerFactory.getLogger(UserInfoEndpoint.class);

	/**
	 * Get information about the user as specified in the accessToken included in this request
	 */
	@PreAuthorize("hasRole('ROLE_USER') and #oauth2.hasScope('openid')")
	@RequestMapping(value="/userinfo", method= {RequestMethod.GET, RequestMethod.POST}, produces = {"application/json", "application/jwt"})
	public String getInfo(@RequestParam(value="claims", required=false) String claimsRequestJsonString,
			@RequestHeader(value="Accept") String acceptHeader,
			OAuth2Authentication auth, Model model, HttpServletRequest request) {

		if (auth == null) {
			logger.error("getInfo failed; no principal. Requester is not authorized.");
			model.addAttribute("code", HttpStatus.FORBIDDEN);
			return "httpCodeView";
		}

		String username = auth.getName();
		UserInfo userInfo = userInfoService.getByUsernameAndClientId(username, auth.getOAuth2Request().getClientId());

		if (userInfo == null) {
			logger.error("getInfo failed; user not found: " + username);
			model.addAttribute("code", HttpStatus.NOT_FOUND);
			return "httpCodeView";
		}

		model.addAttribute("scope", auth.getOAuth2Request().getScope());

		model.addAttribute("authorizedClaims", auth.getOAuth2Request().getExtensions().get("claims"));

		if (!Strings.isNullOrEmpty(claimsRequestJsonString)) {
			model.addAttribute("requestedClaims", claimsRequestJsonString);
		}

		model.addAttribute("userInfo", userInfo);

		/*new FileOutput("userInfo Endpoint called: userInfo loaded");
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getClientId(): "+auth.getOAuth2Request().getClientId());
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getRedirectUri(): "+auth.getOAuth2Request().getRedirectUri());
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getRequestParameters(): "+auth.getOAuth2Request().getRequestParameters());
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getScope(): "+auth.getOAuth2Request().getScope());
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getExtensions().get('claims'): "+auth.getOAuth2Request().getExtensions().get("claims"));
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getExtensions().toString(): "+auth.getOAuth2Request().getExtensions().toString());
		new FileOutput("userInfo Endpoint called: auth.getOAuth2Request().getRequestParameters().toString(): "+auth.getOAuth2Request().getRequestParameters().toString());
		new FileOutput("userInfo Endpoint called: auth.getDetails().toString(): "+auth.getDetails().toString());
		new FileOutput("userInfo Endpoint called: auth.getDetails().getClass().toString(): "+auth.getDetails().getClass().toString());
		new FileOutput("userInfo Endpoint called: auth.getAuthorities().toString(): "+auth.getAuthorities().toString());
		new FileOutput("userInfo Endpoint called: auth.getUserAuthentication().toString(): "+auth.getUserAuthentication().toString());
		new FileOutput("userInfo Endpoint called: auth.getUserAuthentication().getDetails().toString(): "+auth.getUserAuthentication().getDetails().toString());
		new FileOutput("userInfo Endpoint called: auth.getUserAuthentication().getAuthorities().toString(): "+auth.getUserAuthentication().getAuthorities().toString());
		*/
		OAuth2AuthenticationDetails token =  (OAuth2AuthenticationDetails) auth.getDetails();
		
		/*new FileOutput("userInfo Endpoint called: token value: "+token.getTokenValue());
		new FileOutput("userInfo Endpoint called: getSessionId: "+token.getSessionId());
		new FileOutput("userInfo Endpoint called: request.getRequestURL().toString(): "+request.getRequestURL().toString()); 
		*/
		String[] splitStrings = request.getRequestURL().toString().split("/");
		String host = "http://"+splitStrings[2]+"/"+splitStrings[3];
		//new FileOutput("userInfo Endpoint called: host: "+host);

		String[] splitStrings2 = auth.getOAuth2Request().getRedirectUri().split("/");
		String clientHost = "http://"+splitStrings2[2]+"/"+splitStrings2[3];
		//new FileOutput("userInfo Endpoint called: clientHost: "+clientHost);
		
		//new FileOutput("userInfo Endpoint called: userInfo.getSub(): "+userInfo.getSub());
		
		IDPLog idpLog = new IDPLog(auth.getOAuth2Request().getScope().toString(),token.getTokenValue(), host, clientHost,userInfo.getSub(),"UserInfo Release" );
		idplogService.saveLog(idpLog);
		
		// content negotiation
		List<MediaType> mediaTypes = MediaType.parseMediaTypes(acceptHeader);
		MediaType.sortBySpecificityAndQuality(mediaTypes);

		MediaType jose = new MediaType("application", "jwt");

		for (MediaType m : mediaTypes) {
			if (!m.isWildcardType() && m.isCompatibleWith(jose)) {
				ClientDetailsEntity client = clientService.loadClientByClientId(auth.getOAuth2Request().getClientId());
				model.addAttribute("client", client);
				//new FileOutput("UserInfo Endpoint called: client loaded");

				return "userInfoJwtView";
			}
		}

		return "userInfoView";

	}

}
