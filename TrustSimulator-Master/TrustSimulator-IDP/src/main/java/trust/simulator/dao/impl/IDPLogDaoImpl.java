package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.IDPLogDao;
import trust.simulator.entity.IDPLog;

@Repository
public class IDPLogDaoImpl extends AbstractDaoImpl<IDPLog, String> implements IDPLogDao {

    protected IDPLogDaoImpl() {
        super(IDPLog.class);
    }
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorAddIDPLogPath}")
    private String simulator_addIDPLog_path;

	//Log Events
    @Override
    public IDPLog save(IDPLog e) {
        getCurrentSession().persist(e);
        
        try {
        	simulatorApi.addIDPLog(e, simulator_host, simulator_addIDPLog_path);
		}catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(IDPLog e) {
        getCurrentSession().merge(e);
        try {
        	simulatorApi.addIDPLog(e, simulator_host, simulator_addIDPLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(IDPLog e) {
        getCurrentSession().remove(e);
        try {
        	simulatorApi.addIDPLog(e, simulator_host, simulator_addIDPLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }
    
}
