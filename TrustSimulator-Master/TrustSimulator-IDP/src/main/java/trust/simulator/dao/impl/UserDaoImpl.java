package trust.simulator.dao.impl;



import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import trust.simulator.dao.UserDao;
import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.User;
import trust.simulator.entity.UserLog;
import trust.simulator.oauth2.mapping.Authorities_oAuth2;
import trust.simulator.oauth2.mapping.Userinfo_oAuth2;
import trust.simulator.oauth2.mapping.Users_oAuth2;
import trust.simulator.service.UserService;

@Repository
public class UserDaoImpl extends AbstractDaoImpl<User, String> implements UserDao {

    protected UserDaoImpl() {
        super(User.class);
    }
    
    @Autowired
    private UserService userService;
    
	//Log Events
    @Override
    public User save(User e) {
        
    	UserLog userLog = new UserLog(e, "save");
        getCurrentSession().persist(userLog);
        
        Users_oAuth2 oAuth2User = new Users_oAuth2(e.getName(), e.getPassword(), 1);
        getCurrentSession().persist(oAuth2User);
        
        Authorities_oAuth2 Role_User = new Authorities_oAuth2(e.getName(),"ROLE_USER");
        getCurrentSession().persist(Role_User);
        Authorities_oAuth2 Role_End_User = new Authorities_oAuth2(e.getName(),"ROLE_END_USER");
        getCurrentSession().persist(Role_End_User);
        
        int userPhone = 0;
        
        if(e.getPhone() != null && !e.getPhone().equals("")) userPhone = Integer.parseInt(e.getPhone());
        
        Userinfo_oAuth2 oAuth2Userinfo = new Userinfo_oAuth2(e.getID(), e.getSub(),e.getName(),e.getName(),e.getEmail(),1,userPhone,1);
        getCurrentSession().persist(oAuth2Userinfo);
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(User e) {
       
        UserLog userLog = new UserLog(e, "update");
        getCurrentSession().persist(userLog);
        
        Users_oAuth2 oAuth2User = new Users_oAuth2(e.getName(), e.getPassword(), 1);
        getCurrentSession().merge(oAuth2User);
        
        Userinfo_oAuth2 oAuth2Userinfo = new Userinfo_oAuth2(e.getID(), e.getSub(),e.getName(),e.getName(),e.getEmail(),1,Integer.getInteger(e.getPhone()),1);
        getCurrentSession().merge(oAuth2Userinfo);
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(User e) {

        Users_oAuth2 oAuth2User = new Users_oAuth2(e.getName(), e.getPassword(), 1);
        getCurrentSession().remove(getCurrentSession().merge(oAuth2User));
        
        Authorities_oAuth2 Role_User = new Authorities_oAuth2(e.getName(),"ROLE_USER");
        getCurrentSession().remove(getCurrentSession().merge(Role_User));
        Authorities_oAuth2 Role_End_User = new Authorities_oAuth2(e.getName(),"ROLE_END_USER");
        getCurrentSession().remove(getCurrentSession().merge(Role_End_User));
        
    	Userinfo_oAuth2 oAuth2Userinfo = new Userinfo_oAuth2(e.getID());
        getCurrentSession().remove(getCurrentSession().merge(oAuth2Userinfo));
    	
    	UserLog userLog = new UserLog(e, "remove");
        getCurrentSession().persist(userLog);
        
        getCurrentSession().flush();
    }
    
    @Override
	public List<AbstractLog> getAllUserLogs(int userID) {
    	
    	Userinfo_oAuth2 oAuth2Userinfo = getCurrentSession().find(Userinfo_oAuth2.class, userID);
    	
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractLog where "
    			+"entityID = '"+userID+"' OR  auditorcaseUserID = '"+userID+"' OR mspdataUserID = '"+userID+"' "
    			+"OR spdataUserID = '"+userID+"' OR messageReceiverEmail = '"+oAuth2Userinfo.getEmail()+"' OR "
    			+"messageSenderEmail = '"+oAuth2Userinfo.getEmail()+"' OR IDPUserSub = '"+oAuth2Userinfo.getSub()+"'", AbstractLog.class);
    	
    	@SuppressWarnings("unchecked")
		List<AbstractLog> resultList = query.getResultList();
		return resultList;    	
	}
    
    @Override
	public List<AbstractLog> getAllUserLogs(int userID, String untilTime) {
    	
    	Userinfo_oAuth2 oAuth2Userinfo = getCurrentSession().find(Userinfo_oAuth2.class, userID);
    	
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractLog where ("
    			+"entityID = '"+userID+"' OR  auditorcaseUserID = '"+userID+"' OR mspdataUserID = '"+userID+"' "
    			+"OR spdataUserID = '"+userID+"' OR messageReceiverEmail = '"+oAuth2Userinfo.getEmail()+"' OR "
    			+"messageSenderEmail = '"+oAuth2Userinfo.getEmail()+"' OR IDPUserSub = '"+oAuth2Userinfo.getSub()+"') AND createdOn < '"+untilTime+"'", AbstractLog.class);
    	@SuppressWarnings("unchecked")
		List<AbstractLog> resultList = query.getResultList();
		return resultList;    	
	}
    
}
