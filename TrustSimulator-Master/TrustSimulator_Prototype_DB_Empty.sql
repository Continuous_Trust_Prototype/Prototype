-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2017 at 08:10 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `TrustSimulator`
--
CREATE DATABASE `TrustSimulator` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pairwise_identifier`
--

CREATE TABLE `pairwise_identifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(256) DEFAULT NULL,
  `sub` varchar(256) DEFAULT NULL,
  `sector_identifier` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_Auditor1`
--
CREATE DATABASE `TrustSimulator_Auditor1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_Auditor1`;

-- --------------------------------------------------------

--
-- Table structure for table `AUDITORCASE`
--

CREATE TABLE `AUDITORCASE` (
  `AUDITORCASEID` int(11) NOT NULL,
  `Comment` mediumtext,
  `CreatedOn` datetime DEFAULT NULL,
  `InvestigatedDataType` varchar(32) DEFAULT NULL,
  `InvestigatedDataValue` varchar(255) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `Status` varchar(255) NOT NULL,
  `AuditorID` int(11) NOT NULL,
  `MessageID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ConvictedSPID` int(11) DEFAULT NULL,
  `UserName` varchar(32) NOT NULL,
  PRIMARY KEY (`AUDITORCASEID`),
  KEY `FK_AUDITORCASE_MessageID` (`MessageID`),
  KEY `FK_AUDITORCASE_UserID` (`UserID`),
  KEY `FK_AUDITORCASE_ConvictedSPID` (`ConvictedSPID`),
  KEY `FK_AUDITORCASE_AuditorID` (`AuditorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AUDITORCASE`
--
ALTER TABLE `AUDITORCASE`
  ADD CONSTRAINT `AUDITORCASE_ibfk_1` FOREIGN KEY (`AuditorID`) REFERENCES `AbstractEntity` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `AUDITORCASE_ibfk_2` FOREIGN KEY (`MessageID`) REFERENCES `MESSAGE` (`MESSAGEID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_AUDITORCASE_AuditorID` FOREIGN KEY (`AuditorID`) REFERENCES `AbstractEntity` (`ID`),
  ADD CONSTRAINT `FK_AUDITORCASE_MessageID` FOREIGN KEY (`MessageID`) REFERENCES `MESSAGE` (`MESSAGEID`);
--
-- Database: `TrustSimulator_Auditor2`
--
CREATE DATABASE `TrustSimulator_Auditor2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_Auditor2`;

-- --------------------------------------------------------

--
-- Table structure for table `AUDITORCASE`
--

CREATE TABLE `AUDITORCASE` (
  `AUDITORCASEID` int(11) NOT NULL,
  `Comment` mediumtext,
  `CreatedOn` datetime DEFAULT NULL,
  `InvestigatedDataType` varchar(32) DEFAULT NULL,
  `InvestigatedDataValue` varchar(255) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `Status` varchar(255) NOT NULL,
  `AuditorID` int(11) NOT NULL,
  `MessageID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ConvictedSPID` int(11) DEFAULT NULL,
  `UserName` varchar(32) NOT NULL,
  PRIMARY KEY (`AUDITORCASEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_IDP`
--
CREATE DATABASE `TrustSimulator_IDP` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_IDP`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `formatted` varchar(256) DEFAULT NULL,
  `street_address` varchar(256) DEFAULT NULL,
  `locality` varchar(256) DEFAULT NULL,
  `region` varchar(256) DEFAULT NULL,
  `postal_code` varchar(256) DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `approved_site`
--

CREATE TABLE `approved_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(4096) DEFAULT NULL,
  `client_id` varchar(4096) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT NULL,
  `access_date` timestamp NULL DEFAULT NULL,
  `timeout_date` timestamp NULL DEFAULT NULL,
  `whitelisted_site_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `approved_site_scope`
--

CREATE TABLE `approved_site_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `authentication_holder`
--

CREATE TABLE `authentication_holder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) DEFAULT NULL,
  `authentication` longblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `ix_authority` (`username`,`authority`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `authorization_code`
--

CREATE TABLE `authorization_code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(256) DEFAULT NULL,
  `authentication` longblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `blacklisted_site`
--

CREATE TABLE `blacklisted_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uri` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `client_authority`
--

CREATE TABLE `client_authority` (
  `owner_id` bigint(20) DEFAULT NULL,
  `authority` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_contact`
--

CREATE TABLE `client_contact` (
  `owner_id` bigint(20) DEFAULT NULL,
  `contact` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_default_acr_value`
--

CREATE TABLE `client_default_acr_value` (
  `owner_id` bigint(20) DEFAULT NULL,
  `default_acr_value` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_details`
--

CREATE TABLE `client_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_description` varchar(1024) DEFAULT NULL,
  `reuse_refresh_tokens` tinyint(1) NOT NULL DEFAULT '1',
  `dynamically_registered` tinyint(1) NOT NULL DEFAULT '0',
  `allow_introspection` tinyint(1) NOT NULL DEFAULT '0',
  `id_token_validity_seconds` bigint(20) NOT NULL DEFAULT '600',
  `client_id` varchar(256) DEFAULT NULL,
  `client_secret` varchar(2048) DEFAULT NULL,
  `access_token_validity_seconds` bigint(20) DEFAULT NULL,
  `refresh_token_validity_seconds` bigint(20) DEFAULT NULL,
  `application_type` varchar(256) DEFAULT NULL,
  `client_name` varchar(256) DEFAULT NULL,
  `token_endpoint_auth_method` varchar(256) DEFAULT NULL,
  `subject_type` varchar(256) DEFAULT NULL,
  `logo_uri` varchar(2048) DEFAULT NULL,
  `policy_uri` varchar(2048) DEFAULT NULL,
  `client_uri` varchar(2048) DEFAULT NULL,
  `tos_uri` varchar(2048) DEFAULT NULL,
  `jwks_uri` varchar(2048) DEFAULT NULL,
  `sector_identifier_uri` varchar(2048) DEFAULT NULL,
  `request_object_signing_alg` varchar(256) DEFAULT NULL,
  `user_info_signed_response_alg` varchar(256) DEFAULT NULL,
  `user_info_encrypted_response_alg` varchar(256) DEFAULT NULL,
  `user_info_encrypted_response_enc` varchar(256) DEFAULT NULL,
  `id_token_signed_response_alg` varchar(256) DEFAULT NULL,
  `id_token_encrypted_response_alg` varchar(256) DEFAULT NULL,
  `id_token_encrypted_response_enc` varchar(256) DEFAULT NULL,
  `token_endpoint_auth_signing_alg` varchar(256) DEFAULT NULL,
  `default_max_age` bigint(20) DEFAULT NULL,
  `require_auth_time` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `initiate_login_uri` varchar(2048) DEFAULT NULL,
  `post_logout_redirect_uri` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `client_grant_type`
--

CREATE TABLE `client_grant_type` (
  `owner_id` bigint(20) DEFAULT NULL,
  `grant_type` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_redirect_uri`
--

CREATE TABLE `client_redirect_uri` (
  `owner_id` bigint(20) DEFAULT NULL,
  `redirect_uri` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_request_uri`
--

CREATE TABLE `client_request_uri` (
  `owner_id` bigint(20) DEFAULT NULL,
  `request_uri` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_resource`
--

CREATE TABLE `client_resource` (
  `owner_id` bigint(20) DEFAULT NULL,
  `resource_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_response_type`
--

CREATE TABLE `client_response_type` (
  `owner_id` bigint(20) DEFAULT NULL,
  `response_type` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client_scope`
--

CREATE TABLE `client_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pairwise_identifier`
--

CREATE TABLE `pairwise_identifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(256) DEFAULT NULL,
  `sub` varchar(256) DEFAULT NULL,
  `sector_identifier` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `refresh_token`
--

CREATE TABLE `refresh_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `system_scope`
--

CREATE TABLE `system_scope` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scope` varchar(256) NOT NULL,
  `description` varchar(4096) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `allow_dyn_reg` tinyint(1) NOT NULL DEFAULT '0',
  `default_scope` tinyint(1) NOT NULL DEFAULT '0',
  `structured` tinyint(1) NOT NULL DEFAULT '0',
  `structured_param_description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `scope` (`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `token_scope`
--

CREATE TABLE `token_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sub` varchar(256) DEFAULT NULL,
  `preferred_username` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `given_name` varchar(256) DEFAULT NULL,
  `family_name` varchar(256) DEFAULT NULL,
  `middle_name` varchar(256) DEFAULT NULL,
  `nickname` varchar(256) DEFAULT NULL,
  `profile` varchar(256) DEFAULT NULL,
  `picture` varchar(256) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  `gender` varchar(256) DEFAULT NULL,
  `zone_info` varchar(256) DEFAULT NULL,
  `locale` varchar(256) DEFAULT NULL,
  `phone_number` varchar(256) DEFAULT NULL,
  `phone_number_verified` tinyint(1) DEFAULT NULL,
  `address_id` varchar(256) DEFAULT NULL,
  `updated_time` varchar(256) DEFAULT NULL,
  `birthdate` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `whitelisted_site`
--

CREATE TABLE `whitelisted_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creator_user_id` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `whitelisted_site_scope`
--

CREATE TABLE `whitelisted_site_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
--
-- Database: `TrustSimulator_MSP1`
--
CREATE DATABASE `TrustSimulator_MSP1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_MSP1`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MSPDATA`
--

CREATE TABLE `MSPDATA` (
  `MSPDATAID` int(11) NOT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UserID` int(11) NOT NULL,
  `ObtainedDataType` varchar(32) DEFAULT NULL,
  `ObtainedDataValue` varchar(255) DEFAULT NULL,
  `UsedCount` int(11) NOT NULL,
  `MSPID` int(11) NOT NULL,
  `SPID` int(11) NOT NULL,
  `SPHost` varchar(255) NOT NULL,
  PRIMARY KEY (`MSPDATAID`),
  KEY `FK_MSPDATA_MSPID` (`MSPID`),
  KEY `FK_MSPDATA_SPID` (`SPID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_MSP2`
--
CREATE DATABASE `TrustSimulator_MSP2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_MSP2`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MSPDATA`
--

CREATE TABLE `MSPDATA` (
  `MSPDATAID` int(11) NOT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UserID` int(11) NOT NULL,
  `ObtainedDataType` varchar(32) DEFAULT NULL,
  `ObtainedDataValue` varchar(255) DEFAULT NULL,
  `UsedCount` int(11) NOT NULL,
  `MSPID` int(11) NOT NULL,
  `SPID` int(11) NOT NULL,
  `SPHost` varchar(255) NOT NULL,
  PRIMARY KEY (`MSPDATAID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_SP1`
--
CREATE DATABASE `TrustSimulator_SP1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP1`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`),
  KEY `FK_SPDATA_spID` (`spID`),
  KEY `FK_SPDATA_userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_SP2`
--
CREATE DATABASE `TrustSimulator_SP2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP2`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_SP3`
--
CREATE DATABASE `TrustSimulator_SP3` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP3`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
--
-- Database: `TrustSimulator_SP4`
--
CREATE DATABASE `TrustSimulator_SP4` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP4`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
