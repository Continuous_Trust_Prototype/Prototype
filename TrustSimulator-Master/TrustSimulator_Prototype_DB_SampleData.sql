-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 18, 2017 at 08:11 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.11

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `TrustSimulator`
--
CREATE DATABASE `TrustSimulator` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('MSP', 57, '1234', '2014-10-17 15:15:35', '', 'MSP1@TrustSimulator.com', NULL, '2014-10-17 15:15:35', 'MSP 1', NULL, 'http://localhost:8083/TrustSimulator_MSP1', 'Emails', NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('SP', 59, '1234', '2014-10-17 15:15:35', '', 'SP1@TrustSimulator.com', NULL, '2014-10-17 15:15:35', 'SP 1', 'Gold', 'http://localhost:8084/TrustSimulator_SP1', NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1');
INSERT INTO `AbstractEntity` VALUES('SP', 61, '1234', '2014-10-17 15:15:35', '', 'SP2@TrustSimulator.com', NULL, '2014-10-17 15:15:35', 'SP 2', 'Silver', 'http://localhost:8084/TrustSimulator_SP2', NULL, 0, '', '');
INSERT INTO `AbstractEntity` VALUES('SP', 63, '1234', '2014-10-17 15:15:36', '', 'SP3@TrustSimulator.com', NULL, '2014-10-17 15:15:36', 'SP 3', 'Bronze', 'http://localhost:8086/TrustSimulator_SP3', NULL, 0, '', '');
INSERT INTO `AbstractEntity` VALUES('SP', 65, '1234', '2014-10-17 15:15:36', '', 'SP4@TrustSimulator.com', NULL, '2014-10-17 15:15:36', 'SP 4', 'Diamond', 'http://localhost:8086/TrustSimulator_SP4', NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1');
INSERT INTO `AbstractEntity` VALUES('Auditor', 67, '1234', '2014-10-17 15:15:36', '', 'Auditor1@trustSimulator.com', NULL, '2014-10-17 15:15:36', 'Auditor 1', NULL, 'http://localhost:8085/TrustSimulator_Auditor1', NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 69, '1234', '2014-10-17 15:15:37', NULL, 'User1@trustSimulator.com', '0111222333', '2016-09-06 14:09:39', 'User 1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 71, '1234', '2014-10-17 15:15:37', NULL, 'User2@trustSimulator.com', '0333222111', '2014-10-17 15:15:37', 'User 2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 73, '1234', '2014-10-17 15:15:37', NULL, 'User3@trustSimulator.com', '0222333111', '2014-10-17 15:15:37', 'User 3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 75, '1234', '2014-10-17 15:15:37', NULL, 'User4@trustSimulator.com', '', '2014-10-17 15:15:37', 'User 4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 77, '1234', '2014-10-17 15:15:37', NULL, 'User5@trustSimulator.com', '0999888777', '2014-10-17 15:15:37', 'User 5', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 79, '1234', '2014-10-17 15:15:37', NULL, 'User6@trustSimulator.com', '0888999777', '2014-10-17 15:15:37', 'User 6', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 81, '1234', '2014-10-17 15:15:37', NULL, 'User7@trustSimulator.com', '0777999888', '2014-10-17 15:15:37', 'User 7', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 83, '1234', '2014-10-17 15:15:37', NULL, 'User8@trustSimulator.com', '0999777888', '2014-10-17 15:15:37', 'User 8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 85, '1234', '2014-10-17 15:15:37', NULL, 'User9@trustSimulator.com', '0888777999', '2014-10-17 15:15:37', 'User 9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractEntity` VALUES('User', 87, '1234', '2014-10-17 15:15:37', NULL, 'User10@trustSimulator.com', '', '2014-10-17 15:15:37', 'User 10', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'MessageLog', 'save', '2014-10-17 15:23:06', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 100, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(3, 'MessageLog', 'save', '2014-10-17 15:23:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 2, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(4, 'MSPDataLog', 'update', '2014-10-17 15:23:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(5, 'IDPLog', 'UserInfo Release', '2014-10-17 15:23:39', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMyJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI1NjRhZjEwYi1mYWY5LTQ3OTctOTc5Mi1iOWI1NzBhNTZlMmQiLCJpYXQiOjE0MTM1NTU4MTh9.o_nBGhUBDB2DMwdy028ptg7Wa6iEmxcsxWUeRA4_u9cyCUse9Ejzc72z7-r17uWT0189FUBsJvW4ErG4uzXBHXCNmXpFgJA77ojzSMip1zzMFrXtcdj8tIrjjO0pnNE8-jkasWxd0CtjJON2sclTu_iMD8M4DCvFKlHjifJn920', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP3', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(6, 'IDPLog', 'UserInfo Release', '2014-10-17 15:23:43', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMyJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI1NjRhZjEwYi1mYWY5LTQ3OTctOTc5Mi1iOWI1NzBhNTZlMmQiLCJpYXQiOjE0MTM1NTU4MTh9.o_nBGhUBDB2DMwdy028ptg7Wa6iEmxcsxWUeRA4_u9cyCUse9Ejzc72z7-r17uWT0189FUBsJvW4ErG4uzXBHXCNmXpFgJA77ojzSMip1zzMFrXtcdj8tIrjjO0pnNE8-jkasWxd0CtjJON2sclTu_iMD8M4DCvFKlHjifJn920', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP3', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(7, 'SPDataLog', 'save', '2014-10-17 15:23:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 2, 63, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(8, 'SPDataLog', 'save', '2014-10-17 15:23:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 4, 63, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(10, 'MessageLog', 'save', '2014-10-17 15:23:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Bronze</h1><h2> From </h2><h1>SP 3</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 9, 'User1@trustSimulator.com', 'User 1', 'SP3@TrustSimulator.com', 'SP 3', 'unread', 'SP 3 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(11, 'IDPLog', 'UserInfo Release', '2014-10-17 15:38:56', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgNCJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiIyYzRjYTZkNy05NWY1LTQ2YTUtYTA2MS0wZGEwMWI4NjNkOWQiLCJpYXQiOjE0MTM1NTY3MzV9.q75_FwI0l7hfz1eC3WkvS056EtfrkU5i366xm1qCF9DhlrlfuAZAAzdPph9jp-GJqSz2L0hM2Vk5JyyWmx6p9nILEzOLhkIAyHvqn8qU8bCgiBBMuxWMxgCwA07z--P2bwMnwd1bY4sxpJFW9KqUT_gOsFiWXjBVjfXap7XQHSU', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP4', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(12, 'IDPLog', 'UserInfo Release', '2014-10-17 15:38:56', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgNCJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiIyYzRjYTZkNy05NWY1LTQ2YTUtYTA2MS0wZGEwMWI4NjNkOWQiLCJpYXQiOjE0MTM1NTY3MzV9.q75_FwI0l7hfz1eC3WkvS056EtfrkU5i366xm1qCF9DhlrlfuAZAAzdPph9jp-GJqSz2L0hM2Vk5JyyWmx6p9nILEzOLhkIAyHvqn8qU8bCgiBBMuxWMxgCwA07z--P2bwMnwd1bY4sxpJFW9KqUT_gOsFiWXjBVjfXap7XQHSU', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP4', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(13, 'SPDataLog', 'save', '2014-10-17 15:38:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 51, 65, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(14, 'SPDataLog', 'save', '2014-10-17 15:38:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 53, 65, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(15, 'MSPDataLog', 'save', '2014-10-17 15:38:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 57, 65, 'http://localhost:8086/TrustSimulator_SP4', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(17, 'MessageLog', 'save', '2014-10-17 15:39:00', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Diamond</h1><h2> From </h2><h1>SP 4</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 16, 'User1@trustSimulator.com', 'User 1', 'SP4@TrustSimulator.com', 'SP 4', 'unread', 'SP 4 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(19, 'MessageLog', 'save', '2014-10-17 15:39:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 18, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(20, 'MSPDataLog', 'update', '2014-10-17 15:39:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 57, 65, 'http://localhost:8086/TrustSimulator_SP4', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(51, 'UserLog', 'update', '2016-09-06 14:09:39', NULL, 'User1@trustSimulator.com', 69, 'User 1', NULL, '0111222333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(52, 'IDPLog', 'UserInfo Release', '2016-09-06 14:11:34', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(53, 'IDPLog', 'UserInfo Release', '2016-09-06 14:11:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(54, 'SPDataLog', 'save', '2016-09-06 14:12:27', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 101, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(55, 'SPDataLog', 'save', '2016-09-06 14:12:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 103, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(56, 'MSPDataLog', 'save', '2016-09-06 14:12:48', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(58, 'MSPLog', 'save', '2014-10-17 15:15:35', '', 'MSP1@TrustSimulator.com', 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', NULL, 'Emails', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(60, 'SPLog', 'save', '2014-10-17 15:15:35', '', 'SP1@TrustSimulator.com', 59, 'SP 1', 'http://localhost:8084/TrustSimulator_SP1', NULL, NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', 'Gold', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(62, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP2@TrustSimulator.com', 61, 'SP 2', 'http://localhost:8084/TrustSimulator_SP2', NULL, NULL, 0, '', '', 'Silver', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(64, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP3@TrustSimulator.com', 63, 'SP 3', 'http://localhost:8086/TrustSimulator_SP3', NULL, NULL, 0, '', '', 'Bronze', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(66, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP4@TrustSimulator.com', 65, 'SP 4', 'http://localhost:8086/TrustSimulator_SP4', NULL, NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', 'Diamond', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(68, 'AuditorLog', 'save', '2014-10-17 15:15:36', '', 'Auditor1@trustSimulator.com', 67, 'Auditor 1', 'http://localhost:8085/TrustSimulator_Auditor1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(70, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User1@trustSimulator.com', 69, 'User 1', NULL, '0111222333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(72, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User2@trustSimulator.com', 71, 'User 2', NULL, '0333222111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(74, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User3@trustSimulator.com', 73, 'User 3', NULL, '0222333111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(76, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User4@trustSimulator.com', 75, 'User 4', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(78, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User5@trustSimulator.com', 77, 'User 5', NULL, '0999888777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(80, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User6@trustSimulator.com', 79, 'User 6', NULL, '0888999777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(82, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User7@trustSimulator.com', 81, 'User 7', NULL, '0777999888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(84, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User8@trustSimulator.com', 83, 'User 8', NULL, '0999777888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(86, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User9@trustSimulator.com', 85, 'User 9', NULL, '0888777999', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(88, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User10@trustSimulator.com', 87, 'User 10', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(89, 'IDPLog', 'UserInfo Release', '2014-10-17 15:17:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI5NDgyODY2YS0xZjVhLTQxMmMtOTRiMi0yMzIxOThlNmEyMzEiLCJpYXQiOjE0MTM1NTU0Nzd9.l9ta3dijZ9IvMatzX7uyFFI9ro7e7pf_NFMzYmhxl8iZv9nrNXQ_9eFuWxPE_w-OV4YGDOheBSDrh3IPVtLWUOsjY6JiXy71hJCabtrZLhLP4QFJbgvoURTbmbLYOcovIlq4ml9tmmM93fS2uwyICj8T6iC9HtxdaIhbZZ9Nqkc', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(90, 'IDPLog', 'UserInfo Release', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI5NDgyODY2YS0xZjVhLTQxMmMtOTRiMi0yMzIxOThlNmEyMzEiLCJpYXQiOjE0MTM1NTU0Nzd9.l9ta3dijZ9IvMatzX7uyFFI9ro7e7pf_NFMzYmhxl8iZv9nrNXQ_9eFuWxPE_w-OV4YGDOheBSDrh3IPVtLWUOsjY6JiXy71hJCabtrZLhLP4QFJbgvoURTbmbLYOcovIlq4ml9tmmM93fS2uwyICj8T6iC9HtxdaIhbZZ9Nqkc', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(91, 'SPDataLog', 'save', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 2, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(92, 'SPDataLog', 'save', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 4, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(94, 'MessageLog', 'save', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 93, 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(95, 'IDPLog', 'UserInfo Release', '2014-10-17 15:22:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiJkNWFlMDRhMi1mYmIyLTRiZjAtOTc5ZS1hNTk5NmYzMTc4ZjEiLCJpYXQiOjE0MTM1NTU3NjR9.a3Ewq4iok2Y9roFrBiwvgHQ_TR2l3ZcT6Oa_1E2sdy4hdXUJz5NRbMUmdmFG-iVprBVFMoEnlYIUQnV04hBvW4rhE-pbqFpu5e2f8qfy7JFyUTe0Any7qBeClOUZ4YsqYRJe-pb5MUyFpUs9mfhU9OvyVqAoznX7KbKAejmfSC4', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(96, 'IDPLog', 'UserInfo Release', '2014-10-17 15:22:48', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiJkNWFlMDRhMi1mYmIyLTRiZjAtOTc5ZS1hNTk5NmYzMTc4ZjEiLCJpYXQiOjE0MTM1NTU3NjR9.a3Ewq4iok2Y9roFrBiwvgHQ_TR2l3ZcT6Oa_1E2sdy4hdXUJz5NRbMUmdmFG-iVprBVFMoEnlYIUQnV04hBvW4rhE-pbqFpu5e2f8qfy7JFyUTe0Any7qBeClOUZ4YsqYRJe-pb5MUyFpUs9mfhU9OvyVqAoznX7KbKAejmfSC4', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(97, 'SPDataLog', 'save', '2014-10-17 15:22:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 51, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(98, 'SPDataLog', 'save', '2014-10-17 15:22:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 53, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(99, 'MSPDataLog', 'save', '2014-10-17 15:23:03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(101, 'MessageLog', 'save', '2016-09-06 14:14:40', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 99, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(103, 'MessageLog', 'save', '2016-09-06 14:14:49', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 102, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(104, 'MSPDataLog', 'update', '2016-09-06 14:14:54', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(105, 'MSPDataLog', 'update', '2016-09-06 14:14:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(106, 'IDPLog', 'UserInfo Release', '2016-09-06 14:15:08', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc3MDQsImp0aSI6ImY0MzRkNGIyLThhOWYtNDRmNi1hYjQ4LWFmYzFiNTQ1Y2RiNCJ9.EcWal0q3m7Zg7SYARyzd2_UryvQkWYngrSxt3WirukX9q0cSbOiet8sqSO8WRuC1ohi5QWav4LckJS2MB4axEvtIiaOEou8FsUZIwiRnqiJUPUMXkN76s984kkynx_bACXxDhAppO9QsSxbSQo6ECYIbMXEuR6d6BlX9DhKUTlM', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(107, 'IDPLog', 'UserInfo Release', '2016-09-06 14:15:18', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc3MDQsImp0aSI6ImY0MzRkNGIyLThhOWYtNDRmNi1hYjQ4LWFmYzFiNTQ1Y2RiNCJ9.EcWal0q3m7Zg7SYARyzd2_UryvQkWYngrSxt3WirukX9q0cSbOiet8sqSO8WRuC1ohi5QWav4LckJS2MB4axEvtIiaOEou8FsUZIwiRnqiJUPUMXkN76s984kkynx_bACXxDhAppO9QsSxbSQo6ECYIbMXEuR6d6BlX9DhKUTlM', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(108, 'SPDataLog', 'save', '2016-09-06 14:15:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 51, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(109, 'SPDataLog', 'save', '2016-09-06 14:15:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 53, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(111, 'MessageLog', 'save', '2016-09-06 14:15:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 110, 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(112, 'IDPLog', 'UserInfo Release', '2016-09-06 14:16:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(113, 'SPDataLog', 'save', '2016-09-06 14:16:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 105, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(114, 'SPDataLog', 'save', '2016-09-06 14:16:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 107, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(115, 'MSPDataLog', 'save', '2016-09-06 14:16:32', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 105, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(117, 'MessageLog', 'save', '2016-09-06 14:16:33', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 116, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(119, 'MessageLog', 'save', '2016-09-06 14:16:38', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 118, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(120, 'MSPDataLog', 'update', '2016-09-06 14:16:39', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 105, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(121, 'MessageLog', 'update', '2016-09-06 14:19:54', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 118, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(122, 'IDPLog', 'UserInfo Release', '2016-09-06 14:20:20', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(123, 'IDPLog', 'UserInfo Release', '2016-09-06 14:20:22', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(124, 'AuditorCaseLog', 'save', '2016-09-06 14:20:24', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, 51, 118, 69, NULL, 0, 'email', 'User1@trustSimulator.com', 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(125, 'IDPLog', 'UserInfo Release', '2016-09-06 14:20:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(126, 'MessageLog', 'update', '2016-09-06 14:20:57', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 102, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(127, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(128, 'AuditorCaseLog', 'save', '2016-09-06 14:21:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, 53, 102, 69, NULL, 0, 'email', 'User1@trustSimulator.com', 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(129, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:25', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(130, 'MessageLog', 'update', '2016-09-06 14:21:40', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 118, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(131, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(132, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(133, 'MessageLog', 'update', '2016-09-06 14:22:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 118, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(134, 'IDPLog', 'UserInfo Release', '2016-09-06 14:22:06', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(135, 'MessageLog', 'update', '2016-09-06 14:22:35', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 118, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(136, 'IDPLog', 'UserInfo Release', '2016-09-06 14:22:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(201, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzNzAsImp0aSI6ImQ3OGE1OTBmLWVjZjAtNDM2MC1hMjEyLTFlNmM1MWRiNWU4ZCJ9.f3X97Gqir5UK1ixyZJrXpbyAp9MlkKm6MVYI5Hy8Xpx5nYQiH0Uf9sNX0rvYYFRzJHEcsZA5Vr8SLfOzWY6UvbrqAiaFtNvTKPTkJX_HycYCjOj0MyGFDKVNqCE3kA-A04NFP4tuu8pDm-nPE-vlgmcfTD_GDdpUCxLI8s2fDO0', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(202, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzNzAsImp0aSI6ImQ3OGE1OTBmLWVjZjAtNDM2MC1hMjEyLTFlNmM1MWRiNWU4ZCJ9.f3X97Gqir5UK1ixyZJrXpbyAp9MlkKm6MVYI5Hy8Xpx5nYQiH0Uf9sNX0rvYYFRzJHEcsZA5Vr8SLfOzWY6UvbrqAiaFtNvTKPTkJX_HycYCjOj0MyGFDKVNqCE3kA-A04NFP4tuu8pDm-nPE-vlgmcfTD_GDdpUCxLI8s2fDO0', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(203, 'SPDataLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 151, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(204, 'SPDataLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 153, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(205, 'MSPDataLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 201, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(207, 'MessageLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 206, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(209, 'MessageLog', 'save', '2017-05-18 15:26:13', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 208, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(210, 'MSPDataLog', 'update', '2017-05-18 15:26:13', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 201, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(211, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzODEsImp0aSI6IjMzM2E0ZDFkLTQxMjEtNGQxYi1iNWI1LTE3OTQ5ZGQzNDAxYiJ9.WadJQGV9IF6cp_fMfXxAe9dhO_EfA4x5K4KqxoIUYhVgXw3h6uBVog7Nr3xd9WgW7R2huI97bPKiijZfJewnad_RqiTofrHsoOUdpHkFzB7FJTssIhrLYG1JCeWab7ShL297HZ8vcbzPGdTUumw5HZbNnRqrdptH85DiF-uUzIA', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(212, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzODEsImp0aSI6IjMzM2E0ZDFkLTQxMjEtNGQxYi1iNWI1LTE3OTQ5ZGQzNDAxYiJ9.WadJQGV9IF6cp_fMfXxAe9dhO_EfA4x5K4KqxoIUYhVgXw3h6uBVog7Nr3xd9WgW7R2huI97bPKiijZfJewnad_RqiTofrHsoOUdpHkFzB7FJTssIhrLYG1JCeWab7ShL297HZ8vcbzPGdTUumw5HZbNnRqrdptH85DiF-uUzIA', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(213, 'SPDataLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 156, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(214, 'SPDataLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 158, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(215, 'MSPDataLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 203, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(217, 'MessageLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 216, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(219, 'MessageLog', 'save', '2017-05-18 15:26:23', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 218, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(220, 'MSPDataLog', 'update', '2017-05-18 15:26:23', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 203, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(2, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2014-10-17 15:23:10', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(9, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Bronze</h1><h2> From </h2><h1>SP 3</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:23:44', 'User1@trustSimulator.com', 'User 1', 'SP3@TrustSimulator.com', 'SP 3', 'unread', 'SP 3 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(16, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Diamond</h1><h2> From </h2><h1>SP 4</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:39:00', 'User1@trustSimulator.com', 'User 1', 'SP4@TrustSimulator.com', 'SP 4', 'unread', 'SP 4 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(18, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2014-10-17 15:39:02', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(93, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:18:01', 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(99, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:14:39', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(100, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:23:06', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(102, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:14:49', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(110, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2016-09-06 14:15:45', 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(116, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2016-09-06 14:16:33', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(118, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:16:38', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(206, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2017-05-18 15:26:11', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(208, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2017-05-18 15:26:13', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(216, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2017-05-18 15:26:21', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(218, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2017-05-18 15:26:23', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 250);

-- --------------------------------------------------------

--
-- Table structure for table `pairwise_identifier`
--

CREATE TABLE `pairwise_identifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(256) DEFAULT NULL,
  `sub` varchar(256) DEFAULT NULL,
  `sector_identifier` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pairwise_identifier`
--

--
-- Database: `TrustSimulator_Auditor1`
--
CREATE DATABASE `TrustSimulator_Auditor1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_Auditor1`;

-- --------------------------------------------------------

--
-- Table structure for table `AUDITORCASE`
--

CREATE TABLE `AUDITORCASE` (
  `AUDITORCASEID` int(11) NOT NULL,
  `Comment` mediumtext,
  `CreatedOn` datetime DEFAULT NULL,
  `InvestigatedDataType` varchar(32) DEFAULT NULL,
  `InvestigatedDataValue` varchar(255) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `Status` varchar(255) NOT NULL,
  `AuditorID` int(11) NOT NULL,
  `MessageID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ConvictedSPID` int(11) DEFAULT NULL,
  `UserName` varchar(32) NOT NULL,
  PRIMARY KEY (`AUDITORCASEID`),
  KEY `FK_AUDITORCASE_MessageID` (`MessageID`),
  KEY `FK_AUDITORCASE_UserID` (`UserID`),
  KEY `FK_AUDITORCASE_ConvictedSPID` (`ConvictedSPID`),
  KEY `FK_AUDITORCASE_AuditorID` (`AuditorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AUDITORCASE`
--

INSERT INTO `AUDITORCASE` VALUES(51, NULL, '2016-09-06 14:20:24', 'email', 'User1@trustSimulator.com', '2016-09-06 14:20:24', 'Open', 67, 118, 69, 0, 'User 1');
INSERT INTO `AUDITORCASE` VALUES(53, NULL, '2016-09-06 14:21:02', 'email', 'User1@trustSimulator.com', '2016-09-06 14:21:02', 'Open', 67, 102, 69, 0, 'User 1');

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('Auditor', 67, '1234', '2014-10-17 15:15:36', '', 'Auditor1@trustSimulator.com', NULL, '2014-10-17 15:15:36', 'Auditor 1', NULL, 'http://localhost:8085/TrustSimulator_Auditor1', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'AuditorLog', 'save', '2014-10-17 15:15:37', '', 'Auditor1@trustSimulator.com', 67, 'Auditor 1', 'http://localhost:8085/TrustSimulator_Auditor1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(52, 'AuditorCaseLog', 'save', '2016-09-06 14:20:24', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, 51, 118, 69, NULL, 0, 'email', 'User1@trustSimulator.com', 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(54, 'AuditorCaseLog', 'save', '2016-09-06 14:21:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 67, 53, 102, 69, NULL, 0, 'email', 'User1@trustSimulator.com', 'Open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(102, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:14:49', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(118, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:16:38', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'read', 'MSP 1 - Product Offer!!');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 100);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `AUDITORCASE`
--
ALTER TABLE `AUDITORCASE`
  ADD CONSTRAINT `AUDITORCASE_ibfk_1` FOREIGN KEY (`AuditorID`) REFERENCES `AbstractEntity` (`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `AUDITORCASE_ibfk_2` FOREIGN KEY (`MessageID`) REFERENCES `MESSAGE` (`MESSAGEID`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_AUDITORCASE_AuditorID` FOREIGN KEY (`AuditorID`) REFERENCES `AbstractEntity` (`ID`),
  ADD CONSTRAINT `FK_AUDITORCASE_MessageID` FOREIGN KEY (`MessageID`) REFERENCES `MESSAGE` (`MESSAGEID`);
--
-- Database: `TrustSimulator_Auditor2`
--
CREATE DATABASE `TrustSimulator_Auditor2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_Auditor2`;

-- --------------------------------------------------------

--
-- Table structure for table `AUDITORCASE`
--

CREATE TABLE `AUDITORCASE` (
  `AUDITORCASEID` int(11) NOT NULL,
  `Comment` mediumtext,
  `CreatedOn` datetime DEFAULT NULL,
  `InvestigatedDataType` varchar(32) DEFAULT NULL,
  `InvestigatedDataValue` varchar(255) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `Status` varchar(255) NOT NULL,
  `AuditorID` int(11) NOT NULL,
  `MessageID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ConvictedSPID` int(11) DEFAULT NULL,
  `UserName` varchar(32) NOT NULL,
  PRIMARY KEY (`AUDITORCASEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AUDITORCASE`
--


-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `AbstractEntity`
--


-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--


-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--


-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

--
-- Database: `TrustSimulator_IDP`
--
CREATE DATABASE `TrustSimulator_IDP` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_IDP`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(6, 'SPLog', 'save', '2014-10-17 15:15:35', '', 'SP1@TrustSimulator.com', 59, 'SP 1', 'http://localhost:8084/TrustSimulator_SP1', NULL, NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', 'Gold', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(7, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP2@TrustSimulator.com', 61, 'SP 2', 'http://localhost:8084/TrustSimulator_SP2', NULL, NULL, 0, '', '', 'Silver', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(8, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP3@TrustSimulator.com', 63, 'SP 3', 'http://localhost:8086/TrustSimulator_SP3', NULL, NULL, 0, '', '', 'Bronze', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(9, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP4@TrustSimulator.com', 65, 'SP 4', 'http://localhost:8086/TrustSimulator_SP4', NULL, NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', 'Diamond', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(10, 'AuditorLog', 'save', '2014-10-17 15:15:36', '', 'Auditor1@trustSimulator.com', 67, 'Auditor 1', 'http://localhost:8085/TrustSimulator_Auditor1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(11, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User1@trustSimulator.com', 69, 'User 1', NULL, '0111222333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(12, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User2@trustSimulator.com', 71, 'User 2', NULL, '0333222111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(13, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User3@trustSimulator.com', 73, 'User 3', NULL, '0222333111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(14, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User4@trustSimulator.com', 75, 'User 4', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(15, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User5@trustSimulator.com', 77, 'User 5', NULL, '0999888777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(16, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User6@trustSimulator.com', 79, 'User 6', NULL, '0888999777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(17, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User7@trustSimulator.com', 81, 'User 7', NULL, '0777999888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(18, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User8@trustSimulator.com', 83, 'User 8', NULL, '0999777888', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(19, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User9@trustSimulator.com', 85, 'User 9', NULL, '0888777999', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(20, 'UserLog', 'save', '2014-10-17 15:15:37', NULL, 'User10@trustSimulator.com', 87, 'User 10', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(21, 'IDPLog', 'UserInfo Release', '2014-10-17 15:17:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI5NDgyODY2YS0xZjVhLTQxMmMtOTRiMi0yMzIxOThlNmEyMzEiLCJpYXQiOjE0MTM1NTU0Nzd9.l9ta3dijZ9IvMatzX7uyFFI9ro7e7pf_NFMzYmhxl8iZv9nrNXQ_9eFuWxPE_w-OV4YGDOheBSDrh3IPVtLWUOsjY6JiXy71hJCabtrZLhLP4QFJbgvoURTbmbLYOcovIlq4ml9tmmM93fS2uwyICj8T6iC9HtxdaIhbZZ9Nqkc', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(22, 'IDPLog', 'UserInfo Release', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI5NDgyODY2YS0xZjVhLTQxMmMtOTRiMi0yMzIxOThlNmEyMzEiLCJpYXQiOjE0MTM1NTU0Nzd9.l9ta3dijZ9IvMatzX7uyFFI9ro7e7pf_NFMzYmhxl8iZv9nrNXQ_9eFuWxPE_w-OV4YGDOheBSDrh3IPVtLWUOsjY6JiXy71hJCabtrZLhLP4QFJbgvoURTbmbLYOcovIlq4ml9tmmM93fS2uwyICj8T6iC9HtxdaIhbZZ9Nqkc', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(23, 'IDPLog', 'UserInfo Release', '2014-10-17 15:22:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiJkNWFlMDRhMi1mYmIyLTRiZjAtOTc5ZS1hNTk5NmYzMTc4ZjEiLCJpYXQiOjE0MTM1NTU3NjR9.a3Ewq4iok2Y9roFrBiwvgHQ_TR2l3ZcT6Oa_1E2sdy4hdXUJz5NRbMUmdmFG-iVprBVFMoEnlYIUQnV04hBvW4rhE-pbqFpu5e2f8qfy7JFyUTe0Any7qBeClOUZ4YsqYRJe-pb5MUyFpUs9mfhU9OvyVqAoznX7KbKAejmfSC4', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(24, 'IDPLog', 'UserInfo Release', '2014-10-17 15:22:48', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiJkNWFlMDRhMi1mYmIyLTRiZjAtOTc5ZS1hNTk5NmYzMTc4ZjEiLCJpYXQiOjE0MTM1NTU3NjR9.a3Ewq4iok2Y9roFrBiwvgHQ_TR2l3ZcT6Oa_1E2sdy4hdXUJz5NRbMUmdmFG-iVprBVFMoEnlYIUQnV04hBvW4rhE-pbqFpu5e2f8qfy7JFyUTe0Any7qBeClOUZ4YsqYRJe-pb5MUyFpUs9mfhU9OvyVqAoznX7KbKAejmfSC4', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(25, 'IDPLog', 'UserInfo Release', '2014-10-17 15:23:39', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMyJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI1NjRhZjEwYi1mYWY5LTQ3OTctOTc5Mi1iOWI1NzBhNTZlMmQiLCJpYXQiOjE0MTM1NTU4MTh9.o_nBGhUBDB2DMwdy028ptg7Wa6iEmxcsxWUeRA4_u9cyCUse9Ejzc72z7-r17uWT0189FUBsJvW4ErG4uzXBHXCNmXpFgJA77ojzSMip1zzMFrXtcdj8tIrjjO0pnNE8-jkasWxd0CtjJON2sclTu_iMD8M4DCvFKlHjifJn920', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP3', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(26, 'IDPLog', 'UserInfo Release', '2014-10-17 15:23:43', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMyJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI1NjRhZjEwYi1mYWY5LTQ3OTctOTc5Mi1iOWI1NzBhNTZlMmQiLCJpYXQiOjE0MTM1NTU4MTh9.o_nBGhUBDB2DMwdy028ptg7Wa6iEmxcsxWUeRA4_u9cyCUse9Ejzc72z7-r17uWT0189FUBsJvW4ErG4uzXBHXCNmXpFgJA77ojzSMip1zzMFrXtcdj8tIrjjO0pnNE8-jkasWxd0CtjJON2sclTu_iMD8M4DCvFKlHjifJn920', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP3', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(27, 'IDPLog', 'UserInfo Release', '2014-10-17 15:38:56', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgNCJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiIyYzRjYTZkNy05NWY1LTQ2YTUtYTA2MS0wZGEwMWI4NjNkOWQiLCJpYXQiOjE0MTM1NTY3MzV9.q75_FwI0l7hfz1eC3WkvS056EtfrkU5i366xm1qCF9DhlrlfuAZAAzdPph9jp-GJqSz2L0hM2Vk5JyyWmx6p9nILEzOLhkIAyHvqn8qU8bCgiBBMuxWMxgCwA07z--P2bwMnwd1bY4sxpJFW9KqUT_gOsFiWXjBVjfXap7XQHSU', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP4', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(28, 'IDPLog', 'UserInfo Release', '2014-10-17 15:38:56', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgNCJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiIyYzRjYTZkNy05NWY1LTQ2YTUtYTA2MS0wZGEwMWI4NjNkOWQiLCJpYXQiOjE0MTM1NTY3MzV9.q75_FwI0l7hfz1eC3WkvS056EtfrkU5i366xm1qCF9DhlrlfuAZAAzdPph9jp-GJqSz2L0hM2Vk5JyyWmx6p9nILEzOLhkIAyHvqn8qU8bCgiBBMuxWMxgCwA07z--P2bwMnwd1bY4sxpJFW9KqUT_gOsFiWXjBVjfXap7XQHSU', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8086/TrustSimulator_SP4', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(51, 'IDPLog', 'UserInfo Release', '2016-09-06 14:11:34', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(52, 'IDPLog', 'UserInfo Release', '2016-09-06 14:11:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(53, 'IDPLog', 'UserInfo Release', '2016-09-06 14:15:08', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc3MDQsImp0aSI6ImY0MzRkNGIyLThhOWYtNDRmNi1hYjQ4LWFmYzFiNTQ1Y2RiNCJ9.EcWal0q3m7Zg7SYARyzd2_UryvQkWYngrSxt3WirukX9q0cSbOiet8sqSO8WRuC1ohi5QWav4LckJS2MB4axEvtIiaOEou8FsUZIwiRnqiJUPUMXkN76s984kkynx_bACXxDhAppO9QsSxbSQo6ECYIbMXEuR6d6BlX9DhKUTlM', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(54, 'IDPLog', 'UserInfo Release', '2016-09-06 14:15:18', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc3MDQsImp0aSI6ImY0MzRkNGIyLThhOWYtNDRmNi1hYjQ4LWFmYzFiNTQ1Y2RiNCJ9.EcWal0q3m7Zg7SYARyzd2_UryvQkWYngrSxt3WirukX9q0cSbOiet8sqSO8WRuC1ohi5QWav4LckJS2MB4axEvtIiaOEou8FsUZIwiRnqiJUPUMXkN76s984kkynx_bACXxDhAppO9QsSxbSQo6ECYIbMXEuR6d6BlX9DhKUTlM', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP2', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(55, 'IDPLog', 'UserInfo Release', '2016-09-06 14:16:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(56, 'IDPLog', 'UserInfo Release', '2016-09-06 14:20:20', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(57, 'IDPLog', 'UserInfo Release', '2016-09-06 14:20:22', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(58, 'IDPLog', 'UserInfo Release', '2016-09-06 14:20:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(59, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(60, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:25', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[openid, TrustSimulator_Scope]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(61, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(62, 'IDPLog', 'UserInfo Release', '2016-09-06 14:21:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(63, 'IDPLog', 'UserInfo Release', '2016-09-06 14:22:06', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(64, 'IDPLog', 'UserInfo Release', '2016-09-06 14:22:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8085/TrustSimulator_Auditor1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(101, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzNzAsImp0aSI6ImQ3OGE1OTBmLWVjZjAtNDM2MC1hMjEyLTFlNmM1MWRiNWU4ZCJ9.f3X97Gqir5UK1ixyZJrXpbyAp9MlkKm6MVYI5Hy8Xpx5nYQiH0Uf9sNX0rvYYFRzJHEcsZA5Vr8SLfOzWY6UvbrqAiaFtNvTKPTkJX_HycYCjOj0MyGFDKVNqCE3kA-A04NFP4tuu8pDm-nPE-vlgmcfTD_GDdpUCxLI8s2fDO0', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(102, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzNzAsImp0aSI6ImQ3OGE1OTBmLWVjZjAtNDM2MC1hMjEyLTFlNmM1MWRiNWU4ZCJ9.f3X97Gqir5UK1ixyZJrXpbyAp9MlkKm6MVYI5Hy8Xpx5nYQiH0Uf9sNX0rvYYFRzJHEcsZA5Vr8SLfOzWY6UvbrqAiaFtNvTKPTkJX_HycYCjOj0MyGFDKVNqCE3kA-A04NFP4tuu8pDm-nPE-vlgmcfTD_GDdpUCxLI8s2fDO0', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(103, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzODEsImp0aSI6IjMzM2E0ZDFkLTQxMjEtNGQxYi1iNWI1LTE3OTQ5ZGQzNDAxYiJ9.WadJQGV9IF6cp_fMfXxAe9dhO_EfA4x5K4KqxoIUYhVgXw3h6uBVog7Nr3xd9WgW7R2huI97bPKiijZfJewnad_RqiTofrHsoOUdpHkFzB7FJTssIhrLYG1JCeWab7ShL297HZ8vcbzPGdTUumw5HZbNnRqrdptH85DiF-uUzIA', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');
INSERT INTO `AbstractLog` VALUES(104, 'IDPLog', 'UserInfo Release', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[TrustSimulator_Scope, openid]', 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzODEsImp0aSI6IjMzM2E0ZDFkLTQxMjEtNGQxYi1iNWI1LTE3OTQ5ZGQzNDAxYiJ9.WadJQGV9IF6cp_fMfXxAe9dhO_EfA4x5K4KqxoIUYhVgXw3h6uBVog7Nr3xd9WgW7R2huI97bPKiijZfJewnad_RqiTofrHsoOUdpHkFzB7FJTssIhrLYG1JCeWab7ShL297HZ8vcbzPGdTUumw5HZbNnRqrdptH85DiF-uUzIA', 'http://localhost:8070/TrustSimulator_IDP', 'http://localhost:8084/TrustSimulator_SP1', 'User.69.User1');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 150);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `access_token`
--

INSERT INTO `access_token` VALUES(2, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI5NDgyODY2YS0xZjVhLTQxMmMtOTRiMi0yMzIxOThlNmEyMzEiLCJpYXQiOjE0MTM1NTU0Nzd9.l9ta3dijZ9IvMatzX7uyFFI9ro7e7pf_NFMzYmhxl8iZv9nrNXQ_9eFuWxPE_w-OV4YGDOheBSDrh3IPVtLWUOsjY6JiXy71hJCabtrZLhLP4QFJbgvoURTbmbLYOcovIlq4ml9tmmM93fS2uwyICj8T6iC9HtxdaIhbZZ9Nqkc', NULL, 'Bearer', NULL, '2', 1, 1, NULL);
INSERT INTO `access_token` VALUES(9, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0MzEsImp0aSI6IjM0NTRhMTIyLWUxNjktNDlhYi1hN2E4LTI1YjYzZGVhODMzMCJ9.JsJIMSw_RVgajnVMXiqhbSV4XRDaMVXcPrBDVFOks19yVS5Rw1nAyGJj4EvLqcw_-LCwhYFSdtpZul1F-aEKXDlK4D9ke7T39LXfoLObN5zAoQv_GK-vs2vuw9EG3uhvjJi5x_f4y4ETBwllbvS4h6XXijEeK_mDz4ms6fpX-0Q', NULL, 'Bearer', NULL, '1', 5, NULL, 2);
INSERT INTO `access_token` VALUES(4, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiJkNWFlMDRhMi1mYmIyLTRiZjAtOTc5ZS1hNTk5NmYzMTc4ZjEiLCJpYXQiOjE0MTM1NTU3NjR9.a3Ewq4iok2Y9roFrBiwvgHQ_TR2l3ZcT6Oa_1E2sdy4hdXUJz5NRbMUmdmFG-iVprBVFMoEnlYIUQnV04hBvW4rhE-pbqFpu5e2f8qfy7JFyUTe0Any7qBeClOUZ4YsqYRJe-pb5MUyFpUs9mfhU9OvyVqAoznX7KbKAejmfSC4', NULL, 'Bearer', NULL, '1', 2, 3, NULL);
INSERT INTO `access_token` VALUES(8, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgNCJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiIyYzRjYTZkNy05NWY1LTQ2YTUtYTA2MS0wZGEwMWI4NjNkOWQiLCJpYXQiOjE0MTM1NTY3MzV9.q75_FwI0l7hfz1eC3WkvS056EtfrkU5i366xm1qCF9DhlrlfuAZAAzdPph9jp-GJqSz2L0hM2Vk5JyyWmx6p9nILEzOLhkIAyHvqn8qU8bCgiBBMuxWMxgCwA07z--P2bwMnwd1bY4sxpJFW9KqUT_gOsFiWXjBVjfXap7XQHSU', NULL, 'Bearer', NULL, '4', 4, 7, 1);
INSERT INTO `access_token` VALUES(6, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMyJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJqdGkiOiI1NjRhZjEwYi1mYWY5LTQ3OTctOTc5Mi1iOWI1NzBhNTZlMmQiLCJpYXQiOjE0MTM1NTU4MTh9.o_nBGhUBDB2DMwdy028ptg7Wa6iEmxcsxWUeRA4_u9cyCUse9Ejzc72z7-r17uWT0189FUBsJvW4ErG4uzXBHXCNmXpFgJA77ojzSMip1zzMFrXtcdj8tIrjjO0pnNE8-jkasWxd0CtjJON2sclTu_iMD8M4DCvFKlHjifJn920', NULL, 'Bearer', NULL, '3', 3, 5, NULL);
INSERT INTO `access_token` VALUES(11, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc0ODAsImp0aSI6ImY5NDUzZmEzLWUwMzktNDEyNC1hYzZjLTI2YTZiOGMxNzAwNyJ9.w7OEPEBRlCKPDRG4Px8h4Pc01MEo2-kwPPOoh-2FUBBW2jW54MUlaHlrbLCwKm9xqxcTyk1LGULMbmXSdbvsOhgUhBp54ouYbGIHJw-7wttW9pKhhFVmaLX30BBCRiiSYmyFkjSVtI413u9PjVhWt_zPtIYcuGEtRgkYber4Kso', NULL, 'Bearer', NULL, '1', 6, 10, 3);
INSERT INTO `access_token` VALUES(13, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMiJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0NzMxNjc3MDQsImp0aSI6ImY0MzRkNGIyLThhOWYtNDRmNi1hYjQ4LWFmYzFiNTQ1Y2RiNCJ9.EcWal0q3m7Zg7SYARyzd2_UryvQkWYngrSxt3WirukX9q0cSbOiet8sqSO8WRuC1ohi5QWav4LckJS2MB4axEvtIiaOEou8FsUZIwiRnqiJUPUMXkN76s984kkynx_bACXxDhAppO9QsSxbSQo6ECYIbMXEuR6d6BlX9DhKUTlM', NULL, 'Bearer', NULL, '2', 7, 12, 4);
INSERT INTO `access_token` VALUES(19, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzNzAsImp0aSI6ImQ3OGE1OTBmLWVjZjAtNDM2MC1hMjEyLTFlNmM1MWRiNWU4ZCJ9.f3X97Gqir5UK1ixyZJrXpbyAp9MlkKm6MVYI5Hy8Xpx5nYQiH0Uf9sNX0rvYYFRzJHEcsZA5Vr8SLfOzWY6UvbrqAiaFtNvTKPTkJX_HycYCjOj0MyGFDKVNqCE3kA-A04NFP4tuu8pDm-nPE-vlgmcfTD_GDdpUCxLI8s2fDO0', NULL, 'Bearer', NULL, '1', 10, 18, 3);
INSERT INTO `access_token` VALUES(15, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODAxOSwianRpIjoiNjJmOThlMTctMmY5Mi00ZjI2LWI3ODAtNGY4ODJmYzliNjZhIn0.q1TCyQBrxcb6AB0v7Myk9FXnDzUaOkOAuwggIHPWNFn5ZZ33HLvSbJzHmZQULNRmnYkQT_oDH9sWQWO_tY1nlvz3WitE23jVvw0q2vR1c0qB15NyF9tIxFRgKAMPEIW6ACGiabLIVOP1Sw9r105vZycfHPZrjHwmTsOLn3VkSBo', NULL, 'Bearer', NULL, '5', 8, 14, 5);
INSERT INTO `access_token` VALUES(17, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiQXVkaXRvciAxIl0sImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDcwXC9UcnVzdFNpbXVsYXRvcl9JRFBcLyIsImlhdCI6MTQ3MzE2ODEwNCwianRpIjoiNTkxNDFkODQtYTZlZC00YWMwLTg0MTQtZTM3OTA5Y2I4YmM3In0.ZrPEVaWUI2QH4F4FfIUBC3zGghjWiSjEXFhpBb59x17ceBXLG8SzypmYfHSlvCGeulS2bxbpYIKd3drlsAjWwfU_TmR9K5b9g4MM7cNhyZTZ57Ur0hrsuh0j5GZT1vbmHljAViDoj_mzK-m9V5OmIp1xIz-7WqnL5NIhJYYPMWg', NULL, 'Bearer', NULL, '5', 9, 16, 5);
INSERT INTO `access_token` VALUES(21, 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiU1AgMSJdLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA3MFwvVHJ1c3RTaW11bGF0b3JfSURQXC8iLCJpYXQiOjE0OTUxMTAzODEsImp0aSI6IjMzM2E0ZDFkLTQxMjEtNGQxYi1iNWI1LTE3OTQ5ZGQzNDAxYiJ9.WadJQGV9IF6cp_fMfXxAe9dhO_EfA4x5K4KqxoIUYhVgXw3h6uBVog7Nr3xd9WgW7R2huI97bPKiijZfJewnad_RqiTofrHsoOUdpHkFzB7FJTssIhrLYG1JCeWab7ShL297HZ8vcbzPGdTUumw5HZbNnRqrdptH85DiF-uUzIA', NULL, 'Bearer', NULL, '1', 11, 20, 3);

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `formatted` varchar(256) DEFAULT NULL,
  `street_address` varchar(256) DEFAULT NULL,
  `locality` varchar(256) DEFAULT NULL,
  `region` varchar(256) DEFAULT NULL,
  `postal_code` varchar(256) DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `address`
--


-- --------------------------------------------------------

--
-- Table structure for table `approved_site`
--

CREATE TABLE `approved_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(4096) DEFAULT NULL,
  `client_id` varchar(4096) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT NULL,
  `access_date` timestamp NULL DEFAULT NULL,
  `timeout_date` timestamp NULL DEFAULT NULL,
  `whitelisted_site_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `approved_site`
--

INSERT INTO `approved_site` VALUES(1, 'User 1', 'SP 4', '2014-10-17 17:38:55', '2014-10-17 17:38:55', NULL, NULL);
INSERT INTO `approved_site` VALUES(2, 'User 1', 'SP 1', '2016-09-06 16:10:30', '2016-09-06 16:10:30', NULL, NULL);
INSERT INTO `approved_site` VALUES(3, 'User 1', 'SP 1', '2016-09-06 16:11:19', '2017-05-18 15:26:21', NULL, NULL);
INSERT INTO `approved_site` VALUES(4, 'User 1', 'SP 2', '2016-09-06 16:15:03', '2016-09-06 16:15:03', NULL, NULL);
INSERT INTO `approved_site` VALUES(5, 'User 1', 'Auditor 1', '2016-09-06 16:20:19', '2016-09-06 16:21:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `approved_site_scope`
--

CREATE TABLE `approved_site_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `approved_site_scope`
--

INSERT INTO `approved_site_scope` VALUES(1, 'TrustSimulator_Scope');
INSERT INTO `approved_site_scope` VALUES(1, 'openid');
INSERT INTO `approved_site_scope` VALUES(2, 'TrustSimulator_Scope');
INSERT INTO `approved_site_scope` VALUES(3, 'openid');
INSERT INTO `approved_site_scope` VALUES(3, 'TrustSimulator_Scope');
INSERT INTO `approved_site_scope` VALUES(4, 'openid');
INSERT INTO `approved_site_scope` VALUES(4, 'TrustSimulator_Scope');
INSERT INTO `approved_site_scope` VALUES(5, 'openid');
INSERT INTO `approved_site_scope` VALUES(5, 'TrustSimulator_Scope');

-- --------------------------------------------------------

--
-- Table structure for table `authentication_holder`
--

CREATE TABLE `authentication_holder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) DEFAULT NULL,
  `authentication` longblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `authentication_holder`
--

INSERT INTO `authentication_holder` VALUES(1, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502032737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000874000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f64657400056e6f6e636574000d3266653266353361636335633474000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350322f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d31353831353561323637313433740004636f64657400063746444b6248740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000037400046373726674002461663634636366612d613266642d346437372d383562652d6631623166663262623765367400056e6f6e636571007e002474000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b59741903000078707708000001491e78c41b787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350322f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0020787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0040737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e7870740011303a303a303a303a303a303a303a312530740020324333423331464131394633433546453741363844323931343031463532453970737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(2, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000874000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f64657400056e6f6e636574000d3338646434373166646439336674000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d33633730363532633930303035740004636f64657400064a6c34574b75740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000037400046373726674002436373264336666382d666132392d343264322d623832342d6631633338393465393438317400056e6f6e636571007e002474000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b59741903000078707708000001491e78c41b787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0020787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0040737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e7870740011303a303a303a303a303a303a303a312530740020324333423331464131394633433546453741363844323931343031463532453970737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(3, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502033737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000874000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f64657400056e6f6e636574000c61653764626537646539316674000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038362f547275737453696d756c61746f725f5350332f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000c643266383734323134313635740004636f646574000667456d4e3749740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000037400046373726674002464363761366266612d386666662d346636302d616536302d6138353262393165393338647400056e6f6e636571007e002474000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b59741903000078707708000001491e78c41b787874003d687474703a2f2f6c6f63616c686f73743a383038362f547275737453696d756c61746f725f5350332f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0020787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0040737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e7870740011303a303a303a303a303a303a303a312530740020324333423331464131394633433546453741363844323931343031463532453970737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(4, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502034737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000874000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f64657400056e6f6e636574000d3364616630643066623965663974000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038362f547275737453696d756c61746f725f5350342f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d33306637633961373961626537740004636f646574000632764a6f4e72740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002434393330306532382d376565302d346635642d383163302d3466373631363332616462637400056e6f6e636571007e002474000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b59741903000078707708000001491e78c41b7874000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b020000787000000000000000017874003d687474703a2f2f6c6f63616c686f73743a383038362f547275737453696d756c61746f725f5350342f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0020787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e7870740011303a303a303a303a303a303a303a312530740020324333423331464131394633433546453741363844323931343031463532453970737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(5, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f64657400067a684b44333274000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d333535663735346137653262647400056e6f6e636574000d31643337363961633664383135740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000001740014547275737453696d756c61746f725f53636f706578017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002461383539343436362d633662382d343461332d623737322d3431383132643439326633627400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000274000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000156ff9ea4aa787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0043737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e787074000f303a303a303a303a303a303a303a31740020464544463934464237424146463532353336313239413342344345444644304170737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(6, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f6465740006687a5834344274000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d333933323764366436356337367400056e6f6e636574000d33623666666562666566663534740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f400000000000027400066f70656e6964740014547275737453696d756c61746f725f53636f706578017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002435323361303966322d333963382d343036352d396261632d3532363963616439393161657400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000374000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000156ff9ea4aa787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e787074000f303a303a303a303a303a303a303a31740020464544463934464237424146463532353336313239413342344345444644304170737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(7, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502032737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f6465740006467a4777466574000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350322f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d313761393436636264373433647400056e6f6e636574000d31653631613533363031333337740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f400000000000027400066f70656e6964740014547275737453696d756c61746f725f53636f706578017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002431643131616662322d333336612d343865622d386662392d3130653461646164396661337400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000474000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000156ff9ea4aa787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350322f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e787074000f303a303a303a303a303a303a303a31740020464544463934464237424146463532353336313239413342344345444644304170737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(8, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000941756469746f722031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f64657400066c357272507874000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f757269740042687474703a2f2f6c6f63616c686f73743a383038352f547275737453696d756c61746f725f41756469746f72312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d333432353138623738326235317400056e6f6e636574000c313438373236313631346438740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f400000000000027400066f70656e6964740014547275737453696d756c61746f725f53636f706578017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002437343465626139392d366462632d343065342d613435662d3632386262386262653434617400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000574000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000156ffa803987878740042687474703a2f2f6c6f63616c686f73743a383038352f547275737453696d756c61746f725f41756469746f72312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e78707400093132372e302e302e31740020334534364643383734463046303233453538373437334432353730454341384570737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(9, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000941756469746f722031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f646574000673543471347174000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f757269740042687474703a2f2f6c6f63616c686f73743a383038352f547275737453696d756c61746f725f41756469746f72312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000c6534613737376334376232327400056e6f6e636574000d32363732653433383334303938740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002434613263623662622d363838612d343135622d613739612d3434633165386537353932637400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000574000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b5974190300007870770800000156ffa803987878740042687474703a2f2f6c6f63616c686f73743a383038352f547275737453696d756c61746f725f41756469746f72312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e78707400093132372e302e302e31740020334534364643383734463046303233453538373437334432353730454341384570737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(10, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f64657400066d4e466f7a6674000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d326331323337363034393234397400056e6f6e636574000c343633393730303335656635740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002431653564363564332d366431312d346664302d386464352d6336623365353335356332397400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000374000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000015c1b85c435787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e787074000f303a303a303a303a303a303a303a31740020354134323442433830353642363635363437353039334345383931423530434470737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);
INSERT INTO `authentication_holder` VALUES(11, NULL, 0xaced0005737200416f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f417574683241757468656e7469636174696f6ebd400b02166252130200024c000d73746f7265645265717565737474003c4c6f72672f737072696e676672616d65776f726b2f73656375726974792f6f61757468322f70726f76696465722f4f4175746832526571756573743b4c00127573657241757468656e7469636174696f6e7400324c6f72672f737072696e676672616d65776f726b2f73656375726974792f636f72652f41757468656e7469636174696f6e3b787200476f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e546f6b656ed3aa287e6e47640e0200035a000d61757468656e746963617465644c000b617574686f7269746965737400164c6a6176612f7574696c2f436f6c6c656374696f6e3b4c000764657461696c737400124c6a6176612f6c616e672f4f626a6563743b787000737200266a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654c697374fc0f2531b5ec8e100200014c00046c6973747400104c6a6176612f7574696c2f4c6973743b7872002c6a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65436f6c6c656374696f6e19420080cb5ef71e0200014c00016371007e00047870737200136a6176612e7574696c2e41727261794c6973747881d21d99c7619d03000149000473697a65787000000002770400000002737200426f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e617574686f726974792e53696d706c654772616e746564417574686f7269747900000000000001400200014c0004726f6c657400124c6a6176612f6c616e672f537472696e673b787074000d524f4c455f454e445f555345527371007e000d740009524f4c455f555345527871007e000c707372003a6f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e4f41757468325265717565737400000000000000010200065a0008617070726f7665644c000b617574686f72697469657371007e00044c000a657874656e73696f6e7374000f4c6a6176612f7574696c2f4d61703b4c000b726564697265637455726971007e000e4c000b7265736f7572636549647374000f4c6a6176612f7574696c2f5365743b4c000d726573706f6e7365547970657371007e0015787200386f72672e737072696e676672616d65776f726b2e73656375726974792e6f61757468322e70726f76696465722e426173655265717565737436287a3ea37169bd0200034c0008636c69656e74496471007e000e4c001172657175657374506172616d657465727371007e00144c000573636f706571007e0015787074000453502031737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00147870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c77080000001000000008740004636f646574000639517551483774000a6772616e745f74797065740012617574686f72697a6174696f6e5f636f646574000573636f706574001b6f70656e696420547275737453696d756c61746f725f53636f706574000d726573706f6e73655f74797065740004636f646574000c72656469726563745f75726974003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e740005737461746574000d316666666561643763383038637400056e6f6e636574000c323037643733333965316434740009636c69656e745f696471007e001878737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c65536574801d92d18f9b80550200007871007e0009737200176a6176612e7574696c2e4c696e6b656448617368536574d86cd75a95dd2a1e020000787200116a6176612e7574696c2e48617368536574ba44859596b8b7340300007870770c000000103f40000000000002740014547275737453696d756c61746f725f53636f70657400066f70656e696478017371007e002f770c000000103f40000000000000787371007e001b3f4000000000000c770800000010000000047400046373726674002430333765316366302d303532352d346434362d623634392d3734306530663835323933667400056e6f6e636571007e002a74000d617070726f7665645f736974657372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b0200007870000000000000000374000e415554485f54494d455354414d507372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000015c1b85c435787874003d687474703a2f2f6c6f63616c686f73743a383038342f547275737453696d756c61746f725f5350312f6f70656e69645f636f6e6e6563745f6c6f67696e707371007e002f770c000000103f4000000000000171007e0024787372004f6f72672e737072696e676672616d65776f726b2e73656375726974792e61757468656e7469636174696f6e2e557365726e616d6550617373776f726441757468656e7469636174696f6e546f6b656e00000000000001400200024c000b63726564656e7469616c7371007e00054c00097072696e636970616c71007e00057871007e0003017371007e00077371007e000b0000000277040000000271007e000f71007e00117871007e0044737200486f72672e737072696e676672616d65776f726b2e73656375726974792e7765622e61757468656e7469636174696f6e2e57656241757468656e7469636174696f6e44657461696c7300000000000001400200024c000d72656d6f74654164647265737371007e000e4c000973657373696f6e496471007e000e787074000f303a303a303a303a303a303a303a31740020354134323442433830353642363635363437353039334345383931423530434470737200326f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657200000000000001400200075a00116163636f756e744e6f6e457870697265645a00106163636f756e744e6f6e4c6f636b65645a001563726564656e7469616c734e6f6e457870697265645a0007656e61626c65644c000b617574686f72697469657371007e00154c000870617373776f726471007e000e4c0008757365726e616d6571007e000e7870010101017371007e002c737200116a6176612e7574696c2e54726565536574dd98509395ed875b0300007870737200466f72672e737072696e676672616d65776f726b2e73656375726974792e636f72652e7573657264657461696c732e5573657224417574686f72697479436f6d70617261746f720000000000000140020000787077040000000271007e000f71007e00117870740006557365722031);

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `ix_authority` (`username`,`authority`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` VALUES('Auditor 1', 'ROLE_AUDITOR');
INSERT INTO `authorities` VALUES('Auditor 1', 'ROLE_USER');
INSERT INTO `authorities` VALUES('SP 1', 'ROLE_SP');
INSERT INTO `authorities` VALUES('SP 1', 'ROLE_USER');
INSERT INTO `authorities` VALUES('SP 2', 'ROLE_SP');
INSERT INTO `authorities` VALUES('SP 2', 'ROLE_USER');
INSERT INTO `authorities` VALUES('SP 3', 'ROLE_SP');
INSERT INTO `authorities` VALUES('SP 3', 'ROLE_USER');
INSERT INTO `authorities` VALUES('SP 4', 'ROLE_SP');
INSERT INTO `authorities` VALUES('SP 4', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 1', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 1', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 10', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 10', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 2', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 2', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 3', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 3', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 4', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 4', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 5', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 5', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 6', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 6', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 7', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 7', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 8', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 8', 'ROLE_USER');
INSERT INTO `authorities` VALUES('User 9', 'ROLE_END_USER');
INSERT INTO `authorities` VALUES('User 9', 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `authorization_code`
--

CREATE TABLE `authorization_code` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(256) DEFAULT NULL,
  `authentication` longblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `authorization_code`
--


-- --------------------------------------------------------

--
-- Table structure for table `blacklisted_site`
--

CREATE TABLE `blacklisted_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uri` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `blacklisted_site`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_authority`
--

CREATE TABLE `client_authority` (
  `owner_id` bigint(20) DEFAULT NULL,
  `authority` longblob
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_authority`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_contact`
--

CREATE TABLE `client_contact` (
  `owner_id` bigint(20) DEFAULT NULL,
  `contact` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_contact`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_default_acr_value`
--

CREATE TABLE `client_default_acr_value` (
  `owner_id` bigint(20) DEFAULT NULL,
  `default_acr_value` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_default_acr_value`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_details`
--

CREATE TABLE `client_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_description` varchar(1024) DEFAULT NULL,
  `reuse_refresh_tokens` tinyint(1) NOT NULL DEFAULT '1',
  `dynamically_registered` tinyint(1) NOT NULL DEFAULT '0',
  `allow_introspection` tinyint(1) NOT NULL DEFAULT '0',
  `id_token_validity_seconds` bigint(20) NOT NULL DEFAULT '600',
  `client_id` varchar(256) DEFAULT NULL,
  `client_secret` varchar(2048) DEFAULT NULL,
  `access_token_validity_seconds` bigint(20) DEFAULT NULL,
  `refresh_token_validity_seconds` bigint(20) DEFAULT NULL,
  `application_type` varchar(256) DEFAULT NULL,
  `client_name` varchar(256) DEFAULT NULL,
  `token_endpoint_auth_method` varchar(256) DEFAULT NULL,
  `subject_type` varchar(256) DEFAULT NULL,
  `logo_uri` varchar(2048) DEFAULT NULL,
  `policy_uri` varchar(2048) DEFAULT NULL,
  `client_uri` varchar(2048) DEFAULT NULL,
  `tos_uri` varchar(2048) DEFAULT NULL,
  `jwks_uri` varchar(2048) DEFAULT NULL,
  `sector_identifier_uri` varchar(2048) DEFAULT NULL,
  `request_object_signing_alg` varchar(256) DEFAULT NULL,
  `user_info_signed_response_alg` varchar(256) DEFAULT NULL,
  `user_info_encrypted_response_alg` varchar(256) DEFAULT NULL,
  `user_info_encrypted_response_enc` varchar(256) DEFAULT NULL,
  `id_token_signed_response_alg` varchar(256) DEFAULT NULL,
  `id_token_encrypted_response_alg` varchar(256) DEFAULT NULL,
  `id_token_encrypted_response_enc` varchar(256) DEFAULT NULL,
  `token_endpoint_auth_signing_alg` varchar(256) DEFAULT NULL,
  `default_max_age` bigint(20) DEFAULT NULL,
  `require_auth_time` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `initiate_login_uri` varchar(2048) DEFAULT NULL,
  `post_logout_redirect_uri` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `client_details`
--

INSERT INTO `client_details` VALUES(1, '', 1, 0, 1, 600, 'SP 1', '1234', 0, 0, NULL, 'SP 1', 'SECRET_BASIC', 'PUBLIC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-10-17 17:15:35', NULL, NULL);
INSERT INTO `client_details` VALUES(2, '', 1, 0, 1, 600, 'SP 2', '1234', 0, 0, NULL, 'SP 2', 'SECRET_BASIC', 'PUBLIC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-10-17 17:15:36', NULL, NULL);
INSERT INTO `client_details` VALUES(3, '', 1, 0, 1, 600, 'SP 3', '1234', 0, 0, NULL, 'SP 3', 'SECRET_BASIC', 'PUBLIC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-10-17 17:15:36', NULL, NULL);
INSERT INTO `client_details` VALUES(4, '', 1, 0, 1, 600, 'SP 4', '1234', 0, 0, NULL, 'SP 4', 'SECRET_BASIC', 'PUBLIC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-10-17 17:15:36', NULL, NULL);
INSERT INTO `client_details` VALUES(5, '', 1, 0, 1, 600, 'Auditor 1', '1234', 0, 0, NULL, 'Auditor 1', 'SECRET_BASIC', 'PUBLIC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2014-10-17 17:15:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_grant_type`
--

CREATE TABLE `client_grant_type` (
  `owner_id` bigint(20) DEFAULT NULL,
  `grant_type` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_grant_type`
--

INSERT INTO `client_grant_type` VALUES(1, 'authorization_code');
INSERT INTO `client_grant_type` VALUES(1, 'refresh_token');
INSERT INTO `client_grant_type` VALUES(2, 'authorization_code');
INSERT INTO `client_grant_type` VALUES(2, 'refresh_token');
INSERT INTO `client_grant_type` VALUES(3, 'authorization_code');
INSERT INTO `client_grant_type` VALUES(3, 'refresh_token');
INSERT INTO `client_grant_type` VALUES(4, 'authorization_code');
INSERT INTO `client_grant_type` VALUES(4, 'refresh_token');
INSERT INTO `client_grant_type` VALUES(5, 'authorization_code');
INSERT INTO `client_grant_type` VALUES(5, 'refresh_token');

-- --------------------------------------------------------

--
-- Table structure for table `client_redirect_uri`
--

CREATE TABLE `client_redirect_uri` (
  `owner_id` bigint(20) DEFAULT NULL,
  `redirect_uri` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_redirect_uri`
--

INSERT INTO `client_redirect_uri` VALUES(1, 'http://localhost:8084/TrustSimulator_SP1/openid_connect_login');
INSERT INTO `client_redirect_uri` VALUES(2, 'http://localhost:8084/TrustSimulator_SP2/openid_connect_login');
INSERT INTO `client_redirect_uri` VALUES(3, 'http://localhost:8086/TrustSimulator_SP3/openid_connect_login');
INSERT INTO `client_redirect_uri` VALUES(4, 'http://localhost:8086/TrustSimulator_SP4/openid_connect_login');
INSERT INTO `client_redirect_uri` VALUES(5, 'http://localhost:8085/TrustSimulator_Auditor1/openid_connect_login');

-- --------------------------------------------------------

--
-- Table structure for table `client_request_uri`
--

CREATE TABLE `client_request_uri` (
  `owner_id` bigint(20) DEFAULT NULL,
  `request_uri` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_request_uri`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_resource`
--

CREATE TABLE `client_resource` (
  `owner_id` bigint(20) DEFAULT NULL,
  `resource_id` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_resource`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_response_type`
--

CREATE TABLE `client_response_type` (
  `owner_id` bigint(20) DEFAULT NULL,
  `response_type` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_response_type`
--

INSERT INTO `client_response_type` VALUES(1, 'code');
INSERT INTO `client_response_type` VALUES(2, 'code');
INSERT INTO `client_response_type` VALUES(3, 'code');
INSERT INTO `client_response_type` VALUES(4, 'code');
INSERT INTO `client_response_type` VALUES(5, 'code');

-- --------------------------------------------------------

--
-- Table structure for table `client_scope`
--

CREATE TABLE `client_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_scope`
--

INSERT INTO `client_scope` VALUES(1, 'openid');
INSERT INTO `client_scope` VALUES(1, 'TrustSimulator_Scope');
INSERT INTO `client_scope` VALUES(1, 'offline_access');
INSERT INTO `client_scope` VALUES(2, 'openid');
INSERT INTO `client_scope` VALUES(2, 'TrustSimulator_Scope');
INSERT INTO `client_scope` VALUES(2, 'offline_access');
INSERT INTO `client_scope` VALUES(3, 'openid');
INSERT INTO `client_scope` VALUES(3, 'TrustSimulator_Scope');
INSERT INTO `client_scope` VALUES(3, 'offline_access');
INSERT INTO `client_scope` VALUES(4, 'openid');
INSERT INTO `client_scope` VALUES(4, 'TrustSimulator_Scope');
INSERT INTO `client_scope` VALUES(4, 'offline_access');
INSERT INTO `client_scope` VALUES(5, 'openid');
INSERT INTO `client_scope` VALUES(5, 'TrustSimulator_Scope');
INSERT INTO `client_scope` VALUES(5, 'offline_access');

-- --------------------------------------------------------

--
-- Table structure for table `pairwise_identifier`
--

CREATE TABLE `pairwise_identifier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(256) DEFAULT NULL,
  `sub` varchar(256) DEFAULT NULL,
  `sector_identifier` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `pairwise_identifier`
--


-- --------------------------------------------------------

--
-- Table structure for table `refresh_token`
--

CREATE TABLE `refresh_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `refresh_token`
--


-- --------------------------------------------------------

--
-- Table structure for table `system_scope`
--

CREATE TABLE `system_scope` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scope` varchar(256) NOT NULL,
  `description` varchar(4096) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `allow_dyn_reg` tinyint(1) NOT NULL DEFAULT '0',
  `default_scope` tinyint(1) NOT NULL DEFAULT '0',
  `structured` tinyint(1) NOT NULL DEFAULT '0',
  `structured_param_description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `scope` (`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `system_scope`
--


-- --------------------------------------------------------

--
-- Table structure for table `token_scope`
--

CREATE TABLE `token_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(2048) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `token_scope`
--

INSERT INTO `token_scope` VALUES(2, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(2, 'openid');
INSERT INTO `token_scope` VALUES(9, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(4, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(4, 'openid');
INSERT INTO `token_scope` VALUES(8, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(6, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(6, 'openid');
INSERT INTO `token_scope` VALUES(8, 'openid');
INSERT INTO `token_scope` VALUES(11, 'openid');
INSERT INTO `token_scope` VALUES(11, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(19, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(13, 'openid');
INSERT INTO `token_scope` VALUES(13, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(19, 'openid');
INSERT INTO `token_scope` VALUES(15, 'openid');
INSERT INTO `token_scope` VALUES(15, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(17, 'openid');
INSERT INTO `token_scope` VALUES(17, 'TrustSimulator_Scope');
INSERT INTO `token_scope` VALUES(21, 'openid');
INSERT INTO `token_scope` VALUES(21, 'TrustSimulator_Scope');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sub` varchar(256) DEFAULT NULL,
  `preferred_username` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `given_name` varchar(256) DEFAULT NULL,
  `family_name` varchar(256) DEFAULT NULL,
  `middle_name` varchar(256) DEFAULT NULL,
  `nickname` varchar(256) DEFAULT NULL,
  `profile` varchar(256) DEFAULT NULL,
  `picture` varchar(256) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  `gender` varchar(256) DEFAULT NULL,
  `zone_info` varchar(256) DEFAULT NULL,
  `locale` varchar(256) DEFAULT NULL,
  `phone_number` varchar(256) DEFAULT NULL,
  `phone_number_verified` tinyint(1) DEFAULT NULL,
  `address_id` varchar(256) DEFAULT NULL,
  `updated_time` varchar(256) DEFAULT NULL,
  `birthdate` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=88 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` VALUES(59, 'SP.59.SP1', 'SP 1', 'SP 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SP1@TrustSimulator.com', 1, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(61, 'SP.61.SP2', 'SP 2', 'SP 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SP2@TrustSimulator.com', 1, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(63, 'SP.63.SP3', 'SP 3', 'SP 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SP3@TrustSimulator.com', 1, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(65, 'SP.65.SP4', 'SP 4', 'SP 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SP4@TrustSimulator.com', 1, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(67, 'Auditor.67.Auditor1', 'Auditor 1', 'Auditor 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Auditor1@trustSimulator.com', 1, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(69, 'User.69.User1', 'User 1', 'User 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User1@trustSimulator.com', 1, NULL, NULL, NULL, '111222333', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(71, 'User.71.User2', 'User 2', 'User 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User2@trustSimulator.com', 1, NULL, NULL, NULL, '333222111', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(73, 'User.73.User3', 'User 3', 'User 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User3@trustSimulator.com', 1, NULL, NULL, NULL, '222333111', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(75, 'User.75.User4', 'User 4', 'User 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User4@trustSimulator.com', 1, NULL, NULL, NULL, '0', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(77, 'User.77.User5', 'User 5', 'User 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User5@trustSimulator.com', 1, NULL, NULL, NULL, '999888777', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(79, 'User.79.User6', 'User 6', 'User 6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User6@trustSimulator.com', 1, NULL, NULL, NULL, '888999777', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(81, 'User.81.User7', 'User 7', 'User 7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User7@trustSimulator.com', 1, NULL, NULL, NULL, '777999888', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(83, 'User.83.User8', 'User 8', 'User 8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User8@trustSimulator.com', 1, NULL, NULL, NULL, '999777888', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(85, 'User.85.User9', 'User 9', 'User 9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User9@trustSimulator.com', 1, NULL, NULL, NULL, '888777999', 1, NULL, NULL, NULL);
INSERT INTO `user_info` VALUES(87, 'User.87.User10', 'User 10', 'User 10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'User10@trustSimulator.com', 1, NULL, NULL, NULL, '0', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` VALUES('SP 1', '1234', 1);
INSERT INTO `users` VALUES('SP 2', '1234', 1);
INSERT INTO `users` VALUES('SP 3', '1234', 1);
INSERT INTO `users` VALUES('SP 4', '1234', 1);
INSERT INTO `users` VALUES('Auditor 1', '1234', 1);
INSERT INTO `users` VALUES('User 1', '1234', 1);
INSERT INTO `users` VALUES('User 2', '1234', 1);
INSERT INTO `users` VALUES('User 3', '1234', 1);
INSERT INTO `users` VALUES('User 4', '1234', 1);
INSERT INTO `users` VALUES('User 5', '1234', 1);
INSERT INTO `users` VALUES('User 6', '1234', 1);
INSERT INTO `users` VALUES('User 7', '1234', 1);
INSERT INTO `users` VALUES('User 8', '1234', 1);
INSERT INTO `users` VALUES('User 9', '1234', 1);
INSERT INTO `users` VALUES('User 10', '1234', 1);

-- --------------------------------------------------------

--
-- Table structure for table `whitelisted_site`
--

CREATE TABLE `whitelisted_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creator_user_id` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `whitelisted_site`
--


-- --------------------------------------------------------

--
-- Table structure for table `whitelisted_site_scope`
--

CREATE TABLE `whitelisted_site_scope` (
  `owner_id` bigint(20) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `whitelisted_site_scope`
--

--
-- Database: `TrustSimulator_MSP1`
--
CREATE DATABASE `TrustSimulator_MSP1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_MSP1`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('MSP', 57, '1234', '2014-10-17 15:15:35', '', 'MSP1@TrustSimulator.com', NULL, '2014-10-17 15:15:35', 'MSP 1', NULL, 'http://localhost:8083/TrustSimulator_MSP1', 'Emails', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'MSPLog', 'save', '2014-10-17 15:15:35', '', 'MSP1@TrustSimulator.com', 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', NULL, 'Emails', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(3, 'MSPDataLog', 'save', '2014-10-17 15:23:03', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(4, 'MessageLog', 'save', '2014-10-17 15:23:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 2, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(5, 'MSPDataLog', 'update', '2014-10-17 15:23:10', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(52, 'MSPDataLog', 'save', '2014-10-17 15:38:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 57, 65, 'http://localhost:8086/TrustSimulator_SP4', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(53, 'MessageLog', 'save', '2014-10-17 15:39:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 18, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(54, 'MSPDataLog', 'update', '2014-10-17 15:39:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 51, 57, 65, 'http://localhost:8086/TrustSimulator_SP4', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(102, 'MSPDataLog', 'save', '2016-09-06 14:12:48', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(103, 'MessageLog', 'save', '2016-09-06 14:14:48', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 99, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(104, 'MSPDataLog', 'update', '2016-09-06 14:14:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(106, 'MSPDataLog', 'save', '2016-09-06 14:16:32', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 105, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(107, 'MessageLog', 'save', '2016-09-06 14:16:39', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 118, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(108, 'MSPDataLog', 'update', '2016-09-06 14:16:39', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 105, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(151, 'MessageLog', 'save', '2016-09-06 14:14:51', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 102, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(152, 'MSPDataLog', 'update', '2016-09-06 14:14:54', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 101, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(202, 'MSPDataLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 201, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(204, 'MSPDataLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 203, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(251, 'MessageLog', 'save', '2017-05-18 15:26:13', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 208, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(252, 'MSPDataLog', 'update', '2017-05-18 15:26:13', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 201, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(253, 'MessageLog', 'save', '2017-05-18 15:26:23', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', 218, 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(254, 'MSPDataLog', 'update', '2017-05-18 15:26:23', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 203, 57, 59, 'http://localhost:8084/TrustSimulator_SP1', 69, 'email', 'User1@trustSimulator.com', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(2, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2014-10-17 15:23:10', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(18, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2014-10-17 15:39:02', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(99, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:14:39', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(102, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:14:49', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(118, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2016-09-06 14:16:38', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(208, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2017-05-18 15:26:13', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');
INSERT INTO `MESSAGE` VALUES(218, '<h2>Congratulations! </h2><h2>You Got an exclusive 75% discount to buy the best products FROM: </h2><h1>MSP 1</h1><h2>Do not miss this valuable offer, reply back NOW!!!</h2>', '2017-05-18 15:26:23', 'User1@trustSimulator.com', 'my friend', 'MSP1@TrustSimulator.com', 'MSP 1', 'unread', 'MSP 1 - Product Offer!!');

-- --------------------------------------------------------

--
-- Table structure for table `MSPDATA`
--

CREATE TABLE `MSPDATA` (
  `MSPDATAID` int(11) NOT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UserID` int(11) NOT NULL,
  `ObtainedDataType` varchar(32) DEFAULT NULL,
  `ObtainedDataValue` varchar(255) DEFAULT NULL,
  `UsedCount` int(11) NOT NULL,
  `MSPID` int(11) NOT NULL,
  `SPID` int(11) NOT NULL,
  `SPHost` varchar(255) NOT NULL,
  PRIMARY KEY (`MSPDATAID`),
  KEY `FK_MSPDATA_MSPID` (`MSPID`),
  KEY `FK_MSPDATA_SPID` (`SPID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MSPDATA`
--

INSERT INTO `MSPDATA` VALUES(2, '2014-10-17 15:23:03', 69, 'email', 'User1@trustSimulator.com', 1, 57, 59, 'http://localhost:8084/TrustSimulator_SP1');
INSERT INTO `MSPDATA` VALUES(51, '2014-10-17 15:38:59', 69, 'email', 'User1@trustSimulator.com', 1, 57, 65, 'http://localhost:8086/TrustSimulator_SP4');
INSERT INTO `MSPDATA` VALUES(101, '2016-09-06 14:12:47', 69, 'email', 'User1@trustSimulator.com', 1, 57, 59, 'http://localhost:8084/TrustSimulator_SP1');
INSERT INTO `MSPDATA` VALUES(105, '2016-09-06 14:16:32', 69, 'email', 'User1@trustSimulator.com', 1, 57, 59, 'http://localhost:8084/TrustSimulator_SP1');
INSERT INTO `MSPDATA` VALUES(201, '2017-05-18 15:26:11', 69, 'email', 'User1@trustSimulator.com', 1, 57, 59, 'http://localhost:8084/TrustSimulator_SP1');
INSERT INTO `MSPDATA` VALUES(203, '2017-05-18 15:26:21', 69, 'email', 'User1@trustSimulator.com', 1, 57, 59, 'http://localhost:8084/TrustSimulator_SP1');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 300);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

--
-- Database: `TrustSimulator_MSP2`
--
CREATE DATABASE `TrustSimulator_MSP2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_MSP2`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `AbstractEntity`
--


-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--


-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--


-- --------------------------------------------------------

--
-- Table structure for table `MSPDATA`
--

CREATE TABLE `MSPDATA` (
  `MSPDATAID` int(11) NOT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UserID` int(11) NOT NULL,
  `ObtainedDataType` varchar(32) DEFAULT NULL,
  `ObtainedDataValue` varchar(255) DEFAULT NULL,
  `UsedCount` int(11) NOT NULL,
  `MSPID` int(11) NOT NULL,
  `SPID` int(11) NOT NULL,
  `SPHost` varchar(255) NOT NULL,
  PRIMARY KEY (`MSPDATAID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MSPDATA`
--


-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

--
-- Database: `TrustSimulator_SP1`
--
CREATE DATABASE `TrustSimulator_SP1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP1`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_crgtjy6byy1mx2r899ti7ep6o` (`email`),
  UNIQUE KEY `name` (`name`),
  KEY `FK_lw7otj4nflromye8ooqphp1f5` (`affiliatedWithMsp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('SP', 59, '1234', '2014-10-17 15:15:35', '', 'SP1@TrustSimulator.com', NULL, '2014-10-17 15:15:35', 'SP 1', 'Gold', 'http://localhost:8084/TrustSimulator_SP1', NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1');

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'SPLog', 'save', '2014-10-17 15:15:35', '', 'SP1@TrustSimulator.com', 59, 'SP 1', 'http://localhost:8084/TrustSimulator_SP1', NULL, NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', 'Gold', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(52, 'SPDataLog', 'save', '2014-10-17 15:22:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 51, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(54, 'SPDataLog', 'save', '2014-10-17 15:22:59', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 53, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(55, 'MessageLog', 'save', '2014-10-17 15:23:06', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 100, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(102, 'SPDataLog', 'save', '2016-09-06 14:12:27', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 101, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(104, 'SPDataLog', 'save', '2016-09-06 14:12:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 103, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(106, 'SPDataLog', 'save', '2016-09-06 14:16:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 105, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(108, 'SPDataLog', 'save', '2016-09-06 14:16:31', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 107, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(109, 'MessageLog', 'save', '2016-09-06 14:16:33', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 116, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(152, 'SPDataLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 151, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(154, 'SPDataLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 153, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(155, 'MessageLog', 'save', '2017-05-18 15:26:11', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 206, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(157, 'SPDataLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 156, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(159, 'SPDataLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 158, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(160, 'MessageLog', 'save', '2017-05-18 15:26:21', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 216, 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(100, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:23:06', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(116, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2016-09-06 14:16:33', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(206, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2017-05-18 15:26:11', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(216, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Gold</h1><h2> From </h2><h1>SP 1</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2017-05-18 15:26:21', 'User1@trustSimulator.com', 'User 1', 'SP1@TrustSimulator.com', 'SP 1', 'unread', 'SP 1 - Service Confirmation');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 200);

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`),
  KEY `FK_SPDATA_spID` (`spID`),
  KEY `FK_SPDATA_userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SPDATA`
--

INSERT INTO `SPDATA` VALUES(51, '2014-10-17 15:22:59', 'name', 'User 1', 59, 69);
INSERT INTO `SPDATA` VALUES(53, '2014-10-17 15:22:59', 'email', 'User1@trustSimulator.com', 59, 69);
INSERT INTO `SPDATA` VALUES(101, '2016-09-06 14:12:24', 'name', 'User 1', 59, 69);
INSERT INTO `SPDATA` VALUES(103, '2016-09-06 14:12:31', 'email', 'User1@trustSimulator.com', 59, 69);
INSERT INTO `SPDATA` VALUES(105, '2016-09-06 14:16:31', 'name', 'User 1', 59, 69);
INSERT INTO `SPDATA` VALUES(107, '2016-09-06 14:16:31', 'email', 'User1@trustSimulator.com', 59, 69);
INSERT INTO `SPDATA` VALUES(151, '2017-05-18 15:26:10', 'name', 'User 1', 59, 69);
INSERT INTO `SPDATA` VALUES(153, '2017-05-18 15:26:11', 'email', 'User1@trustSimulator.com', 59, 69);
INSERT INTO `SPDATA` VALUES(156, '2017-05-18 15:26:21', 'name', 'User 1', 59, 69);
INSERT INTO `SPDATA` VALUES(158, '2017-05-18 15:26:21', 'email', 'User1@trustSimulator.com', 59, 69);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

--
-- Database: `TrustSimulator_SP2`
--
CREATE DATABASE `TrustSimulator_SP2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP2`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('SP', 61, '1234', '2014-10-17 15:15:35', '', 'SP2@TrustSimulator.com', NULL, '2014-10-17 15:15:35', 'SP 2', 'Silver', 'http://localhost:8084/TrustSimulator_SP2', NULL, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP2@TrustSimulator.com', 61, 'SP 2', 'http://localhost:8084/TrustSimulator_SP2', NULL, NULL, 0, '', '', 'Silver', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(3, 'SPDataLog', 'save', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 2, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(5, 'SPDataLog', 'save', '2014-10-17 15:18:01', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 4, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(6, 'MessageLog', 'save', '2014-10-17 15:18:02', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 93, 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(52, 'SPDataLog', 'save', '2016-09-06 14:15:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 51, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(54, 'SPDataLog', 'save', '2016-09-06 14:15:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 53, 61, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(55, 'MessageLog', 'save', '2016-09-06 14:15:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 110, 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(93, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:18:01', 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation');
INSERT INTO `MESSAGE` VALUES(110, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Silver</h1><h2> From </h2><h1>SP 2</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2016-09-06 14:15:45', 'User1@trustSimulator.com', 'User 1', 'SP2@TrustSimulator.com', 'SP 2', 'unread', 'SP 2 - Service Confirmation');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 100);

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SPDATA`
--

INSERT INTO `SPDATA` VALUES(2, '2014-10-17 15:18:01', 'name', 'User 1', 61, 69);
INSERT INTO `SPDATA` VALUES(4, '2014-10-17 15:18:01', 'email', 'User1@trustSimulator.com', 61, 69);
INSERT INTO `SPDATA` VALUES(51, '2016-09-06 14:15:43', 'name', 'User 1', 61, 69);
INSERT INTO `SPDATA` VALUES(53, '2016-09-06 14:15:45', 'email', 'User1@trustSimulator.com', 61, 69);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

--
-- Database: `TrustSimulator_SP3`
--
CREATE DATABASE `TrustSimulator_SP3` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP3`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('SP', 63, '1234', '2014-10-17 15:15:36', '', 'SP3@TrustSimulator.com', NULL, '2014-10-17 15:15:36', 'SP 3', 'Bronze', 'http://localhost:8086/TrustSimulator_SP3', NULL, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP3@TrustSimulator.com', 63, 'SP 3', 'http://localhost:8086/TrustSimulator_SP3', NULL, NULL, 0, '', '', 'Bronze', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(3, 'SPDataLog', 'save', '2014-10-17 15:23:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 2, 63, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(5, 'SPDataLog', 'save', '2014-10-17 15:23:44', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 4, 63, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(6, 'MessageLog', 'save', '2014-10-17 15:23:45', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Bronze</h1><h2> From </h2><h1>SP 3</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 9, 'User1@trustSimulator.com', 'User 1', 'SP3@TrustSimulator.com', 'SP 3', 'unread', 'SP 3 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(9, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Bronze</h1><h2> From </h2><h1>SP 3</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:23:44', 'User1@trustSimulator.com', 'User 1', 'SP3@TrustSimulator.com', 'SP 3', 'unread', 'SP 3 - Service Confirmation');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 50);

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SPDATA`
--

INSERT INTO `SPDATA` VALUES(2, '2014-10-17 15:23:44', 'name', 'User 1', 63, 69);
INSERT INTO `SPDATA` VALUES(4, '2014-10-17 15:23:44', 'email', 'User1@trustSimulator.com', 63, 69);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

--
-- Database: `TrustSimulator_SP4`
--
CREATE DATABASE `TrustSimulator_SP4` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `TrustSimulator_SP4`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `AbstractEntity`
--

INSERT INTO `AbstractEntity` VALUES('SP', 65, '1234', '2014-10-17 15:15:36', '', 'SP4@TrustSimulator.com', NULL, '2014-10-17 15:15:36', 'SP 4', 'Diamond', 'http://localhost:8086/TrustSimulator_SP4', NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1');

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractLog`
--

INSERT INTO `AbstractLog` VALUES(1, 'SPLog', 'save', '2014-10-17 15:15:36', '', 'SP4@TrustSimulator.com', 65, 'SP 4', 'http://localhost:8086/TrustSimulator_SP4', NULL, NULL, 57, 'MSP 1', 'http://localhost:8083/TrustSimulator_MSP1', 'Diamond', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(52, 'SPDataLog', 'save', '2014-10-17 15:38:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'name', 'User 1', 51, 65, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(54, 'SPDataLog', 'save', '2014-10-17 15:38:58', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'email', 'User1@trustSimulator.com', 53, 65, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `AbstractLog` VALUES(55, 'MessageLog', 'save', '2014-10-17 15:39:00', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Diamond</h1><h2> From </h2><h1>SP 4</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', 16, 'User1@trustSimulator.com', 'User 1', 'SP4@TrustSimulator.com', 'SP 4', 'unread', 'SP 4 - Service Confirmation', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MESSAGE`
--

INSERT INTO `MESSAGE` VALUES(16, '<h2>Congratulations! </h2><h2>You Got a new pack of </h2><h1>Diamond</h1><h2> From </h2><h1>SP 4</h1><h2>Your custom is highly appreciated - Keep Dealing with us!</h2>', '2014-10-17 15:39:00', 'User1@trustSimulator.com', 'User 1', 'SP4@TrustSimulator.com', 'SP 4', 'unread', 'SP 4 - Service Confirmation');

-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 100);

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SPDATA`
--

INSERT INTO `SPDATA` VALUES(51, '2014-10-17 15:38:58', 'name', 'User 1', 65, 69);
INSERT INTO `SPDATA` VALUES(53, '2014-10-17 15:38:58', 'email', 'User1@trustSimulator.com', 65, 69);

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `access_token`
--

