package trust.simulator.util;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorCase;
import trust.simulator.entity.Message;
import trust.simulator.service.AuditorService;
import trust.simulator.service.MessageService;

public class  Operations {
	
	private static String entityPath ="trust.simulator.entity.";
	
    @Autowired
    private static AuditorService auditorService;
	
	@Autowired
	private static MessageService messageService;
	
	public static String getCurrentTime(){
		
		Date dt = new Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(dt);
		return currentTime;
	}
	
	public static String getTableName(String entityName){
		int pathLength = entityPath.length();
		return entityName.substring(pathLength);
	}
	
	public static String routeView(HttpSession session, String manageView, String normalView){
		
		String ses_page = (String) session.getAttribute("ses_page");
        System.out.println("ses_page = "+ses_page);
        if(ses_page.equals(manageView)) return manageView;
        else return normalView;
		
	}
	
	public static String setMessageServiceRequestAcknowledgmentTitle(String senderName)
	{
		String title = senderName+" - Service Confirmation";
		return title;
	}
	
	public static String setMessageServiceRequestAcknowledgmentBody(String senderName, String sentService, String receiverName)
	{
		String body = "<h2>Congratulations! </h2>"+
					"<h2>You Got a new pack of </h2>"+
					"<h1>"+sentService+"</h1>"+
					"<h2> From </h2>"+
					"<h1>"+senderName+"</h1>"+
					"<h2>Your custom is highly appreciated - Keep Dealing with us!</h2>";
		return body;
	}
	
	public static String setMessageSpamTitle(String senderName)
	{
		String title = senderName+" - Product Offer!!";
		return title;
	}
	
	public static String setMessageSpamBody(String senderName, String receiverName)
	{
		String body = "<h2>Congratulations! </h2>"+
					"<h2>You Got an exclusive 75% discount to buy the best products FROM: </h2>"+
					"<h1>"+senderName+"</h1>"+
					"<h2>Do not miss this valuable offer, reply back NOW!!!</h2>";
		return body;
	}
	
	public static String setMessageAuditorUpdateTitle(String senderName)
	{
		return "A SPAM Case Update from: "+senderName;
	}
	
	public static String setMessageAuditorUpdateBody(AuditorCase auditorcase, Auditor auditor, Message message)
	{
		String body = "";
		
		System.out.println("auditorcase.getAuditorcaseAuditorID(): "+auditorcase.getAuditorcaseAuditorID());
		
		//Auditor auditor = auditorService.findByAuditorID(67);
		
		//Message message = messageService.findByMessageID(auditorcase.getAuditorcaseMessageID());
		
		if(auditorcase.getConvictedSPID() == 0)
		{
			body = 	"<h2>This is an update from Auditor "+auditor.getName()+" regarding your SPAM report which you sent on: "+auditorcase.getCreatedOn()+"</h2>"+
					"<h2>There is NO Convicted SP in your case yet</h2>"+
					"<h2>The current status of your report is: "+auditorcase.getStatus()+"</h2>"+
					"<h2>The current comment of the auditor is:"+auditorcase.getComment()+"</h2>"+
					"<h1>Below is the original message you reported: </h1>"+
					"<table class='recordsTable'> <tr class='recordsHeader'> <td colspan='4'>  </td> </tr>"+
					"<tr> <td class='cellTitle'>Sender name</td> <td class='cellData'>"+ message.getSenderName() + "</td> <td class='cellTitle'>Date</td>  <td class='cellData'>"+ message.getCreatedOn()+"</td> </tr>"+
					"<tr> <td colspan='2' class='cellTitle'>Title</td> <td colspan='2' class='cellData'>"+ message.getTitle() +"</tr>"+		
					"<tr class='recordsHeader'> <td colspan='4'>  </td> </tr>"+
					"<tr> <td class='cellTitle'>Body</td> <td colspan='3' rowspan='5' class='cellData'>"+message.getBody()+"</td> </tr> </table>";
		}
		else
		{
			body = 	"<h2>This is an update from Auditor "+auditor.getName()+" regarding your SPAM report which you sent on: "+auditorcase.getCreatedOn()+"</h2>"+
					"<h2>The Convicted SP in your case is:"+auditorcase.getConvictedSPID()+" </h2>"+
					"<h2>The current status of your report is: "+auditorcase.getStatus()+"</h2>"+
					"<h2>The current comment of the auditor is:"+auditorcase.getComment()+"</h2>"+
					"<h1>Below is the original message you reported: </h1>"+
					"<table class='recordsTable'> <tr class='recordsHeader'> <td colspan='4'>  </td> </tr>"+
					"<tr> <td class='cellTitle'>Sender name</td> <td class='cellData'>"+ message.getSenderName() + "</td> <td class='cellTitle'>Date</td>  <td class='cellData'>"+ message.getCreatedOn()+"</td> </tr>"+
					"<tr> <td colspan='2' class='cellTitle'>Title</td> <td colspan='2' class='cellData'>"+ message.getTitle() +"</tr>"+		
					"<tr class='recordsHeader'> <td colspan='4'>  </td> </tr>"+
					"<tr> <td class='cellTitle'>Body</td> <td colspan='3' rowspan='5' class='cellData'>"+message.getBody()+"</td> </tr> </table>";
		}
		return body;
	}
	
	public static int getIdFromSub(String sub)
	{
		int id = 0;
		
		String[] splitStrings = sub.split("\\.");
		
		id = Integer.parseInt(splitStrings[1]);
		
		System.out.println("ID: "+id);
		
		return id;
	}

}
