package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="SP")
@DiscriminatorValue("SP")
public class SP extends AbstractEntity{
	
	@Column (name = "offer", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String offer;

	@Column (name = "affiliatedWithMsp", unique = false, nullable = true)
    private int affiliatedWithMsp;
	
	@Column (name = "affiliatedWithMspName", unique = false, nullable = true)
    private String affiliatedWithMspName;
    
    @Column (name = "affiliatedWithMspHost", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String affiliatedWithMspHost;

	public SP() {
    }

    public SP(String name, String password, String description, String SpHost, String affiliatedWithMspName, int affiliatedWithMsp, String affiliatedWithMspHost, String offer, String email) {
        //this.setSpID(spID);
        this.setName(name);
        this.setPassword(password);
        this.setDescription(description);
        this.setOffer(offer);
        this.setHost(SpHost);
        this.setAffiliatedWithMsp(affiliatedWithMsp);
        this.setAffiliatedWithMspName(affiliatedWithMspName);
        this.setAffiliatedWithMspHost(affiliatedWithMspHost);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setModifiedOn(Operations.getCurrentTime());
        this.setEmail(email);
    }

	
	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public int getAffiliatedWithMsp() {
		return affiliatedWithMsp;
	}

	public void setAffiliatedWithMsp(int affiliatedWithMsp) {
		this.affiliatedWithMsp = affiliatedWithMsp;
	}
	
	public String getAffiliatedWithMspName() {
		return affiliatedWithMspName;
	}

	public void setAffiliatedWithMspName(String affiliatedWithMspName) {
		this.affiliatedWithMspName = affiliatedWithMspName;
	}

	public String getAffiliatedWithMspHost() {
		return affiliatedWithMspHost;
	}

	public void setAffiliatedWithMspHost(String affiliatedWithMspHost) {
		this.affiliatedWithMspHost = affiliatedWithMspHost;
	}
	
	//TODO ** restrict access to IDP entity only
	//     ** encrypt password OR Encrypt whole JSON
	//	   ** prevent IDP from accessing knowledge about MSP affiliation
	public JsonObject toJsonFull() {
		
		System.out.println("toJsonFull");
		
		JsonObject obj = new JsonObject();

		obj.addProperty("ID", this.getID());
		obj.addProperty("name", this.getName());
		obj.addProperty("password", this.getPassword());
		obj.addProperty("email", this.getEmail());
		obj.addProperty("host", this.getHost());
		if(this.getDescription() != null) obj.addProperty("description", this.getDescription());
		if((Integer) this.getAffiliatedWithMsp() != null) obj.addProperty("affiliatedWithMsp", this.getAffiliatedWithMsp());
		if(this.getAffiliatedWithMspName() != null) obj.addProperty("affiliatedWithMspName", this.getAffiliatedWithMspName());
		if(this.getAffiliatedWithMspHost() != null) obj.addProperty("affiliatedWithMspHost", this.getAffiliatedWithMspHost());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("modifiedOn", this.getModifiedOn());
		if(this.getOffer() != null) obj.addProperty("offer", this.getOffer());
		return obj;
	}
	
	public JsonObject toJson() {
		
		System.out.println("toJson");
		
		JsonObject obj = new JsonObject();

		obj.addProperty("ID", this.getID());
		obj.addProperty("name", this.getName());
		obj.addProperty("email", this.getEmail());
		obj.addProperty("host", this.getHost());
		if(this.getDescription() != null) obj.addProperty("description", this.getDescription());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("modifiedOn", this.getModifiedOn());
		if(this.getOffer() != null) obj.addProperty("offer", this.getOffer());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static SP fromJson(JsonObject obj) {
		
		SP ui = new SP();

		System.out.println("obj.has('affiliatedWithMsp') == "+obj.has("affiliatedWithMsp"));
		
		ui.setID(obj.has("ID") ? obj.get("ID").getAsInt() : null);
		ui.setName(obj.has("name") ? obj.get("name").getAsString() : null);
		ui.setPassword(obj.has("password") ? obj.get("password").getAsString() : null);
		ui.setEmail(obj.has("email") ? obj.get("email").getAsString() : null);
		ui.setHost(obj.has("host") ? obj.get("host").getAsString() : null);
		ui.setDescription(obj.has("description") ? obj.get("description").getAsString() : null);
		ui.setAffiliatedWithMsp(obj.has("affiliatedWithMsp") ? (obj.get("affiliatedWithMsp").getAsInt()) : 0);
		ui.setAffiliatedWithMspName(obj.has("affiliatedWithMspName") ? obj.get("affiliatedWithMspName").getAsString() : null);
		ui.setAffiliatedWithMspHost(obj.has("affiliatedWithMspHost") ? obj.get("affiliatedWithMspHost").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setModifiedOn(obj.has("modifiedOn") ? obj.get("modifiedOn").getAsString() : null);
		ui.setOffer(obj.has("offer") ? obj.get("offer").getAsString() : null);
		
		return ui;

	}

}
