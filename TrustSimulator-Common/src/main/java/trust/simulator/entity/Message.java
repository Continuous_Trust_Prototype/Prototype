package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity(name = "Message")
public class Message {

	@Id
	@GeneratedValue
    private int messageID;

	@Column (name = "senderEmail", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String senderEmail;
	
	@Column (name = "senderName", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String senderName;

	@Column (name = "receiverEmail", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String receiverEmail;
	
	@Column (name = "receiverName", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String receiverName;

	@Column (name = "title", unique = false, nullable = false, columnDefinition="varchar(255)")
    private String title;
	
	@Column (name = "body", unique = false, nullable = false, columnDefinition="mediumtext")
    private String body;
	
	@Column (name = "status", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String status;

	@Column (name = "createdOn", unique = false, nullable = false, columnDefinition="datetime")
    private String createdOn;

    public Message() {
    }

    public Message(String senderEmail, String senderName, String receiverEmail, String receiverName, String title, String body, String status) {
        //this.setMessageID(messageID);
    	this.setSenderEmail(senderEmail);
    	this.setSenderName(senderName);
    	this.setReceiverEmail(receiverEmail);
        this.setReceiverName(receiverName);
        this.setTitle(title);
        this.setBody(body);
        this.setStatus(status);
        this.setCreatedOn(Operations.getCurrentTime());
    }

	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
    
	public String getSenderName() {
		return senderName;
	}
	
	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status= status;
	}
	
    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("messageID", this.getMessageID());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("senderEmail", this.getSenderEmail());
		obj.addProperty("senderName", this.getSenderName());
		obj.addProperty("receiverEmail", this.getReceiverEmail());
		obj.addProperty("receiverName", this.getReceiverName());
		obj.addProperty("title", this.getTitle());
		obj.addProperty("body", this.getBody());
		obj.addProperty("status", this.getStatus());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static Message fromJson(JsonObject obj) {
		
		Message ui = new Message();

		ui.setMessageID(obj.has("messageID") ? obj.get("messageID").getAsInt() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setSenderEmail(obj.has("senderEmail") ? obj.get("senderEmail").getAsString() : null);
		ui.setSenderName(obj.has("senderName") ? obj.get("senderName").getAsString() : null);
		ui.setReceiverEmail(obj.has("receiverEmail") ? obj.get("receiverEmail").getAsString() : null);
		ui.setReceiverName(obj.has("receiverName") ? obj.get("receiverName").getAsString() : null);
		ui.setTitle(obj.has("title") ? obj.get("title").getAsString() : null);
		ui.setBody(obj.has("body") ? obj.get("body").getAsString() : null);
		ui.setStatus(obj.has("status") ? obj.get("status").getAsString() : null);
		
		return ui;

	}

}
