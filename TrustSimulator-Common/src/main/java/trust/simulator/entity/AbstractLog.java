package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;


@Entity
@Table(name = "AbstractLog")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name="discriminator",
    discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="AL")
public class AbstractLog {

	@Id
	@GeneratedValue
    private int ID;
	
	@Column (name = "entityID", unique = false, nullable = true, columnDefinition="int(5)")
	private int entityID;
	
	@Column (name = "action", unique = false, nullable = false, columnDefinition="varchar(255)")
    private String action;
	
	@Column (name = "name", unique = false, nullable = true, columnDefinition="varchar(128)")
    private String name;
	
	@Column (name = "email", unique = false, nullable = true, columnDefinition="varchar(128)")
    private String email;

	@Column (name = "description", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String description;
	
	@Column (name = "host", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String host;

	@Column (name = "createdOn", unique = false, nullable = false, columnDefinition="datetime")
    private String createdOn;
	
    public AbstractLog() {
    }

/*    public AbstractLog(AbstractEntity entity, String action) {
        this.setEntityID(entity.getID());
        this.setName(entity.getName());
        this.setEmail(entity.getEmail());
        this.setDescription(entity.getDescription());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setAction(action);
    }
*/
	public int getEntityID() {
		return entityID;
	}

	public void setEntityID(int entityID) {
		this.entityID = entityID;
	}
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

	public int getID() {
		return ID;
	}

	public void setAuditorLogID(int ID) {
		this.ID = ID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
