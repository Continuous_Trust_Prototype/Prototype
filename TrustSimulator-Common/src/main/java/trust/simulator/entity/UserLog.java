package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="UserLog")
@DiscriminatorValue("UserLog")
public class UserLog extends AbstractLog{

	@Column (name = "UserPhone", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String userPhone;

	public UserLog() {
    }

    public UserLog(User user, String action) {
        this.setEntityID(user.getID());
        this.setAction(action);
        this.setName(user.getName());
        this.setEmail(user.getEmail());
        this.setUserPhone(user.getPhone());
        this.setCreatedOn(Operations.getCurrentTime());
    }
    
    public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("logClass", this.getClass().getSimpleName());
		obj.addProperty("action", this.getAction());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("email", this.getEmail());
		obj.addProperty("entityID", this.getEntityID());
		if(this.getUserPhone() != null) obj.addProperty("userPhone", this.getUserPhone());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static UserLog fromJson(JsonObject obj) {
		
		UserLog ui = new UserLog();

		ui.setAction(obj.has("action") ? obj.get("action").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setEmail(obj.has("email") ? obj.get("email").getAsString() : null);
		ui.setEntityID(obj.has("entityID") ? obj.get("entityID").getAsInt() : null);
		ui.setUserPhone(obj.has("userPhone") ? obj.get("userPhone").getAsString() : null);
		
		return ui;

	}


}
