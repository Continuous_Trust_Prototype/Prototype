package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="MSP")
@DiscriminatorValue("MSP")
public class MSP extends AbstractEntity{

	
	@Column (name = "interestedIn", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String interestedIn;

    public MSP() {
    }

    public MSP(String name, String password, String description, String interestedIn, String email, String host) {
        //this.setMspID(mspID);
        this.setName(name);
        this.setPassword(password);
        this.setDescription(description);
        this.setInterestedIn(interestedIn);
        this.setEmail(email);
        this.setHost(host);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setModifiedOn(Operations.getCurrentTime());
    }


	public String getInterestedIn() {
		return interestedIn;
	}

	public void setInterestedIn(String interestedIn) {
		this.interestedIn = interestedIn;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("ID", this.getID());
		obj.addProperty("name", this.getName());
		obj.addProperty("email", this.getEmail());
		obj.addProperty("host", this.getHost());
		if(this.getDescription() != null) obj.addProperty("description", this.getDescription());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("modifiedOn", this.getModifiedOn());
		obj.addProperty("interestedIn", this.getInterestedIn());

		return obj;
	}
	
	//TODO ** Restrict access to Simulator entity only
	//     ** encrypt password OR Encrypt whole JSON
	public JsonObject toJsonFull() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("ID", this.getID());
		obj.addProperty("name", this.getName());
		obj.addProperty("password", this.getPassword());
		obj.addProperty("email", this.getEmail());
		obj.addProperty("host", this.getHost());
		if(this.getDescription() != null) obj.addProperty("description", this.getDescription());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("modifiedOn", this.getModifiedOn());
		obj.addProperty("interestedIn", this.getInterestedIn());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static MSP fromJson(JsonObject obj) {
		
		MSP ui = new MSP();

		ui.setID(obj.has("ID") ? obj.get("ID").getAsInt() : null);
		ui.setName(obj.has("name") ? obj.get("name").getAsString() : null);
		ui.setPassword(obj.has("password") ? obj.get("password").getAsString() : null);
		ui.setEmail(obj.has("email") ? obj.get("email").getAsString() : null);
		ui.setHost(obj.has("host") ? obj.get("host").getAsString() : null);
		ui.setDescription(obj.has("description") ? obj.get("description").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setModifiedOn(obj.has("modifiedOn") ? obj.get("modifiedOn").getAsString() : null);
		ui.setInterestedIn(obj.has("interestedIn") ? obj.get("interestedIn").getAsString() : null);
		
		return ui;

	}

}
