package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;

import trust.simulator.util.Operations;

@Entity
@Table(name="AuditorCaseLog")
@DiscriminatorValue("AuditorCaseLog")
public class AuditorCaseLog extends AbstractLog{
	
	@Column (name = "auditorcaseID", unique = false, nullable = true, columnDefinition="int(5)")
    private int auditorcaseID;
	
    @Column (name = "auditorcaseAuditorID", unique = false, nullable = true, columnDefinition="int(5)")
    private int auditorcaseAuditorID;
	
	@Column (name = "auditorcaseInvestigatedDataType", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String investigatedDataType;

	@Column (name = "auditorcaseInvestigatedDataValue", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String investigatedDataValue;
	
	@Column (name = "auditorcaseUserID", unique = false, nullable = true, columnDefinition="int(5)")
    private int auditorcaseUserID;
		
    @Column (name = "auditorcaseMessageID", unique = false, nullable = true, columnDefinition="int(11)")
    private int auditorcaseMessageID;

	@Column (name = "auditorcaseStatus", unique = false, nullable = true)
    private String status;
	
	@Column (name = "auditorcaseConvictedSPID", unique = false, nullable = true, columnDefinition="int(5)")
    private int convictedSPID;
	
	@Column (name = "auditorcaseComment", unique = false, nullable = true, columnDefinition="mediumtext")
    private String comment;
	

    public AuditorCaseLog() {
    }

    public AuditorCaseLog(AuditorCase auditorcase, String action) {
        this.setAuditorcaseID(auditorcase.getAuditorcaseID());
    	this.setAuditorcaseAuditorID(auditorcase.getAuditorcaseAuditorID());
        this.setInvestigatedDataType(auditorcase.getInvestigatedDataType());
        this.setInvestigatedDataValue(auditorcase.getInvestigatedDataValue());
        this.setAuditorcaseUserID(auditorcase.getAuditorcaseUserID());
        this.setAuditorcaseMessageID(auditorcase.getAuditorcaseMessageID());
        this.setStatus(auditorcase.getStatus());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setComment(auditorcase.getComment());
        if(auditorcase.getConvictedSPID() != 0)
        	this.setConvictedSPID(auditorcase.getConvictedSPID());
        this.setAction(action);
    }

	public int getAuditorcaseID() {
		return auditorcaseID;
	}

	public void setAuditorcaseID(int auditorcaseID) {
		this.auditorcaseID = auditorcaseID;
	}
    
	public int getAuditorcaseAuditorID() {
		return auditorcaseAuditorID;
	}

	public void setAuditorcaseAuditorID(int auditorcaseAuditorID) {
		this.auditorcaseAuditorID = auditorcaseAuditorID;
	}
    
	public String getInvestigatedDataType() {
		return investigatedDataType;
	}

	public void setInvestigatedDataType(String investigatedDataType) {
		this.investigatedDataType = investigatedDataType;
	}

	public String getInvestigatedDataValue() {
		return investigatedDataValue;
	}

	public void setInvestigatedDataValue(String investigatedDataValue) {
		this.investigatedDataValue = investigatedDataValue;
	}

	public int getAuditorcaseUserID() {
		return auditorcaseUserID;
	}

	public void setAuditorcaseUserID(int auditorcaseUserID) {
		this.auditorcaseUserID = auditorcaseUserID;
	}
	
	public int getAuditorcaseMessageID() {
		return auditorcaseMessageID;
	}

	public void setAuditorcaseMessageID(int auditorcaseMessageID) {
		this.auditorcaseMessageID = auditorcaseMessageID;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getConvictedSPID() {
		return convictedSPID;
	}

	public void setConvictedSPID(int convictedSPID) {
		this.convictedSPID = convictedSPID;
	}
	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("action", this.getAction());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("auditorcaseID", this.getAuditorcaseID());
		obj.addProperty("auditorcaseAuditorID", this.getAuditorcaseAuditorID());
		obj.addProperty("investigatedDataType", this.getInvestigatedDataType());
		obj.addProperty("investigatedDataValue", this.getInvestigatedDataValue());
		obj.addProperty("auditorcaseUserID", this.getAuditorcaseUserID());
		obj.addProperty("auditorcaseMessageID", this.getAuditorcaseMessageID());
		obj.addProperty("status", this.getStatus());
		if((Integer) this.getConvictedSPID() != null) obj.addProperty("convictedSPID", this.getConvictedSPID());
		if(this.getComment() != null) obj.addProperty("comment", this.getComment());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static AuditorCaseLog fromJson(JsonObject obj) {
		
		AuditorCaseLog ui = new AuditorCaseLog();

		ui.setAction(obj.has("action") ? obj.get("action").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setAuditorcaseID(obj.has("auditorcaseID") ? obj.get("auditorcaseID").getAsInt() : null);
		ui.setAuditorcaseAuditorID(obj.has("auditorcaseAuditorID") ? obj.get("auditorcaseAuditorID").getAsInt() : null);
		ui.setInvestigatedDataType(obj.has("investigatedDataType") ? obj.get("investigatedDataType").getAsString() : null);
		ui.setInvestigatedDataValue(obj.has("investigatedDataValue") ? obj.get("investigatedDataValue").getAsString() : null);
		ui.setAuditorcaseUserID(obj.has("auditorcaseUserID") ? obj.get("auditorcaseUserID").getAsInt() : null);
		ui.setAuditorcaseMessageID(obj.has("auditorcaseMessageID") ? obj.get("auditorcaseMessageID").getAsInt() : null);
		ui.setStatus(obj.has("status") ? obj.get("status").getAsString() : null);
		ui.setConvictedSPID(obj.has("convictedSPID") ? obj.get("convictedSPID").getAsInt() : null);
		ui.setComment(obj.has("comment") &&  !obj.get("comment").isJsonNull() ? obj.get("comment").getAsString() : null);
	
		return ui;

	}

}
