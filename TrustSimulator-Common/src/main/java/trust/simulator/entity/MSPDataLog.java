package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.JsonObject;


import trust.simulator.util.Operations;

@Entity
@Table(name="MSPDataLog")
@DiscriminatorValue("MSPDataLog")
public class MSPDataLog extends AbstractLog{
	
	@Column (name = "mspdataID", unique = false, nullable = true, columnDefinition="int(5)")
    private int mspdataID;
		
	@Column (name = "mspdataMSPID", unique = false, nullable = true, columnDefinition="int(5)")
    private int mspdataMSPID;

	@Column (name = "mspdataObtainedDataType", unique = false, nullable = true, columnDefinition="varchar(32)")
    private String obtainedDataType;

	@Column (name = "mspdataObtainedDataValue", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String obtainedDataValue;
	
	@Column (name = "mspdataUserID", unique = false, nullable = true, columnDefinition="int(5)")
    private int mspdataUserID;
	
	@Column (name = "mspdataSPID", unique = false, nullable = true, columnDefinition="int(5)")
    private int mspdataSPID;
	
	@Column (name = "mspdataSPHost", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String mspdataSPHost;
	
	@Column (name = "mspdataUsedCount", unique = false, nullable = true)
    private int usedCount;

    public MSPDataLog() {
    }

    public MSPDataLog(MSPData mspdata, String action) {
        this.setMspdataID(mspdata.getMspdataID());
    	this.setMspdataMSPID(mspdata.getMspdataMSPID());
        this.setObtainedDataType(mspdata.getObtainedDataType());
        this.setObtainedDataValue(mspdata.getObtainedDataValue());
        this.setMspdataUserID(mspdata.getMspdataUserID());
        this.setMspdataSPID(mspdata.getMspdataSPID());
        this.setMspdataSPHost(mspdata.getMspdataSPHost());
        this.setCreatedOn(Operations.getCurrentTime());
        this.setUsedCount(mspdata.getUsedCount());
        this.setAction(action);
    }

	public int getMspdataID() {
		return mspdataID;
	}

	public void setMspdataID(int mspdataID) {
		this.mspdataID = mspdataID;
	}
    
	public int getMspdataMSPID() {
		return mspdataMSPID;
	}

	public void setMspdataMSPID(int mspdataMSPID) {
		this.mspdataMSPID = mspdataMSPID;
	}
    
	public String getObtainedDataType() {
		return obtainedDataType;
	}

	public void setObtainedDataType(String obtainedDataType) {
		this.obtainedDataType = obtainedDataType;
	}

	public String getObtainedDataValue() {
		return obtainedDataValue;
	}

	public void setObtainedDataValue(String obtainedDataValue) {
		this.obtainedDataValue = obtainedDataValue;
	}

	public int getMspdataUserID() {
		return mspdataUserID;
	}

	public void setMspdataUserID(int mspdataUserID) {
		this.mspdataUserID = mspdataUserID;
	}
	
	public int getMspdataSPID() {
		return mspdataSPID;
	}

	public void setMspdataSPID(int mspdataSPID) {
		this.mspdataSPID = mspdataSPID;
	}
	
	public String getMspdataSPHost() {
		return mspdataSPHost;
	}

	public void setMspdataSPHost(String mspdataSPHost) {
		this.mspdataSPHost = mspdataSPHost;
	}

	public int getUsedCount() {
		return usedCount;
	}

	public void setUsedCount(int usedCount) {
		this.usedCount = usedCount;
	}
	
	public JsonObject toJson() {
		
		JsonObject obj = new JsonObject();

		obj.addProperty("action", this.getAction());
		obj.addProperty("createdOn", this.getCreatedOn());
		obj.addProperty("mspdataID", this.getMspdataID());
		obj.addProperty("mspdataMSPID", this.getMspdataMSPID());
		obj.addProperty("obtainedDataType", this.getObtainedDataType());
		obj.addProperty("obtainedDataValue", this.getObtainedDataValue());
		obj.addProperty("mspdataUserID", this.getMspdataUserID());
		obj.addProperty("mspdataSPID", this.getMspdataSPID());
		obj.addProperty("mspdataSPHost", this.getMspdataSPHost());
		obj.addProperty("usedCount", this.getUsedCount());

		return obj;
	}

	/**
	 * Parse a JsonObject into a UserInfo.
	 * @param o
	 * @return
	 */
	public static MSPDataLog fromJson(JsonObject obj) {
		
		MSPDataLog ui = new MSPDataLog();

		ui.setAction(obj.has("action") ? obj.get("action").getAsString() : null);
		ui.setCreatedOn(obj.has("createdOn") ? obj.get("createdOn").getAsString() : null);
		ui.setMspdataID(obj.has("mspdataID") ? obj.get("mspdataID").getAsInt() : null);
		ui.setMspdataMSPID(obj.has("mspdataMSPID") ? obj.get("mspdataMSPID").getAsInt() : null);
		ui.setObtainedDataType(obj.has("obtainedDataType") ? obj.get("obtainedDataType").getAsString() : null);
		ui.setObtainedDataValue(obj.has("obtainedDataValue") ? obj.get("obtainedDataValue").getAsString() : null);
		ui.setMspdataUserID(obj.has("mspdataUserID") ? obj.get("mspdataUserID").getAsInt() : null);
		ui.setMspdataSPID(obj.has("mspdataSPID") ? obj.get("mspdataSPID").getAsInt() : null);
		ui.setMspdataSPHost(obj.has("mspdataSPHost") ? obj.get("mspdataSPHost").getAsString() : null);
		ui.setUsedCount(obj.has("usedCount") ? obj.get("usedCount").getAsInt() : null);
		
		return ui;

	}

}
