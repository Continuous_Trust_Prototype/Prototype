package trust.simulator.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import trust.simulator.util.Operations;

@Entity(name = "MSPData")
public class MSPData {

	@Id
	@GeneratedValue
    private int mspdataID;
	
    @Column (name = "MSPID", unique = false, nullable = false)
    private int mspdataMSPID;

	@Column (name = "ObtainedDataType", unique = false, nullable = false, columnDefinition="varchar(32)")
    private String obtainedDataType;

	@Column (name = "ObtainedDataValue", unique = false, nullable = true, columnDefinition="varchar(255)")
    private String obtainedDataValue;
	
    @Column (name = "UserID", unique = false, nullable = false)
    private int mspdataUserID;
	
    @Column (name = "SPID", unique = false, nullable = false)
    private int mspdataSPID;
    
    @Column (name = "SPHost", unique = false, nullable = false)
    private String mspdataSPHost;
	
	@Column (name = "UsedCount", unique = false, nullable = false)
    private int usedCount;

	@Column (name = "CreatedOn", unique = false, nullable = false, columnDefinition="datetime")
    private String createdOn;

    public MSPData() {
    }

    public MSPData(int mspdataMSPID, String obtainedDataType, String obtainedDataValue, int mspdataUserID, int mspdataSPID, String spHost) {
        //this.setMspdataID(mspdataID);
    	this.setMspdataMSPID(mspdataMSPID);
        this.setObtainedDataType(obtainedDataType);
        this.setObtainedDataValue(obtainedDataValue);
        this.setMspdataUserID(mspdataUserID);
        this.setMspdataSPID(mspdataSPID);
        this.setMspdataSPHost(spHost);
        this.setCreatedOn(Operations.getCurrentTime());
        this.setUsedCount(0);
    }

	public int getMspdataID() {
		return mspdataID;
	}

	public void setMspdataID(int mspdataID) {
		this.mspdataID = mspdataID;
	}
    
	public int getMspdataMSPID() {
		return mspdataMSPID;
	}

	public void setMspdataMSPID(int mspdataMSPID) {
		this.mspdataMSPID = mspdataMSPID;
	}
    
	public String getObtainedDataType() {
		return obtainedDataType;
	}

	public void setObtainedDataType(String obtainedDataType) {
		this.obtainedDataType = obtainedDataType;
	}

	public String getObtainedDataValue() {
		return obtainedDataValue;
	}

	public void setObtainedDataValue(String obtainedDataValue) {
		this.obtainedDataValue = obtainedDataValue;
	}

	public int getMspdataUserID() {
		return mspdataUserID;
	}

	public void setMspdataUserID(int mspdataUserID) {
		this.mspdataUserID = mspdataUserID;
	}
	
	public int getMspdataSPID() {
		return mspdataSPID;
	}

	public void setMspdataSPID(int mspdataSPID) {
		this.mspdataSPID = mspdataSPID;
	}
	
    public String getMspdataSPHost() {
		return mspdataSPHost;
	}

	public void setMspdataSPHost(String mspdataSPHost) {
		this.mspdataSPHost = mspdataSPHost;
	}

	public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

	public int getUsedCount() {
		return usedCount;
	}

	public void setUsedCount(int usedCount) {
		this.usedCount = usedCount;
	}


}
