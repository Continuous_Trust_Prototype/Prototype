package trust.simulator.oauth2.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users_oAuth2 {

	@Id
    private String username;
	
	@Column (name = "password", unique = false, nullable = false, columnDefinition="varchar(50)")
    private String password;

	@Column (name = "enabled", unique = false, nullable = false, columnDefinition="tinyint(1)")
    private int enabled;
	
    public Users_oAuth2() {
    }

    public Users_oAuth2(String username, String password, int enabled) {
        this.setUsername(username);
        this.setPassword(password);
        this.setEnabled(enabled);
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	
}
