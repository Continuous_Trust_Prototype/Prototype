package trust.simulator.oauth2.mapping;

public class AuthoritiesPK_oAuth2 {
    private String username;
    private String authority;

    public AuthoritiesPK_oAuth2() {
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return this.authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
