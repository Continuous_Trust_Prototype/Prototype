package trust.simulator.dao;

import trust.simulator.entity.UserLog;

public interface UserLogDao extends AbstractDao<UserLog, String> {

}
