package trust.simulator.dao;

import trust.simulator.entity.MessageLog;

public interface MessageLogDao extends AbstractDao<MessageLog, String> {

}
