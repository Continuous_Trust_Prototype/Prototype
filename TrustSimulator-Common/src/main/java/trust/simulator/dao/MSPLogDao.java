package trust.simulator.dao;

import trust.simulator.entity.MSPLog;

public interface MSPLogDao extends AbstractDao<MSPLog, String> {

}
