package trust.simulator.dao;

import trust.simulator.entity.Auditor;

public interface AuditorDao extends AbstractDao<Auditor, String> {

}
