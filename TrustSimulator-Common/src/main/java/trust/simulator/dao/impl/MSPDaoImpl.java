package trust.simulator.dao.impl;


import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import trust.simulator.dao.MSPDao;
import trust.simulator.entity.AbstractEntity;
import trust.simulator.entity.MSP;
import trust.simulator.entity.MSPLog;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPLog;

@Repository
public class MSPDaoImpl extends AbstractDaoImpl<MSP, String> implements MSPDao {

    protected MSPDaoImpl() {
        super(MSP.class);
    }
    
	//Log Events
    @Override
    public MSP save(MSP e) {
        getCurrentSession().persist(e);
       
        MSPLog mspLog = new MSPLog(e, "save");
        getCurrentSession().persist(mspLog);
        getCurrentSession().flush();
        return e;
        
/*        try {
        	
			idpApi.persistSP(Constants.idp_host, Constants.idp_persistSP_path, e, "save");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
    }
    
    //Log Events
    @Override
    public void update(MSP e) {
       
    	getCurrentSession().merge(e);
        
    	MSPLog mspLog = new MSPLog(e, "update");
        getCurrentSession().persist(mspLog);
        getCurrentSession().flush();
        
      /*  try {
        	
			idpApi.persistSP(Constants.idp_host, Constants.idp_persistSP_path, e, "update");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}*/
    }

    //Log Events
    @Override
	public void delete(MSP e) {
       
    	/*try {
        	
			idpApi.persistSP(Constants.idp_host, Constants.idp_persistSP_path, e, "delete");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} */
    	
    	MSP msp = getCurrentSession().getReference(MSP.class, e.getID());
    	getCurrentSession().remove(msp);
        
    	MSPLog mspLog = new MSPLog(e, "remove");
        getCurrentSession().persist(mspLog);
        
        
        //TODO instead of updating all SPs, let each SP check if the MSP it is 
        //Affiliated with still existing before communicating with it or not
    	Query query = getCurrentSession().createNativeQuery("SELECT * FROM AbstractEntity where "
    			+"affiliatedWithMsp = '"+e.getID()+"'", AbstractEntity.class);
        
        @SuppressWarnings("unchecked")
		List<SP> sps = query.getResultList();
        Iterator<SP> iterator = sps.iterator();
        while (iterator.hasNext()) {
            SP sp = iterator.next();
            sp.setAffiliatedWithMsp(0);
            SPLog spLog = new SPLog(sp, "update");
            getCurrentSession().persist(spLog);
        }
        
        getCurrentSession().flush();
    }

}
