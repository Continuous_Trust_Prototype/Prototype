package trust.simulator.dao.impl;

import org.springframework.stereotype.Repository;

import trust.simulator.dao.SPDao;
import trust.simulator.entity.SP;

@Repository
public class SPDaoImpl extends AbstractDaoImpl<SP, String> implements SPDao {

    protected SPDaoImpl() {
        super(SP.class);
    }

}
