package trust.simulator.dao.impl;


import org.springframework.stereotype.Repository;

import trust.simulator.dao.UserLogDao;
import trust.simulator.entity.UserLog;

@Repository
public class UserLogDaoImpl extends AbstractDaoImpl<UserLog, String> implements UserLogDao {

    protected UserLogDaoImpl() {
        super(UserLog.class);
    }

}
