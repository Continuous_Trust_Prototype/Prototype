package trust.simulator.dao.impl;


import org.springframework.stereotype.Repository;

import trust.simulator.dao.SPLogDao;
import trust.simulator.entity.SPLog;

@Repository
public class SPLogDaoImpl extends AbstractDaoImpl<SPLog, String> implements SPLogDao {

    protected SPLogDaoImpl() {
        super(SPLog.class);
    }

}
