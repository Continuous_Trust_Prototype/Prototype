package trust.simulator.dao.impl;

import org.springframework.stereotype.Repository;
import trust.simulator.dao.AuditorCaseLogDao;
import trust.simulator.entity.AuditorCaseLog;

@Repository
public class AuditorCaseLogDaoImpl extends AbstractDaoImpl<AuditorCaseLog, String> implements AuditorCaseLogDao {

    protected AuditorCaseLogDaoImpl() {
        super(AuditorCaseLog.class);
    }

}
