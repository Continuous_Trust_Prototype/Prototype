package trust.simulator.dao;

import java.util.List;

import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.User;

public interface UserDao extends AbstractDao<User, String> {
	
	List<AbstractLog> getAllUserLogs(int userID);
	List<AbstractLog> getAllUserLogs(int userID, String untilTime);
	
}
