package trust.simulator.dao;

import trust.simulator.entity.LogsCollectionLog;

public interface LogsCollectionLogDao extends AbstractDao<LogsCollectionLog, String> {

}
