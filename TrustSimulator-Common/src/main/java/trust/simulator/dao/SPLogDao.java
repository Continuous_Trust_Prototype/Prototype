package trust.simulator.dao;

import trust.simulator.entity.SPLog;

public interface SPLogDao extends AbstractDao<SPLog, String> {

}
