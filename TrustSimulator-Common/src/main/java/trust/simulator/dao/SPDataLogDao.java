package trust.simulator.dao;

import trust.simulator.entity.SPDataLog;

public interface SPDataLogDao extends AbstractDao<SPDataLog, String> {

}
