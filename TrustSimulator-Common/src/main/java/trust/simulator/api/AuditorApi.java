package trust.simulator.api;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import trust.simulator.entity.Auditor;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class AuditorApi {
	
	public AuditorApi()
	{
		
	}
	
	public void persistAuditor(String host, String path, Auditor auditor, String action) throws URISyntaxException, ClientProtocolException, IOException
	{		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(host+path);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(auditor.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_auditor", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_auditor", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}
	
	public void resetSimulation(String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(host+path);
	
		httpclient.execute(httpGet);
	}
}
