package trust.simulator.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import trust.simulator.entity.MSP;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPData;

public class MSPApi {
	
	public MSPApi()
	{
		
	}
	
	public void addSPdata(SPData spdata, SP sp, String host, String path) throws URISyntaxException, IOException
	{		
		
		System.out.println("MSP API 1");
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(host+path);
		
		//String spdataString = spdata.toJson().toString();
		
		JsonArray jsonArray = new JsonArray();    	
    	
		jsonArray.add(sp.toJson());
		jsonArray.add(spdata.toJson());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("sp_spdata", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("sp_spdata", jsonArrayString));
		
	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);		
	}
	
	public void persistMSP(String host, String path, MSP msp, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(host+path);
		System.out.println("httpPost.getURI(): "+httpPost.getURI().toString());
		System.out.println("uri: "+uri.toString());
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(msp.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_msp", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_msp", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}
	
	public void resetSimulation(String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(host+path);
	
		httpclient.execute(httpGet);
	}
}
