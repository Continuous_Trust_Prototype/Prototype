package trust.simulator.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import trust.simulator.entity.Auditor;
import trust.simulator.entity.AuditorCaseLog;
import trust.simulator.entity.IDPLog;
import trust.simulator.entity.MSP;
import trust.simulator.entity.MSPDataLog;
import trust.simulator.entity.Message;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPDataLog;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class SimulatorApi {
	
	public SimulatorApi()
	{
		
	}
	
	public void persistSP(String host, String path, SP sp, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(sp.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_sp", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_sp", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}
	
	public void persistAuditor(String host, String path, Auditor auditor, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(auditor.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_auditor", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_auditor", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}

	public void persistMSP(String host, String path, MSP msp, String action) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		JsonArray jsonArray = new JsonArray();    	
    	
		JsonObject actionJson = new JsonObject();

		actionJson.addProperty("action", action);
		
		jsonArray.add(actionJson);
		jsonArray.add(msp.toJsonFull());
    	
    	JsonObject mainJsonObj = new JsonObject();
    	mainJsonObj.add("action_msp", jsonArray);
    	
    	String jsonArrayString = mainJsonObj.toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("action_msp", jsonArrayString));		

	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);
	}

	
	public List<SP> getSPs(String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		List<SP> sps = new ArrayList<SP>(); 
		
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.addHeader("Accept","application/json");
		
		HttpResponse response1 = httpclient.execute(httpGet);	
		
        HttpEntity entity1 = response1.getEntity();    
        
        String jsonString = EntityUtils.toString(entity1);
        
        JsonParser parser = new JsonParser();
        System.out.println("jsonString: "+jsonString);
        JsonObject jsonObj = (JsonObject)parser.parse(jsonString);
        
        JsonArray jsonMainArr = jsonObj.getAsJsonArray("sps"); 
        
        for(int i=0; i<jsonMainArr.size(); i++)
        {
        	SP sp = SP.fromJson((JsonObject) jsonMainArr.get(i));
        	sps.add(i, sp);
        }
        
        EntityUtils.consume(entity1);

        return sps;
		
	}

	public List<MSP> getMSPs(String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		List<MSP> msps = new ArrayList<MSP>(); 
		
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.addHeader("Accept","application/json");
		
		HttpResponse response1 = httpclient.execute(httpGet);	
		
        HttpEntity entity1 = response1.getEntity();    
        
        String jsonString = EntityUtils.toString(entity1);
        
        JsonParser parser = new JsonParser();
        //System.out.println("jsonString: "+jsonString);
        JsonObject jsonObj = (JsonObject)parser.parse(jsonString);
        
        JsonArray jsonMainArr = jsonObj.getAsJsonArray("msps"); 
        
        for(int i=0; i<jsonMainArr.size(); i++)
        {
        	MSP msp = MSP.fromJson((JsonObject) jsonMainArr.get(i));
        	msps.add(i, msp);
        }
        
        EntityUtils.consume(entity1);

        return msps;
		
	}
	
	public MSP getMSP(String host, String path, int mspID) throws URISyntaxException, ClientProtocolException, IOException
	{
		MSP msp = new MSP(); 
		
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .setParameter("mspID", Integer.toString(mspID))
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.addHeader("Accept","application/json");
		
		HttpResponse response1 = httpclient.execute(httpGet);	
		
        HttpEntity entity1 = response1.getEntity();    
        
        String jsonString = EntityUtils.toString(entity1);
       
        EntityUtils.consume(entity1);
       
        JsonParser parser = new JsonParser();
        System.out.println("jsonString: "+jsonString);
        JsonObject jsonObj = (JsonObject)parser.parse(jsonString);
        
        msp = MSP.fromJson(jsonObj);
                
        EntityUtils.consume(entity1);

        return msp;
	}

	
	public void addIDPLog(IDPLog idplog, String host, String path) throws URISyntaxException, ClientProtocolException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		String idplogString = idplog.toJson().toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("idpLog", idplogString));

		
	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);	
		
	}


	public void addSPdataLog(SPDataLog spdatalog, String host, String path) throws URISyntaxException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		String spdatalogString = spdatalog.toJson().toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("spdataLog", spdatalogString));

		
	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);	
		
	}
	
	public void addMSPdataLog(MSPDataLog mspdatalog, String host, String path) throws URISyntaxException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		String mspdatalogString = mspdatalog.toJson().toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("mspdataLog", mspdatalogString));

		
	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);	
		
	}
	
	public void addAuditorCaseLog(AuditorCaseLog auditorcaseLog, String host, String path) throws URISyntaxException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		String auditorcaselogString = auditorcaseLog.toJson().toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("auditorcaseLog", auditorcaselogString));

		
	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
		httpclient.execute(httpPost);	
	}
	
	public int addMessage(Message message, String host, String path) throws URISyntaxException, IOException
	{
		URI uri = new URIBuilder()
        .setScheme("http")
        .setHost(host)
        .setPath(path)
        .build();
		
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(uri);
		
		String messageString = message.toJson().toString();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("message", messageString));

		
	    httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
		
	    HttpResponse response1 = httpclient.execute(httpPost);	
	    
	    HttpEntity entity1 = response1.getEntity();    
        
	    String newMessageIDString = EntityUtils.toString(entity1);
        
	    //System.out.println("newMessageIDString "+newMessageIDString);
	    
	    int newMessageID = Integer.parseInt(newMessageIDString);
        
        EntityUtils.consume(entity1);
        
        return newMessageID;
		
	}
}
