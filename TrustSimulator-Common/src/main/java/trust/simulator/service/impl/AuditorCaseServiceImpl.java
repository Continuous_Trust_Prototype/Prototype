package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.AbstractLogDao;
import trust.simulator.dao.AuditorCaseDao;
import trust.simulator.entity.AuditorCase;
import trust.simulator.service.AuditorCaseService;


@Service("AuditorCaseService")
@Transactional(readOnly = true)
public class AuditorCaseServiceImpl implements AuditorCaseService {

    @Autowired
    private AuditorCaseDao auditorcaseDao;

    @Autowired
    private AbstractLogDao auditorcaseLogDao;
    
    @Override
    public AuditorCase findByAuditorCaseID(int auditorcaseID) {
        return auditorcaseDao.findById(auditorcaseID);
    }

    @Override
    @Transactional(readOnly = false)
    public int saveAuditorCase(AuditorCase auditorcase) {
        return auditorcaseDao.save(auditorcase).getAuditorcaseID();
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateAuditorCase(AuditorCase auditorcase) {
        auditorcaseDao.update(auditorcase);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteAuditorCase(int auditorcaseID) {
        AuditorCase auditorcase = auditorcaseDao.findById(auditorcaseID);
        if (auditorcase != null) {
            auditorcaseDao.delete(auditorcase);
        }
    }

	@Override
	public List<AuditorCase> getAllAuditorCases(int auditorID) {
		return auditorcaseDao.getAllCases(auditorID);
		
	}
	

/*	@Override
	public int getNewId() {
		return auditorcaseDao.getNewId();
	}
*/
}
