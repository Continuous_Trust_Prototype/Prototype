package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.MessageDao;
import trust.simulator.dao.UserDao;
import trust.simulator.entity.AbstractLog;
import trust.simulator.entity.Message;
import trust.simulator.entity.User;
import trust.simulator.service.MessagesNotification;
import trust.simulator.service.UserService;


@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    
    @Autowired
    private MessageDao messageDao;
    
    @Override
    public User findByUserID(int userID) {
        return userDao.findById(userID);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveUser(User user) {
        userDao.save(user);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateUser(User user) {
        userDao.update(user);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteUser(User user) {
    	userDao.delete(user);
    }

	@Override
	public List<User> getAllUsers() {
		return userDao.getAllInstances();
		
	}
	
    @Override
    public User findByName(String name) {
        return userDao.findByName(name);
    }

	/*@Override
	public int getNewId() {
		return userDao.getNewId();
	}*/

	@Override
	public MessagesNotification getMessagesNotification(User user) {
		return messageDao.getMessagesNotification(user.getEmail());
	}
	
	@Override
	public List<Message> getUserMessages(User user) {
		return messageDao.getUserMessages(user.getEmail());
	}
	
	@Override
	public List<AbstractLog> getAllUserLogs(int userID) {
		return userDao.getAllUserLogs(userID);
		
	}
	
	@Override
	public List<AbstractLog> getAllUserLogs(int userID, String untilTime) {
		return userDao.getAllUserLogs(userID, untilTime);
		
	}

}
