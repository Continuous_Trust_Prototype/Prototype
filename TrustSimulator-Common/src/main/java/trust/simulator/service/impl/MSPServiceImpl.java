package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.AbstractLogDao;
import trust.simulator.dao.MSPDao;
import trust.simulator.entity.MSP;
import trust.simulator.service.MSPService;


@Service("MSPService")
@Transactional(readOnly = true)
public class MSPServiceImpl implements MSPService {

    @Autowired
    private MSPDao mspDao;
    
    @Autowired
    private AbstractLogDao mspdataLogDao;

    @Override
    public MSP findByMspID(int mspID) {
        return mspDao.findById(mspID);
    }
    
    @Override
    public MSP findByName(String name) {
        return mspDao.findByName(name);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveMsp(MSP msp) {
        mspDao.save(msp);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateMsp(MSP msp) {
        mspDao.update(msp);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteMsp(int mspID) {
        MSP msp = mspDao.findById(mspID);
        if (msp != null) {
            mspDao.delete(msp);
        }
    }
    
    @Override
    @Transactional(readOnly = false)
    public void simpleSaveMsp_Log(MSP msp) {
        mspDao.simpleSave_Log(msp);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void simpleUpdateMsp_Log(MSP msp) {
        mspDao.simpleUpdate_Log(msp);
    }

    @Override
    @Transactional(readOnly = false)
    public void simpleDeleteMsp_Log(MSP msp) {
    	mspDao.simpleDelete_Log(msp);
    }

	@Override
	public List<MSP> getAllMsps() {
		return mspDao.getAllInstances();
	}

	@Override
	public MSP getThisMsp() {
		return mspDao.getFirstEntity();
	}
	

/*	@Override
	public int getNewId() {
		return mspDao.getNewId();
	}
*/
}
