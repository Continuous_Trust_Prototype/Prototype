package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.AuditorDao;
import trust.simulator.entity.Auditor;
import trust.simulator.service.AuditorService;


@Service("AuditorService")
@Transactional(readOnly = true)
public class AuditorServiceImpl implements AuditorService {

    @Autowired
    private AuditorDao auditorDao;

    @Override
    public Auditor findByAuditorID(int auditorID) {
        return auditorDao.findById(auditorID);
    }
    
    @Override
    public Auditor findByName(String name) {
        return auditorDao.findByName(name);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveAuditor(Auditor auditor) {
        auditorDao.save(auditor);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateAuditor(Auditor auditor) {
        auditorDao.update(auditor);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteAuditor(Auditor auditor) {
    	auditorDao.delete(auditor);
    }

	@Override
	public List<Auditor> getAllAuditors() {
		return auditorDao.getAllInstances();
	}
	
    @Override
    @Transactional(readOnly = false)
    public void simpleSaveAuditor_Log(Auditor auditor) {
        auditorDao.simpleSave_Log(auditor);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void simpleUpdateAuditor_Log(Auditor auditor) {
        auditorDao.simpleUpdate_Log(auditor);
    }

    @Override
    @Transactional(readOnly = false)
    public void simpleDeleteAuditor_Log(Auditor auditor) {
    	auditorDao.simpleDelete_Log(auditor);
    }

/*	@Override
	public int getNewId() {
		return auditorDao.getNewId();
	}
*/
}
