package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.AbstractLogDao;
import trust.simulator.entity.AbstractLog;
import trust.simulator.service.AbstractLogService;


@Service("AbstractLogService")
@Transactional(readOnly = true)
public class AbstractLogServiceImpl implements AbstractLogService {

    @Autowired
    private AbstractLogDao abstractlogDao;

    @Override
    public AbstractLog findByLogID(int logID) {
        return abstractlogDao.findById(logID);
    }

	@Override
	public List<AbstractLog> getAllLogs() {
		return abstractlogDao.getAllInstances();
		
	}
	

	@Override
	public void saveLog(AbstractLog log) {
		abstractlogDao.save(log);
		
	}

	@Override
	public void updateLog(AbstractLog log) {
		abstractlogDao.update(log);
		
	}

	@Override
	public void deleteLog(int logID) {
		AbstractLog abstractLog = abstractlogDao.findById(logID);
        if (abstractLog != null) {
        	abstractlogDao.delete(abstractLog);
        }
		
	}

}
