package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.MessageDao;
import trust.simulator.entity.Message;
import trust.simulator.service.MessageService;


@Service("messageService")
@Transactional(readOnly = true)
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Override
    public Message findByMessageID(int messageID) {
        return messageDao.findById(messageID);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveMessage(Message message) {
        messageDao.save(message);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateMessage(Message message) {
        messageDao.update(message);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteMessage(int messageID) {
        Message message = messageDao.findById(messageID);
        if (message != null) {
            messageDao.delete(message);
        }
    }

	@Override
	public List<Message> getAllMessages() {
		return messageDao.getAllInstances();
		
	}

/*	@Override
	public int getNewId() {
		return messageDao.getNewId();
	}
*/
}
