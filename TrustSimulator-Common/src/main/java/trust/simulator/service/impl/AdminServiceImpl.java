package trust.simulator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.AdminDao;
import trust.simulator.service.AdminService;

@Service("adminService")
@Transactional(readOnly = false)
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminDao adminDao;
    
    @Override
    @Transactional(readOnly = false)
    public void resetDB() {
      
    	adminDao.resetDB();
    }

}
