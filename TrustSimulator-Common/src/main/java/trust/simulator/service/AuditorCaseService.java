package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.AuditorCase;

public interface AuditorCaseService {

    AuditorCase findByAuditorCaseID(int auditorcaseID);
    int saveAuditorCase(AuditorCase auditorcase);
    void updateAuditorCase(AuditorCase auditorcase);
    void deleteAuditorCase(int auditorcaseID);
    List<AuditorCase> getAllAuditorCases(int auditorID);
    //int getNewId();
}
