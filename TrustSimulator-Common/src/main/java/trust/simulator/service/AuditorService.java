package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.Auditor;

public interface AuditorService {

    Auditor findByAuditorID(int auditorID);
    Auditor findByName(String name);
    void saveAuditor(Auditor auidtor);
    void updateAuditor(Auditor auditor);
    void deleteAuditor(Auditor auditor);
    List<Auditor> getAllAuditors();
    //int getNewId();
	void simpleSaveAuditor_Log(Auditor auditor);
	void simpleUpdateAuditor_Log(Auditor auditor);
	void simpleDeleteAuditor_Log(Auditor auditor);
}
