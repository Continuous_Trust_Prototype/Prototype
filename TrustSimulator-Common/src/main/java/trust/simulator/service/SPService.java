package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.SP;

public interface SPService {

    SP findBySpID(int spID);
    void saveSp(SP sp);
    void updateSp(SP sp);
    void deleteSp(SP sp);
    List<SP> getAllSps();
    //int getNewId();
	SP findByName(String name);
	void simpleSaveSp_Log(SP sp);
	void simpleUpdateSp_Log(SP sp);
	void simpleDeleteSp_Log(SP sp);
}
