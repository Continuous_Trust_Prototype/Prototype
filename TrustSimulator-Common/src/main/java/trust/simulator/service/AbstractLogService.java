package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.AbstractLog;

public interface AbstractLogService {

    AbstractLog findByLogID(int logID);
    List<AbstractLog> getAllLogs();
    void saveLog(AbstractLog log);
    void updateLog(AbstractLog log);
    void deleteLog(int logID);
}
