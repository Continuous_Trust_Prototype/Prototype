package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.Message;

public interface MessageService {

    Message findByMessageID(int messageID);
    void saveMessage(Message message);
    void updateMessage(Message message);
    void deleteMessage(int messageID);
    List<Message> getAllMessages();
    //int getNewId();
}
