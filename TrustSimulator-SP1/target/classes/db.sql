SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `TrustSimulator_SP<x>`
--
-- Change SP1 to the relevant number SP<x>!!!
CREATE DATABASE `TrustSimulator_SP1`;
USE `TrustSimulator_SP1`;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractEntity`
--

CREATE TABLE `AbstractEntity` (
  `discriminator` varchar(31) NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `modifiedOn` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `offer` varchar(32) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `interestedIn` varchar(32) DEFAULT NULL,
  `affiliatedWithMsp` int(11) DEFAULT NULL,
  `affiliatedWithMspName` varchar(255) DEFAULT NULL,
  `affiliatedWithMspHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `AbstractLog`
--

CREATE TABLE `AbstractLog` (
  `ID` int(11) NOT NULL,
  `discriminator` varchar(31) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `entityID` int(5) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `UserPhone` varchar(32) DEFAULT NULL,
  `MSPinterestedIn` varchar(32) DEFAULT NULL,
  `SPAffiliatedWithMsp` int(5) DEFAULT NULL,
  `SPAffiliatedWithMspName` varchar(255) DEFAULT NULL,
  `SPAffiliatedWithMspHost` varchar(255) DEFAULT NULL,
  `SPOffer` varchar(32) DEFAULT NULL,
  `auditorcaseAuditorID` int(5) DEFAULT NULL,
  `auditorcaseID` int(5) DEFAULT NULL,
  `auditorcaseMessageID` int(11) DEFAULT NULL,
  `auditorcaseUserID` int(5) DEFAULT NULL,
  `auditorcaseComment` mediumtext,
  `auditorcaseConvictedSPID` int(5) DEFAULT NULL,
  `auditorcaseInvestigatedDataType` varchar(32) DEFAULT NULL,
  `auditorcaseInvestigatedDataValue` varchar(255) DEFAULT NULL,
  `auditorcaseStatus` varchar(255) DEFAULT NULL,
  `SPDataObtainedDataType` varchar(32) DEFAULT NULL,
  `SPDatOobtainedDataValue` varchar(255) DEFAULT NULL,
  `spdataID` int(5) DEFAULT NULL,
  `SPDataSPID` int(5) DEFAULT NULL,
  `SPDataUserID` int(5) DEFAULT NULL,
  `mspdataID` int(5) DEFAULT NULL,
  `mspdataMSPID` int(5) DEFAULT NULL,
  `mspdataSPID` int(5) DEFAULT NULL,
  `mspdataSPHost` varchar(255) DEFAULT NULL,
  `mspdataUserID` int(5) DEFAULT NULL,
  `mspdataObtainedDataType` varchar(32) DEFAULT NULL,
  `mspdataObtainedDataValue` varchar(255) DEFAULT NULL,
  `mspdataUsedCount` int(11) DEFAULT NULL,
  `messageBody` mediumtext,
  `messageID` int(5) DEFAULT NULL,
  `messageReceiverEmail` varchar(32) DEFAULT NULL,
  `messageReceiverName` varchar(32) DEFAULT NULL,
  `messageSenderEmail` varchar(32) DEFAULT NULL,
  `messageSenderName` varchar(32) DEFAULT NULL,
  `messageStatus` varchar(32) DEFAULT NULL,
  `messageTitle` varchar(255) DEFAULT NULL,
  `IDPScope` varchar(255) DEFAULT NULL,
  `IDPTokenValue` varchar(4096) DEFAULT NULL,
  `IDPHost` varchar(255) DEFAULT NULL,
  `IDPClientHost` varchar(255) DEFAULT NULL,
  `IDPUserSub` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token_value` varchar(4096) DEFAULT NULL,
  `expiration` timestamp NULL DEFAULT NULL,
  `token_type` varchar(256) DEFAULT NULL,
  `refresh_token_id` bigint(20) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `auth_holder_id` bigint(20) DEFAULT NULL,
  `id_token_id` bigint(20) DEFAULT NULL,
  `approved_site_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `MESSAGE`
--

CREATE TABLE `MESSAGE` (
  `MESSAGEID` int(11) NOT NULL,
  `body` mediumtext,
  `createdOn` datetime DEFAULT NULL,
  `receiverEmail` varchar(32) DEFAULT NULL,
  `receiverName` varchar(32) DEFAULT NULL,
  `senderEmail` varchar(32) DEFAULT NULL,
  `senderName` varchar(32) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MESSAGEID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `SEQUENCE`
--

CREATE TABLE `SEQUENCE` (
  `SEQ_NAME` varchar(50) NOT NULL,
  `SEQ_COUNT` decimal(38,0) DEFAULT NULL,
  PRIMARY KEY (`SEQ_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SEQUENCE`
--

INSERT INTO `SEQUENCE` VALUES('SEQ_GEN', 0);

-- --------------------------------------------------------

--
-- Table structure for table `SPDATA`
--

CREATE TABLE `SPDATA` (
  `SPDATAID` int(11) NOT NULL,
  `createdOn` datetime DEFAULT NULL,
  `obtainedDataType` varchar(32) DEFAULT NULL,
  `obtainedDataValue` varchar(255) DEFAULT NULL,
  `spID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`SPDATAID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

