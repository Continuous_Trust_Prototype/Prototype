<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageSPs" scope="session" />
<c:set var="ses_headerTitle" value="Manage SPs..." scope="session" />
<c:set var="ses_pageTitle" value="Manage SPs..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Edit this Service Provider - SP:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'edit'}">
<div id="mainA">

	<h2>Edit SP Details</h2>

	<c:if test="${updated == 'success'}">
		<p class="success">SP Updated Successfully</p>
	</c:if>
	
	<form:form modelAttribute="sp" action="${sessionScope.app_url}/editSP_2" method="post">
	<form:input type="hidden" path="ID" value="${sp.ID}"/>
	<form:input type="hidden" path="name" value="${sp.name}"/>
	<form:input type="hidden" path="createdOn" value="${sp.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${sp.modifiedOn}"/>
	<form:input type="hidden" path="email" value="${sp.email}"/>
	<form:input type="hidden" path="affiliatedWithMspName" value="${sp.affiliatedWithMspName}"/>
	<form:input type="hidden" path="affiliatedWithMspHost" value="${sp.affiliatedWithMspHost}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>SP ID</td></form:label>
			<td colspan='4' class='cellData'> ${sp.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${sp.name}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${sp.password}"/></td>
		</tr>
		
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${sp.email}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" value="${sp.host}" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" value="${sp.description}" /></td>
		</tr>
		
		<tr class='required'>
			<form:label path="offer"><td colspan='2' class='cellTitle'>Offer</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="offer" value="${sp.offer}" /></td>
		</tr>
		
		<tr>
			<td colspan='2' class='cellTitle'>Affiliated With MSP</td>
			<td colspan='4' class='cellData'>  
			<select name='mspID'>
			<option value='0'></option>
			<c:forEach var="currentMSP" items="${msps}">
			<c:choose>
			<c:when test="${currentMSP.ID == sp.affiliatedWithMsp}"><option value='${currentMSP.ID}' selected='selected'>${currentMSP.name}</option></c:when>
			<c:otherwise> <option value='${currentMSP.ID}'>${currentMSP.name}</option></c:otherwise>
			</c:choose>
			</c:forEach>
			</select>
			</td>
		</tr>

		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update SP</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>



