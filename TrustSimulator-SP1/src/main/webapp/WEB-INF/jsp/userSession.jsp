<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="userSession" scope="session" />
<c:set var="ses_headerTitle" value="User Simulation Session..." scope="session" />
<c:set var="ses_pageTitle" value="User Simulation Session..." scope="session" />
<c:set var="ses_pageDescription" value="In this simulation page, you can choose to edit your personal details or check your inbox using the menu tabs. Plus, you can start interacting with one of the service providers listed below:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>

<c:choose>

<c:when test="${request == 'getService2'}">
<div id="mainA">

	<script>goUser()</script>
	<h2>Thanks for dealing with: </h2>
	<h1>${sp.name}</h1>
	<h2> You will be contacted shortly to get your requested: </h2>
	<h1>${sp.offer}</h1>
	<h2>You will be redirected to the Main page in few seconds</h2>
	<h3><a href='http://localhost:8080/TrustSimulator/user'>or click here to go there directly</a></h3>
	
</div>
</c:when>

<c:otherwise>
<div id="mainA">

<h2>:)</h2>
	
</div>
</c:otherwise>

</c:choose>


<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>