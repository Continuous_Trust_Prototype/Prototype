package trust.simulator.dao;

import trust.simulator.entity.SPData;

public interface SPDataDao extends AbstractDao<SPData, String> {

}
