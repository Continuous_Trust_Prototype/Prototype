package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.IDPApi;
import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.SPDao;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPLog;

@Repository
public class SPDaoImpl extends AbstractDaoImpl<SP, String> implements SPDao {

    protected SPDaoImpl() {
        super(SP.class);
    }
    
    private IDPApi idpApi = new IDPApi();
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${idpHost}")
    private String idp_host;
    
    @Value("${idpPersistSPPath}")
    private String idp_persistSP_path;
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorPersistSPPath}")
    private String simulator_persistSP_path;
    
	//Log Events
    @Override
    public SP save(SP e) {
        
    	getCurrentSession().persist(e);
        
    	SPLog spLog = new SPLog(e, "save");
        getCurrentSession().persist(spLog);
        
        try {
        	
			idpApi.persistSP(idp_host, idp_persistSP_path, e, "save");
			simulatorApi.persistSP(simulator_host, simulator_persistSP_path, e, "save");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(SP e) {

    	//System.out.println("SP DAO!!");
    	
    	getCurrentSession().merge(e);
        
    	SPLog spLog = new SPLog(e, "update");
        getCurrentSession().persist(spLog);
        
        try {
        	
			idpApi.persistSP(idp_host, idp_persistSP_path, e, "update");
			simulatorApi.persistSP(simulator_host, simulator_persistSP_path, e, "update");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(SP e) {
        
    	try {
        	
			idpApi.persistSP(idp_host, idp_persistSP_path, e, "delete");
			simulatorApi.persistSP(simulator_host, simulator_persistSP_path, e, "delete");
			
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().remove(e);
        
        SPLog spLog = new SPLog(e, "remove");
        getCurrentSession().persist(spLog);
        
        getCurrentSession().flush();
    }

	//Log Events
    @Override
    public void simpleSave_Log(SP e) {
        
    	getCurrentSession().persist(e);
        
    	SPLog spLog = new SPLog(e, "save");
        getCurrentSession().persist(spLog);
        
        getCurrentSession().flush();
      
     }
    
  //Log Events
    @Override
    public void simpleUpdate_Log(SP e) {
  
    	getCurrentSession().merge(e);
        
    	SPLog spLog = new SPLog(e, "update");
        getCurrentSession().persist(spLog);
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void simpleDelete_Log(SP e) {
                
    	SP sp = getCurrentSession().getReference(SP.class, e.getID());
        getCurrentSession().remove(sp);
        
        SPLog spLog = new SPLog(e, "remove");
        getCurrentSession().persist(spLog);
        
        getCurrentSession().flush();
    }
    
}
