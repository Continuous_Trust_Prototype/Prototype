package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.MessageDao;
import trust.simulator.entity.Message;
import trust.simulator.entity.MessageLog;
import trust.simulator.service.MessagesNotification;

@Repository
public class MessageDaoImpl extends AbstractDaoImpl<Message, String> implements MessageDao {

    protected MessageDaoImpl() {
        super(Message.class);
    }
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorAddMessagePath}")
    private String simulator_addMessage_path;
    
	//Log Events
    @Override
    public Message save(Message e) {
    	
    	
        try {
 			
        	int newMessageID = simulatorApi.addMessage(e, simulator_host, simulator_addMessage_path);
 			e.setMessageID(newMessageID);
 	        getCurrentSession().persist(e);
 	        MessageLog messageLog = new MessageLog(e, "save");
 	        getCurrentSession().persist(messageLog);
 	        
 		}catch (URISyntaxException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) {
 			e1.printStackTrace();
 		}
        
        getCurrentSession().flush();
        return e;
    }
    
    @Override
    public MessagesNotification getMessagesNotification(String receiverEmail) {
        MessagesNotification messagesNotification = new MessagesNotification();
        TypedQuery<String> query = getCurrentSession().createQuery("SELECT g.receiverEmail FROM Message g WHERE g.receiverEmail = '"+receiverEmail+"' AND g.status = 'unread' ORDER BY g.createdOn DESC", String.class);
		if (query.getResultList().isEmpty())
		{
			messagesNotification.setEmpty(true);
			messagesNotification.setUnreadMessages(0);
		}
		else
		{
			messagesNotification.setEmpty(false);
			messagesNotification.setUnreadMessages(query.getResultList().size());
		}
		
		getCurrentSession().flush();
		
		return messagesNotification;
    }
    
    @Override
    public List<Message> getUserMessages(String receiverEmail) {

        TypedQuery<Message> query = getCurrentSession().createQuery("SELECT g FROM Message g WHERE g.receiverEmail = '"+receiverEmail+"' ORDER BY g.createdOn DESC", Message.class);
        return query.getResultList();
    }
    
}
