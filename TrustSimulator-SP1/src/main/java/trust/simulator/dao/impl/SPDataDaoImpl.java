package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.SPDataDao;
import trust.simulator.entity.SPData;
import trust.simulator.entity.SPDataLog;

@Repository
public class SPDataDaoImpl extends AbstractDaoImpl<SPData, String> implements SPDataDao {

    protected SPDataDaoImpl() {
        super(SPData.class);
    }
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorAddSPDataLogPath}")
    private String simulator_addSPDataLog_path;
    
	//Log Events
    @Override
    public SPData save(SPData e) {
        getCurrentSession().persist(e);
        SPDataLog spdataLog = new SPDataLog(e, "save");
        getCurrentSession().persist(spdataLog);

        try {
        	simulatorApi.addSPdataLog(spdataLog, simulator_host, simulator_addSPDataLog_path);
		}catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(SPData e) {
        getCurrentSession().merge(e);
        SPDataLog spdataLog = new SPDataLog(e, "update");
        getCurrentSession().persist(spdataLog);
        try {
        	simulatorApi.addSPdataLog(spdataLog, simulator_host, simulator_addSPDataLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(SPData e) {
        getCurrentSession().remove(e);
        SPDataLog spdataLog = new SPDataLog(e, "remove");
        getCurrentSession().persist(spdataLog);
   
        try {
        	simulatorApi.addSPdataLog(spdataLog, simulator_host, simulator_addSPDataLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

}
