package trust.simulator.controller;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.api.SimulatorApi;
import trust.simulator.entity.MSP;
import trust.simulator.entity.SP;
import trust.simulator.service.MessageService;
import trust.simulator.service.SPService;
import trust.simulator.util.Operations;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Controller
public class SPController {

    @Autowired
    private SPService spService;

    @Autowired
    private MessageService messageService;
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorGetMSPsPath}")
    private String simulator_getMSPs_path;
    
    @Value("${simulatorGetMSPPath}")
    private String simulator_getMSP_path;
        
    @RequestMapping(value = "/editSP_{spID}", method = RequestMethod.GET)
    public String editSP(Model model, @PathVariable int spID) throws ClientProtocolException, URISyntaxException, IOException {
        SP sp = spService.findBySpID(spID);
        
    	List<MSP> msps = simulatorApi.getMSPs(simulator_host, simulator_getMSPs_path);
        model.addAttribute("msps", msps);
               
        model.addAttribute("sp", sp);
        model.addAttribute("request", "edit");
        return "manageSPs";
    }

    @RequestMapping(value = "/editSP_2", method = RequestMethod.POST)
    public String editSP_2( Model model, SP sp, HttpServletRequest request) throws ClientProtocolException, URISyntaxException, IOException {
    	
    	Integer mspID = Integer.parseInt(request.getParameter("mspID"));
      
    	if(mspID != null && mspID != 0)
        {
        	MSP msp = simulatorApi.getMSP(simulator_host, simulator_getMSP_path, mspID);
            
	        sp.setAffiliatedWithMsp(mspID);
	        sp.setAffiliatedWithMspName(msp.getName());
	        sp.setAffiliatedWithMspHost(msp.getHost());
        }
        else
        {
	        sp.setAffiliatedWithMsp(0);
	        sp.setAffiliatedWithMspName(null);
	        sp.setAffiliatedWithMspHost(null);
        }
    	sp.setModifiedOn(Operations.getCurrentTime());
    	
        spService.updateSp(sp);
        
        model.addAttribute("updated", "success");
        model.addAttribute("request", "edit");
        model.addAttribute("sp", sp);

        List<MSP> msps = simulatorApi.getMSPs(simulator_host, simulator_getMSPs_path);
        model.addAttribute("msps", msps);
        
        return "manageSPs";
    }    
    
    @RequestMapping(value = "/persistSP", method = RequestMethod.POST)
    public void persistSP(@RequestParam("action_sp") String action_sp) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_sp);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_sp"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	SP sp = SP.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) spService.simpleSaveSp_Log(sp);
    	else if(action.equals("update")) spService.simpleUpdateSp_Log(sp);
    	else if(action.equals("delete")) spService.simpleDeleteSp_Log(sp);
    }

}
