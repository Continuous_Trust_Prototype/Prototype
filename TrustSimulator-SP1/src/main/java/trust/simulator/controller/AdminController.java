package trust.simulator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.SP;
import trust.simulator.service.AdminService;
import trust.simulator.service.SPService;


@Controller
public class AdminController {
	
    @Autowired
    private SPService spService;
    
    @Autowired
    private AdminService adminService;
    
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showAdminPage() {
    	return "admin";
    }
    
    @RequestMapping(value = "/resetDB", method = RequestMethod.GET)
    public String resetDB() {
    	
    	adminService.resetDB();
    	
    	return "admin";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(Model model) {
        model.addAttribute("sp",new SP());
        return "login";
    }
    
    @RequestMapping(value = "/login_2", method = RequestMethod.POST)
    public String confirmLogin(Model model, SP sp, HttpSession session) {
    	SP existing = spService.findByName(sp.getName());
    	
    	if (existing != null && existing.getPassword().equals(sp.getPassword()) ) {   	
    		
             model.addAttribute("status", "exist");
             model.addAttribute("request", "greet");
             session.setAttribute("logedinSP", "true");
             session.setAttribute("logedinSPID", Integer.toString(existing.getID()));
             session.setAttribute("logedinSPName", existing.getName());
             return "login";
        }
    	
        model.addAttribute("sp",new SP());
        model.addAttribute("status", "Does not exist");
        
        return "login";
    }
        
}
