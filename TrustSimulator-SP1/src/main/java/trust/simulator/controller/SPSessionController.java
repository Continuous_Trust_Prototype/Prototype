package trust.simulator.controller;

import org.apache.http.client.ClientProtocolException;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.mitre.openid.connect.model.UserInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.api.IDPApi;
import trust.simulator.api.MSPApi;
import trust.simulator.entity.Message;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPData;
import trust.simulator.service.MessageService;
import trust.simulator.service.SPDataService;
import trust.simulator.service.SPService;
import trust.simulator.util.Operations;

import java.io.IOException;
import java.net.URISyntaxException;


import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@Controller
public class SPSessionController {
    
    @Autowired
    private SPService spService;
    
    @Autowired
    private SPDataService spdataService;
    
    @Autowired
    private MessageService messageService;
    
    private IDPApi idpApi = new IDPApi();
    
    private MSPApi mspApi = new MSPApi();
    
    @Value("${mspAddSPDataPath}")
    private String msp_addSPData_path;
    
    @Value("${idpHost}")
    private String idp_host; 
    
    @Value("${idpUserInfoPath}")
    private String idp_userInfo_path;     
    
    @RequestMapping(value = "/getService_2_{spID}", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_USER')")
    public String getService2(Model model, @PathVariable int spID, HttpSession session, HttpServletResponse response) throws ClientProtocolException, URISyntaxException, IOException {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	OIDCAuthenticationToken token = (OIDCAuthenticationToken) auth;
    	
    	String access_token = token.getAccessTokenValue();
    	
    	
    	UserInfo userInfo = idpApi.getUserInfo(idp_host, idp_userInfo_path, access_token);
    	System.out.println("userInfo.getPhoneNumber(): "+userInfo.getPhoneNumber());
    
    	//TODO load client's detail instead of searching 
    	//--> when each client is deployed in its own domain
    	SP sp = spService.findBySpID(spID);
    	
    	int userID = Operations.getIdFromSub((userInfo.getSub()));
    	
    	
        SPData spdata1 = new SPData(sp.getID(), "name", userInfo.getName(), userID);
        spdataService.saveSpData(spdata1);      
        
        SPData spdata2 = new SPData(sp.getID(), "email", userInfo.getEmail(), userID);
        spdataService.saveSpData(spdata2);
        
        int affiliatedWithMsp = sp.getAffiliatedWithMsp();
        if(affiliatedWithMsp != 0)
        {
            try {
    			mspApi.addSPdata(spdata2, sp, sp.getAffiliatedWithMspHost(), msp_addSPData_path);
    		}catch (URISyntaxException e1) {
    			e1.printStackTrace();
    		} catch (IOException e1) {
    			e1.printStackTrace();
    		}
        	
        }
        
        String messageTitle = Operations.setMessageServiceRequestAcknowledgmentTitle(sp.getName());
        String messageBody = Operations.setMessageServiceRequestAcknowledgmentBody(sp.getName(), sp.getOffer(), userInfo.getName());
        Message message = new Message(sp.getEmail(), sp.getName(), userInfo.getEmail(), userInfo.getName(), messageTitle, messageBody, "unread");
        messageService.saveMessage(message);
        
        //model.addAttribute("sp", sp);
        model.addAttribute("request", "getService2");
        
        //List<SP> sps = spService.getAllSps();
        //model.addAttribute("sps", sps);
        
        //idpApi.logout(Constants.idp_host, Constants.idp_logout_path);
        SecurityContextHolder.clearContext();        
 
        return "userSession";
        
        //String redirectUrl = "http://localhost:8080/TrustSimulator/getService_2_4";
        //return "redirect:" + redirectUrl;
    }
        
    
    @RequestMapping(value = "/session", method = RequestMethod.GET)
    public String testSession(){
  
    	return "session";
    }
}
