package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.SPDataDao;
import trust.simulator.entity.SPData;
import trust.simulator.service.SPDataService;


@Service("SPDataService")
@Transactional(readOnly = true)
public class SPDataServiceImpl implements SPDataService {

    @Autowired
    private SPDataDao spdataDao;

    @Override
    public SPData findBySpDataID(int spdataID) {
        return spdataDao.findById(spdataID);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveSpData(SPData spdata) {
        spdataDao.save(spdata);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateSpData(SPData spdata) {
        spdataDao.update(spdata);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteSpData(int spdataID) {
        SPData spdata = spdataDao.findById(spdataID);
        if (spdata != null) {
            spdataDao.delete(spdata);
        }
    }

	@Override
	public List<SPData> getAllSpData() {
		return spdataDao.getAllInstances();
		
	}

/*	@Override
	public int getNewId() {
		return spdataDao.getNewId();
	}
*/
}
