package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.SPData;

public interface SPDataService {

    SPData findBySpDataID(int spdataID);
    void saveSpData(SPData spdata);
    void updateSpData(SPData spdata);
    void deleteSpData(int spdataID);
    List<SPData> getAllSpData();
    //int getNewId();
}
