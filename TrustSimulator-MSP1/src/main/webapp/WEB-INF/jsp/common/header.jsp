<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="firstHead">

	<dl class="miniDl">

		<c:choose>
		
		<c:when test="${not empty sessionScope.logedinMSP && sessionScope.logedinMSP =='true'}">
		<dt id="main" class="Links">
			<a href="${sessionScope.app_url}">Main</a>
		</dt>
	
		<dt id="logout" class="Links">
			<a href="${sessionScope.app_url}/j_spring_security_logout" >Logout MSP</a>
		</dt>
		</c:when>
				
		<c:otherwise>
		<dt id="main" class="Links">
			<a href="${sessionScope.app_url}">Main</a>
		</dt>
		</c:otherwise>

		</c:choose>

	</dl>
	
<c:choose>	

	<c:when test="${not empty sessionScope.logedinMSP && sessionScope.logedinMSP == 'true' && (sessionScope.ses_page == 'admin' || sessionScope.ses_page == 'manageMSPs' || sessionScope.ses_page == 'manageLogs')}">
		<dl>
	
			<dt class="mainLinks">
				<a href="${sessionScope.app_url}/editMSP_${logedinMSPID}">Edit Profile</a>
			</dt>
			
			<dt class="mainLinks">
				<a href="#">Inbox</a>
			</dt>
			
			<dt class="mainLinks">
				<a href="#">Manage Logs</a>
			</dt>
		</dl>	
	</c:when>
	
	<c:when test="${sessionScope.ses_page == 'index'}">
		<dl>
			<dt class="mainLinks">
				<a href="${sessionScope.app_url}/admin">Admin View</a>
			</dt>
	  
			<dt class="mainLinks">
				<a href="#">User View</a>
			</dt>
		</dl>
	</c:when>

</c:choose>

</div>
