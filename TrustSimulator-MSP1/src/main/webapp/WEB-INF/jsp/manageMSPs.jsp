<%@ include file="common/top.jsp" %>

<c:set var="ses_page" value="manageMSPs" scope="session" />
<c:set var="ses_headerTitle" value="Manage MSPs..." scope="session" />
<c:set var="ses_pageTitle" value="Manage MSPs..." scope="session" />
<c:set var="ses_pageDescription" value="Below, you can Add / Edit / Delete Malicious Third Parties - MSPs:" scope="session" />
<c:set var="ses_pageDescriptionImageSrc" value="images/trust.jpg" scope="session" />

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<jsp:include page="common/head.jsp"/>

</head>

<body>

<div id="divContainer">

<jsp:include page="common/header.jsp"/>

<jsp:include page="common/secondHeader.jsp"/>


<c:choose>

<c:when test="${request == 'edit'}">

<div id="mainA">

	<h2>Edit MSP Details</h2>
	
	<c:if test="${updated == 'success'}">
		<p class="success">MSP Updated Successfully</p>
	</c:if>
	
	<form:form modelAttribute="msp" action="${sessionScope.app_url}/editMSP_2" method="post">
	<form:input type="hidden" path="ID" value="${msp.ID}"/>
	<form:input type="hidden" path="name" value="${msp.name}"/>
	<form:input type="hidden" path="createdOn" value="${msp.createdOn}"/>
	<form:input type="hidden" path="modifiedOn" value="${msp.modifiedOn}"/>
	<form:input type="hidden" path="interestedIn" value="${msp.interestedIn}"/>
	<form:input type="hidden" path="email" value="${msp.email}"/>
	<table class='formTable'>
		<tr class='formHeader'>
			<td colspan='6'>  </td>
		</tr>
		
		<tr class='required'>
			<form:label path="ID"><td colspan='2' class='cellTitle'>MSP ID</td></form:label>
			<td colspan='4' class='cellData'> ${msp.ID} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="name"><td colspan='2' class='cellTitle'>Name</td></form:label> 
			<td colspan='4' class='cellData'> ${msp.name} </td>
		</tr>
		
		<tr class='required'>
			<form:label path="password"><td colspan='2' class='cellTitle'>Password</td></form:label> 
			<td colspan='4' class='cellData'> <form:password path="password" value="${msp.password}"/></td>
		</tr>
		
		<tr class='required'>
			<form:label path="email"><td colspan='2' class='cellTitle'>Email</td></form:label> 
			<td colspan='4' class='cellData'> ${msp.email}</td>
		</tr>
		
		<tr class='required'>
			<form:label path="host"><td colspan='2' class='cellTitle'>Host</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="host" value="${msp.host}" /></td>
		</tr>
				
		<tr>
			<form:label path="description"><td colspan='2' class='cellTitle'>Description</td></form:label> 
			<td colspan='4' class='cellData'> <form:input path="description" value="${msp.description}" /></td>
		</tr>

		<tr class='formHeader'>
			<td colspan='3'><button type="reset" class='buttonSubmit'>Reset</button></td>
			<td colspan='3'><button type="submit" class='buttonSubmit' onclick="submitForm()">Update MSP</button></td>
		</tr>
			
	</table>
	
	</form:form>
	</div>
</c:when>

</c:choose>

<jsp:include page="common/footer.jsp"/>

</div>

</body>
</html>



