package trust.simulator.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trust.simulator.entity.MSP;
import trust.simulator.service.AdminService;
import trust.simulator.service.MSPService;

@Controller
public class AdminController {
	
    @Autowired
    private MSPService mspService;
    
    @Autowired
    private AdminService adminService;
    
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String showAdminPage() {
    	return "admin";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin(Model model) {
        model.addAttribute("msp",new MSP());
        return "login";
    }
    
    @RequestMapping(value = "/resetDB", method = RequestMethod.GET)
    public String resetDB() {
    	
    	adminService.resetDB();
    	
    	return "admin";
    }
    
    @RequestMapping(value = "/login_2", method = RequestMethod.POST)
    public String confirmLogin(Model model, MSP msp, HttpSession session) {
    	MSP existing = mspService.findByName(msp.getName());
    	
    	if (existing != null && existing.getPassword().equals(msp.getPassword()) ) {   	
    		
             model.addAttribute("status", "exist");
             model.addAttribute("request", "greet");
             session.setAttribute("logedinMSP", "true");
             session.setAttribute("logedinMSPID", Integer.toString(existing.getID()));
             session.setAttribute("logedinMSPName", existing.getName());
             return "login";
        }
    	
        model.addAttribute("msp",new MSP());
        model.addAttribute("status", "Does not exist");
        
        return "login";
    }
        
}
