package trust.simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.entity.MSP;
import trust.simulator.service.MSPService;
import trust.simulator.util.Operations;

@Controller
public class MSPController {

    @Autowired
    private MSPService mspService;
  
    @RequestMapping(value = "/editMSP_{mspID}", method = RequestMethod.GET)
    public String editMSP(Model model, @PathVariable int mspID) {
        MSP msp = mspService.findByMspID(mspID);
        model.addAttribute("msp", msp);
        model.addAttribute("request", "edit");
        return "manageMSPs";
    }

    @RequestMapping(value = "/editMSP_2", method = RequestMethod.POST)
    public String editMSP_2( Model model, MSP msp) {
    	msp.setModifiedOn(Operations.getCurrentTime());
        mspService.updateMsp(msp);
        model.addAttribute("updated", "success");
        model.addAttribute("request", "edit");
        model.addAttribute("msp", msp);

        return "manageMSPs";
    }
    
    @RequestMapping(value = "/persistMSP", method = RequestMethod.POST)
    public void persistMSP(@RequestParam("action_msp") String action_msp) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(action_msp);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("action_msp"); 
    	
    	String action = ((JsonObject) jsonMainArr.get(0)).get("action").getAsString();
    
    	MSP msp = MSP.fromJson((JsonObject) jsonMainArr.get(1));
    	 
    	if(action.equals("save")) mspService.simpleSaveMsp_Log(msp);
    	else if(action.equals("update")) mspService.simpleUpdateMsp_Log(msp);
    	else if(action.equals("delete")) mspService.simpleDeleteMsp_Log(msp);
    }


}
