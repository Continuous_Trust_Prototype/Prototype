package trust.simulator.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import trust.simulator.entity.MSP;
import trust.simulator.entity.MSPData;
import trust.simulator.entity.SP;
import trust.simulator.entity.SPData;
import trust.simulator.service.MSPDataService;
import trust.simulator.service.MSPService;


@Controller
public class MSPSessionController {
    
    @Autowired
    private MSPService mspService;
    
    @Autowired
    private MSPDataService mspdataService;
         
    @RequestMapping(value = "/addSPData", method = RequestMethod.POST)
    public void addSPDataLog(@RequestParam("sp_spdata") String sp_spdata) {
    	
    	JsonParser parser = new JsonParser();
    	JsonObject jsonObj = (JsonObject)parser.parse(sp_spdata);
    	
    	JsonArray jsonMainArr = jsonObj.getAsJsonArray("sp_spdata"); 
        
    	SP sp = SP.fromJson((JsonObject) jsonMainArr.get(0));
    	SPData spdata = SPData.fromJson((JsonObject) jsonMainArr.get(1));
    	
    	MSP msp = mspService.getThisMsp();
    	
    	MSPData mspdata = new MSPData(msp.getID(),spdata.getObtainedDataType(), spdata.getObtainedDataValue(), spdata.getSpdataUserID(), spdata.getSpdataSPID(), sp.getHost());
    	mspdataService.saveMspData(mspdata);
    }
        
}
