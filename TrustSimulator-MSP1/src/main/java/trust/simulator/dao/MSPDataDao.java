package trust.simulator.dao;

import java.util.List;

import trust.simulator.entity.MSPData;

public interface MSPDataDao extends AbstractDao<MSPData, String> {
	
	List<MSPData> getFreshEntries();

}
