package trust.simulator.dao.impl;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import trust.simulator.api.SimulatorApi;
import trust.simulator.dao.MSPDataDao;
import trust.simulator.dao.impl.AbstractDaoImpl;
import trust.simulator.entity.MSPData;
import trust.simulator.entity.MSPDataLog;

@Repository
public class MSPDataDaoImpl extends AbstractDaoImpl<MSPData, String> implements MSPDataDao {

    protected MSPDataDaoImpl() {
        super(MSPData.class);
    }
    
    private SimulatorApi simulatorApi = new SimulatorApi();
    
    @Value("${simulatorHost}")
    private String simulator_host;
    
    @Value("${simulatorAddMSPDataLogPath}")
    private String simulator_addMSPDataLog_path;
    
	//Log Events
    @Override
    public MSPData save(MSPData e) {
        getCurrentSession().persist(e);
        MSPDataLog mspdataLog = new MSPDataLog(e, "save");
        getCurrentSession().persist(mspdataLog);
        
        try {
        	simulatorApi.addMSPdataLog(mspdataLog, simulator_host, simulator_addMSPDataLog_path);
		}catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
        return e;
    }
    
    //Log Events
    @Override
    public void update(MSPData e) {
        getCurrentSession().merge(e);
        MSPDataLog mspdataLog = new MSPDataLog(e, "update");
        getCurrentSession().persist(mspdataLog);
        try {
        	simulatorApi.addMSPdataLog(mspdataLog, simulator_host, simulator_addMSPDataLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

    //Log Events
    @Override
    public void delete(MSPData e) {
        getCurrentSession().remove(e);
        MSPDataLog mspdataLog = new MSPDataLog(e, "remove");
        getCurrentSession().persist(mspdataLog);
        try {
        	simulatorApi.addMSPdataLog(mspdataLog, simulator_host, simulator_addMSPDataLog_path);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        
        getCurrentSession().flush();
    }

	@Override
	public List<MSPData> getFreshEntries() {
    	TypedQuery<MSPData> query = getCurrentSession().createQuery("SELECT g FROM MSPData g WHERE g.usedCount = 0", MSPData.class);
    	return query.getResultList();  
		
	}


}
