package trust.simulator.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import trust.simulator.dao.MSPDao;
import trust.simulator.dao.MSPDataDao;
import trust.simulator.entity.MSP;
import trust.simulator.entity.MSPData;
import trust.simulator.entity.Message;
import trust.simulator.service.MSPDataService;
import trust.simulator.service.MessageService;
import trust.simulator.util.Operations;


@Service("MSPDataService")
@Transactional(readOnly = true)
public class MSPDataServiceImpl implements MSPDataService {

    @Autowired
    private MSPDataDao mspdataDao;
    
    @Autowired
    private MSPDao mspDao;
    
    @Autowired
    private MessageService messageService;

    @Override
    public MSPData findByMspDataID(int mspdataID) {
        return mspdataDao.findById(mspdataID);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveMspData(MSPData mspdata) {
        mspdataDao.save(mspdata);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void updateMspData(MSPData mspdata) {
        mspdataDao.update(mspdata);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteMspData(int mspdataID) {
        MSPData mspdata = mspdataDao.findById(mspdataID);
        if (mspdata != null) {
            mspdataDao.delete(mspdata);
        }
    }

	@Override
	public List<MSPData> getAllMspData() {
		return mspdataDao.getAllInstances();
		
	}

/*	@Override
	public int getNewId() {
		return mspdataDao.getNewId();
	}
*/
	@Override
	@Scheduled(fixedRate=10000)
	@Transactional(readOnly = false)
	public void spamFreshEntries() {
		
		//System.out.println("spamFreshEntries!!");
		
		List<MSPData> mspdataList = mspdataDao.getFreshEntries();
		
		for(int i=0; i <mspdataList.size(); i++)
		{
	        System.out.println("FreshEntry ID - "+mspdataList.get(i).getMspdataID());
			
	        MSP msp = mspDao.findById(mspdataList.get(i).getMspdataMSPID());

	        //TODO utilise the obtained username to send personalised SPAM
	        String userName = "my friend";
			String userEmail = mspdataList.get(i).getObtainedDataValue();
			
			String messageTitle = Operations.setMessageSpamTitle(msp.getName());
	        String messageBody = Operations.setMessageSpamBody(msp.getName(), userName);
	        
	        Message message = new Message(msp.getEmail(), msp.getName(), userEmail, userName, messageTitle, messageBody, "unread");
	        messageService.saveMessage(message);
	        
	        mspdataList.get(i).setUsedCount(mspdataList.get(i).getUsedCount()+1);
	        mspdataDao.update(mspdataList.get(i));

		}

	}

}
