package trust.simulator.service;


import java.util.List;

import trust.simulator.entity.MSPData;

public interface MSPDataService {

    MSPData findByMspDataID(int mspdataID);
    void saveMspData(MSPData mspdata);
    void updateMspData(MSPData mspdata);
    void deleteMspData(int mspdataID);
    List<MSPData> getAllMspData();
    //int getNewId();
    void spamFreshEntries();
}
